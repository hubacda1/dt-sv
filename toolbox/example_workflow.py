"""
This file provides just an overview how could a SV algorithm look like.
"""
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

from spatial_verification import *

################
## SETUP DATA ##
################

MAX_SPATIAL = 100
INLIER_THRESHOLD = 62
SCALE_THRESHOLD = 8
QUERY_IMAGES = [
    {"bbox": ..., "fname": ..., "id": ...},
    ...
]
IMG_ROOT = "/path/to/images"


#######################
## End of SETUP DATA ##
#######################

##############
## DATABASE ##
##############

class Database:
    """
    This implementation depends on particular data provided.
    """

    def __init__(self, *args, **kwargs):
        ...

    def get_shortlist(self, query_visual_words, top_k=10000):
        """Get a shortlist of images."""
        ...

    def get_geometries(self, id, bbox=None, query=False):
        """Get feature properties from the database."""
        ...

    def get_image_id(self, fname, query=False):
        """Get image identifier from an image name."""
        ...

    def get_image_fname(self, id, query=False):
        """Get image name from an image ID."""
        ...

    def get_lines(self, fname, **kwargs):
        ...
    
    @staticmethod
    def kernel_similarity(v1, v2):
        ...



DATABASE = Database()

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

print('Start spatial verification.')

t0 = perf_counter()
#  =========================

for query in QUERY_IMAGES:

    q_bbox = query['bbox']
    q_fname = query['fname']
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(query['id'], bbox=q_bbox, query=True)

    for gnd_imid in DATABASE.get_shortlist(q_vecs, MAX_SPATIAL):
        
        db_fname = DATABASE.get_image_fname(gnd_imid)
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)

        if not corrs.size:
            print('No correspondences.')
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]
        coordy1 = q_coordy[corrs[:, 0]]

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]
        coordy2 = db_coordy[corrs[:, 1]]

        # Detect lines too
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(q_fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{q_fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        
        # Generate hypotheses
        # hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        if new_hypotheses.size:
            hypotheses = np.vstack([hypotheses, new_hypotheses])

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        determinants = np.linalg.det(hypotheses)
        scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
        verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD, scale_threshold=SCALE_THRESHOLD)

        # Local optimization of the weighted best model
        weighted_scores = -(verifications * _similarities[np.newaxis]).sum(axis=1)
        sorted_hypothesis_indexes = weighted_scores.argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, scales1,
            coordx2, coordy2, scales2, inlier_threshold=INLIER_THRESHOLD, eigenvals_threshold=5, scale_threshold=SCALE_THRESHOLD
        )

        # Log
        print('STATS:', json.dumps({
            'q_fname': q_fname,
            'db_fname': db_fname,
            'gnd_imid': int(gnd_imid),
            'corrs': corrs.shape[0],
            'result': total_support,
        }))


#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)
