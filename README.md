# Spatial verification

This repository contains, unfortunately, all the ballast I had to carry all
along the development of the thesis. There are tousands of useless files which
even I am lost in. Tens and hundreds of evaluation procedures, it is all here...
Only the last version with majority of function used in the final version of the
algorithm was extracted into the `toolbox` folder (even though it does not
contain all versions of all scripts).

What will be people missing, probably, are the input data. However, the SIFT
data can be digged out of `horn.felk.cvut.cz` from the project located in
`/local/vrg3_demo/vrg3_demo/app/engine_vdvm22`. On the other hand, HOW data can
be downloaded from 
`http://ptak.felk.cvut.cz/personal/jenicto2/export/descriptors_ransac_2023/resnet50-in_e20_im500:extract_last_1000/`,

In both cases, the `ROxford` dataset can be found on `ptak.felk.cvut.cz` at
`/mnt/datagrid/personal/jenicto2/projects/how/test`.

One more neccesary repository to check (and install along with `pip-freeze.txt`)
is `https://github.com/jenicek/asmk`. 
