"""
Script for interactive coordinates picking.
This script enables to interactively extract coordinates from images simply by diplaying them and then clicking on them.
It is based on `matplotlib`, therefore it requires some graphical backend engine which would enable such visualisation.
Left-click on an image adds a point, right-click on an image removes the closest point (next left-click adds this
removed point to preserve the order). Point colors correspond to the order in which they were added accross all images.
It is also possible to provide a set of already created points in the beginning using `-p, --points-paths` parameter.
Currently, only TIFF images are supported.
Final CSV files with the coordinates are saved on disk. Their location is adjustable via the `-o, --output-paths`
optional parameter. By default, image name with `.csv` suffix is used. The CSV file contains just two columns with the
x and y coordinates and contains no header. If the number (and therefore order of the points) does not fit, missing
points are filled with [0, 0].
Both output paths and points paths must correspond to the images in order, if provided.
Usage examples:
    * `python interactive_coords_picker.py image1.tiff image2.tiff`
    * `python interactive_coords_picker.py -o out1.csv out2.csv -- image1.tiff image2.tiff`
"""
import argparse
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
import pandas as pd

from itertools import zip_longest
from matplotlib.backend_bases import MouseButton, MouseEvent
from matplotlib.lines import Line2D
from pathlib import Path
from typing import NamedTuple, Sequence
from warnings import warn


class Point(NamedTuple):
    """Structure for saving a point - coordinates together with their `plt` object as a reference for removal."""
    x: float
    y: float
    point: Line2D


class PointsManager:
    """Manager for adding and removing points into images."""

    def __init__(self, axs: np.ndarray, colors=None, selection_threshold: int = 200) -> None:
        self.axs = axs
        self.colors = colors or list(mcolors.TABLEAU_COLORS.values())
        self.selection_threshold = selection_threshold
        self.pts: list[list[Point | None]] = [[] for _ in axs]
        self.indexes_to_add: list[list[int]] = [[] for _ in axs]
        self.final_points: list[list[tuple[int, int]]] = []

    def get_color(self, index: int) -> str:
        """Get next color to be used for a point depending on the order."""
        return self.colors[index % len(self.colors)]

    def are_points_consistent(self):
        """Check whether each image contains the same number of points."""
        lengths = {len(pts) for pts in self.pts}
        return len(lengths) == 1

    def _get_axid(self, ax) -> int | None:
        for i, _ax in enumerate(self.axs):
            if ax is _ax:
                return i

    def _add_point(self, axid: int, x: float, y: float) -> None:
        if self.indexes_to_add[axid]:
            pti = self.indexes_to_add[axid].pop(0)
        else:
            pti = len(self.pts[axid])
            self.pts[axid].append(None)

        line2d, = self.axs[axid].plot(x, y, marker='x', mew=2, markersize=8, color=self.get_color(pti))
        self.pts[axid][pti] = Point(x, y, line2d)
        self.axs[axid].figure.canvas.draw()

    def _get_closest_point_index(self, axid: int, x: float, y: float) -> int | None:
        # Find the closest point in L2 norm
        best_i, best_dist = -1, np.inf
        for i, pt in enumerate(self.pts[axid]):
            if pt is None:
                continue

            px, py, _ = pt
            dist = abs(y - py) + abs(x - px)
            if dist < best_dist:
                best_i, best_dist = i, dist

        # Check if the closest point is close enough
        if best_dist < self.selection_threshold:
            return best_i

    def _remove_point(self, axid: int, x: float, y: float) -> None:
        if not self.pts[axid]:
            return

        pti = self._get_closest_point_index(axid, x, y)
        if pti is not None:
            self.pts[axid][pti].point.remove()  # type: ignore
            self.pts[axid][pti] = None
            self.indexes_to_add[axid].append(pti)
            self.axs[axid].figure.canvas.draw()

    def onclick(self, event: MouseEvent) -> None:
        """Onclick mouse event callback."""
        axid = self._get_axid(event.inaxes)
        if axid is None:
            return

        match event.button:
            case MouseButton.LEFT:
                self._add_point(axid, event.xdata, event.ydata)
            case MouseButton.RIGHT:
                self._remove_point(axid, event.xdata, event.ydata)

    def stop(self) -> None:
        """Stop managing the points, squash empty points."""
        self.final_points = [[] for _ in self.axs]
        for pti, values in enumerate(zip_longest(*self.pts), 1):
            if all(v is None for v in values):
                continue

            if any(v is None for v in values):
                warn(f'Point No.{pti} is not defined in all input images. Coordinates (0, 0) will be used instead.')

            for i, v in enumerate(values):
                self.final_points[i].append((v[0], v[1]) if v is not None else (0, 0))

    def to_csv(self, axid: int, filename: Path | str | None = None) -> str | None:
        """Export points for a particular image into a CSV file."""
        ret = '\n'.join(f'{x},{y}' for x, y in self.final_points[axid])
        if filename:
            with open(filename, 'w+') as f:
                f.write(ret)
        else:
            return ret


def run_interactive_selection(
    image_paths: list[Path],
    colors: list[str] | None = None,
    output_paths: list[Path] | None = None,
    points_paths: list[Path] | None = None,
) -> None:
    """Run interactive coordinates selection from chosen images."""
    # Clean the input data
    points_paths = points_paths or []
    output_paths = output_paths or [p.with_suffix('.csv') for p in image_paths]

    # Basic checks
    if len(output_paths) != len(image_paths):
        raise ValueError('Number of output paths does not correspond to number of images.')
    if len(points_paths) != len(image_paths) and len(points_paths) != 0:
        raise ValueError('Number of points paths does not correspond to number of images.')

    # Create the images plot
    fig, ax = plt.subplots(1, len(image_paths), figsize=(12, 6))
    if len(image_paths) == 1:  # Matplotlib squashes the array into a scalar
        ax = [ax]

    for i, im_path in enumerate(image_paths):
        img = plt.imread(im_path)
        ax[i].imshow(img)

    # Create PointsManager and add default points
    pm = PointsManager(ax, colors)
    for axid, pts_path in enumerate(points_paths):
        pts = pd.read_csv(pts_path, header=None)
        for _, x, y in pts.itertuples():
            pm._add_point(axid, x, y)

    # Run the visualisation
    cid = fig.canvas.mpl_connect('button_press_event', pm.onclick)
    plt.show()
    fig.canvas.mpl_disconnect(cid)
    pm.stop()

    if not pm.are_points_consistent():
        warn('Number of points is not consistent through the images.')

    # Save the results
    for i, out_path in enumerate(output_paths):
        pm.to_csv(i, out_path)


def get_parser() -> argparse.ArgumentParser:
    """Argument parser."""
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument(
        'images',
        type=str,
        nargs='+',
        help='Path to images which should be used for coords picking.',
    )
    parser.add_argument(
        '-c',
        '--colors',
        type=str,
        nargs='+',
        help='List of colors to be used to distinquish between corresponding points. Optional in favor of a basic set.',
    )
    parser.add_argument(
        '-p',
        '--points-paths',
        type=str,
        nargs='+',
        help='Paths to CSV files of already created coordinates to all the images. Must correspond to images in order. '
             'Optional.',
    )
    parser.add_argument(
        '-o',
        '--output-paths',
        type=str,
        nargs='+',
        help='List of filenames to be used for the output. Must correspond to images in order. Optional, image '
             'filenames with .csv suffix by default.',
    )

    return parser


def main(argv: Sequence[str] | None = None) -> None:
    """Run the interactive coords selection via CLI."""
    parser = get_parser()
    options = parser.parse_args(argv)

    output_paths = [Path(p) for p in options.output_paths] if options.output_paths else None
    points_paths = [Path(p) for p in options.points_paths] if options.points_paths else None

    run_interactive_selection(
        image_paths=[Path(p) for p in options.images],
        colors=options.colors,
        output_paths=output_paths,
        points_paths=points_paths,
    )


if __name__ == '__main__':
    main()
