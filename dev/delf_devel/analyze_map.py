from pathlib import Path
import sys

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

################
## INPUT ARGS ##
################

assert len(sys.argv) == 2, 'Incorrect params'

FILE = Path(sys.argv[1])
assert FILE.exists()

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'delf_gld'
DATASET = 'roxford5k'
EVAL_FEATURES = 'delf_gld-features'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])

gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

# Load the results
ranks = io_helpers.load_pickle(FILE)

print('Analyzing:', FILE)
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
print()
