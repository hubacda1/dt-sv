from collections import defaultdict
from pathlib import Path
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'delf_gld'
DATASET = 'roxford5k'
EVAL_FEATURES = 'delf_gld-features'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()

q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    qvecs_centroid_ids,
    dbvecs_centroid_ids,
    q_residual_packs,
    db_residual_packs,
])
"""
qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs, db_residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../oxbuild_images')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }

        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        q = ['', 'q'][query]

        ql = np.searchsorted(self.desc[f'{q}imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'{q}imids'], id, 'right')

        tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc[f'{q}vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[f'{q}vecs_centroid_ids'][ql:qr]
        scales = self.desc[f'{q}scales'][ql:qr]
        # !!! It is inverted? Wtf???
        coordy = (self.desc[f'{q}coordx'][ql:qr]) + ty
        coordx = (self.desc[f'{q}coordy'][ql:qr]) + tx
        residuals = self.residual_packs[f'{q}residual_packs'][ql:qr]

        if bbox:
            mask = is_in_bbox(bbox, np.array([coordx, coordy]))
            return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        else:
            return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim



lines_db = io_helpers.load_pickle('../sift_check/_lines_database.pkl')
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                    db_residual_packs)

DISPLAY = False
if DISPLAY:
    qi = 0
    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)
    fname = gnd['qimlist'][qi]

    qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
    plt.imshow(qimg, cmap='gray')
    plt.plot(coordx, coordy, 'r.')
    plt.show()

    dbi = 511
    vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(dbi, bbox=None, query=False)
    fname = gnd['imlist'][dbi]

    dbimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
    plt.imshow(dbimg, cmap='gray')
    plt.plot(coordx, coordy, 'r.')
    plt.show()

#####################
## End of DATABASE ##
#####################
