from collections import defaultdict
from pathlib import Path
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print


IMG_ROOT = Path('../../oxbuild_images')

metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')
cache = io_helpers.load_pickle('_lines_database.pkl')
gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')

class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }

        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_shortlist(self, q_vw, top_k=10000):
        raise NotImplemented

    def get_geometries(self, id, bbox=None, query=False):
        q = ['', 'q'][query]

        ql = np.searchsorted(self.desc[f'{q}imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'{q}imids'], id, 'right')

        tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc[f'{q}vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[f'{q}vecs_centroid_ids'][ql:qr]
        scales = self.desc[f'{q}scales'][ql:qr]
        # !!! It is inverted? Wtf???
        coordy = (self.desc[f'{q}coordx'][ql:qr]) + ty
        coordx = (self.desc[f'{q}coordy'][ql:qr]) + tx
        residuals = self.residual_packs[f'{q}residual_packs'][ql:qr]

        if bbox:
            mask = is_in_bbox(bbox, np.array([coordx, coordy]))
            return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        else:
            return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        raise NotImplemented


DATABASE = Database(None, None, gnd, cache, None, None, None, None)

MIN_LINE_LENGTHS = 40
MAX_LINE_LENGTH = 110
MAX_SPATIAL = 100

for qi, fname in enumerate(gnd['qimlist']):  # query index
    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    qid = DATABASE.get_image_id(fname, query=True)

    if fname not in cache:
        lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[fname] = lines
        print('Query', qi, 'not in cache.')

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        db_fname = gnd['imlist'][gnd_imid]
        if db_fname in cache:
            continue
        lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[db_fname] = lines
        print('Shortlist', db_fname, 'not in cache.')

    for gnd_imid in (gnd['gnd'][qi]['easy'] + gnd['gnd'][qi]['hard']):
        db_fname = gnd['imlist'][gnd_imid]
        if db_fname in cache:
            continue
        lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[db_fname] = lines
        print('GND', db_fname, 'not in cache.')
    
    print(qi, 'completely done.')

io_helpers.save_pickle('_lines_database_updated.pkl', cache)
