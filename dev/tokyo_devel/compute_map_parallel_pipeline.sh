MAX=1125
CORES=48

STEP=$(($MAX/$CORES))

tasks=0

for i in $(seq 0 $STEP $(($MAX-1))); do
    max=$(( ($i+$STEP) > $MAX ? $MAX : ($i+$STEP) ))

    from=$i
    to=$max

    echo $from $to
    python compute_map_parallel.py $from $to &
    tasks=$((tasks+1))
done

echo "Tasks assigned, waiting"

wait

echo "All done, run integration"
echo "Tasks: $tasks"
ls -lah "compute_map_parallel/mid_results/" | wc -l
python compute_map_parallel_integration.py
echo "Integration done"
