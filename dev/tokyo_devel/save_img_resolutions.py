from pathlib import Path
from PIL import Image
from asmk import io_helpers

IMG_ROOT = Path('../../247tokyo1k/jpg')


def main():
    data = {}
    
    for f in Path(IMG_ROOT).glob('*.jpg'):
        img = Image.open(f)
        width, height = img.size

        data[f.stem] = {
            'width': width,
            'height': height,
        }

    io_helpers.save_pickle('_image_resolutions.pkl', data)


if __name__ == '__main__':
    main()
