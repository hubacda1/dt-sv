from asmk import io_helpers
from pathlib import Path
from PIL import Image


INTEGRATE_LINES = True
if INTEGRATE_LINES:
    INPUT_FOLDER = Path('lines_database_tmpdir')
    OUTPUT_FILE = Path('_lines_database.pkl')

    cache = {}
    for file in INPUT_FOLDER.glob('*.pkl'):
        lines = io_helpers.load_pickle(file)
        cache[file.stem] = lines

    io_helpers.save_pickle(OUTPUT_FILE, cache)


INTEGRATE_IMAGE_RESOLUTIONS = True
if INTEGRATE_IMAGE_RESOLUTIONS:
    INPUT_FOLDER = Path('../../247tokyo1k/resized')
    OUTPUT_FILE = Path('_image_resolutions_resized.pkl')

    data = {}

    for f in INPUT_FOLDER.glob('*.jpg'):
        img = Image.open(f)
        width, height = img.size

        data[f.stem] = {
            'width': width,
            'height': height,
        }

    io_helpers.save_pickle(OUTPUT_FILE, data)
