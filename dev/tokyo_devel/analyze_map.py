from pathlib import Path
import sys

import numpy as np

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

################
## INPUT ARGS ##
################

assert len(sys.argv) == 2, 'Incorrect params'

FILE = Path(sys.argv[1])
assert FILE.exists()

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'tokyo'
DATASET = '247tokyo1k'
EVAL_FEATURES = 'features'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])

gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

# Load the results
ranks = io_helpers.load_pickle(FILE)

idxs = []
for i, (r, g) in enumerate(zip(ranks, gnd['gnd'])):
    idxs.append(r.tolist().index(g['ok'][0]))
    idxs.append(r.tolist().index(g['ok'][1]))

print('Analyzing:', FILE)
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
print()
print('Where are the true values:')
print(np.unique(idxs, return_counts=True))
print()

possible_improvements = []

for i in range(ranks.shape[0]):

    any_false = False
    bad = False
    for r in ranks[i, :10]:
        if r in gnd['gnd'][i]['ok']:
            if any_false:
                bad = True
                if i not in possible_improvements:
                    possible_improvements.append(i)
        elif r in gnd['gnd'][i]['junk']:
            pass
        else:
            any_false = True

    if not bad:
        continue

    print(i, '===')
    for r in ranks[i, :10]:
        if r in gnd['gnd'][i]['ok']:
            print('T', end='', sep='')
        elif r in gnd['gnd'][i]['junk']:
            print('J', end='', sep='')
        else:
            print('.', end='', sep='')
    print()
    print(ranks[i, :10])

print()
print('Possible improvements #:', len(possible_improvements))
print(possible_improvements)
print()
