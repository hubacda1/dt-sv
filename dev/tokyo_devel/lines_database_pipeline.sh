IMG_ROOT=../../247tokyo1k/jpg

for i in $(seq -f "%04g" 0 112); do

    args=()
    for f in $IMG_ROOT/$i*.jpg; do
        filename=${f##*/}
        name=${filename%.*}
        args+=($name)
    done

    echo ${args[@]}

    python lines_database.py ${args[@]} &

done

# for f in $IMG_ROOT/0000*.jpg; do
#     filename=${f##*/}
#     name=${filename%.*}
#
#     echo $name
#
#     python lines_database.py $name &
# done

echo "Waiting for all precomputations"

wait

echo "Precomputed, starting integration"
python lines_database_integration.py
