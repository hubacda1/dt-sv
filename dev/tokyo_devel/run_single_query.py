from collections import defaultdict
from pathlib import Path
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'tokyo'
DATASET = '247tokyo1k'
EVAL_FEATURES = 'features'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
The following was slightly edited for TOKYO data. Especially, queries must be taken from database set of images.

# metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
ri = np.searchsorted(desc['imids'], 1124, 'right')
# ri = np.searchsorted(desc['imids'], 20, 'right')
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['vecs'][:ri], desc['imids'][:ri])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')

idxs = []
for i, (r, g) in enumerate(zip(ranks, gnd['gnd'])):
    idxs.append(r.tolist().index(g['ok'][0]))
    idxs.append(r.tolist().index(g['ok'][1]))

    # s = 0
    # for j in range(10):
    #     if r[j] in g['ok']:
    #         s += 1
    #         continue

    #     if r[j] in g['junk']:
    #         continue
        
    #     if s == 2:
    #         break

    #     print(f'{i},')
    #     break


print()
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])

print()
print()
print(np.unique(idxs, return_counts=True))
print()
print()

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
vecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
centroids = asmk_dataset.codebook.centroids[vecs_centroid_ids]
residual_packs = get_packed_residuals(desc['vecs'], centroids, binary=asmk_dataset.kernel.binary)
io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    vecs_centroid_ids,
    residual_packs,
])
"""

vecs_centroid_ids, residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../247tokyo1k/jpg')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, vecs_centroid_ids, residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = vecs_centroid_ids
        self.residual_packs = residual_packs

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}
        self._image_resolutions = io_helpers.load_pickle('_image_resolutions_resized.pkl')

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        ql = np.searchsorted(self.desc[f'imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'imids'], id, 'right')

        # Again some new format of coords :-(
        width = self._image_resolutions[gnd["imlist"][id]]['width']
        height = self._image_resolutions[gnd["imlist"][id]]['height']

        # 247tokyo1k does not have bounding boxes
        # tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc['vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[ql:qr]
        scales = self.desc['scales'][ql:qr]
        coordx = self.desc['coordx'][ql:qr] * width  # * 16 / scales
        coordy = self.desc['coordy'][ql:qr] * height  # * 16 / scales
        residuals = self.residual_packs[ql:qr]

        # 247tokyo1k does not have bounding boxes
        # if bbox:
        #     mask = is_in_bbox(bbox, np.array([coordx, coordy]))
        #     return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        # else:
        #     return vecs, centroid_ids, coordx, coordy, scales, residuals
        
        return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        return self.imlist_map[fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim


# lines_db = io_helpers.load_pickle('../sift_check/_lines_database.pkl')
lines_db = {}
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, vecs_centroid_ids, residual_packs)

"""
qi = 0
q_bbox = None  # BBox(*gnd['gnd'][qi]['bbx'])
vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)
fname = gnd['qimlist'][qi]

qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
plt.imshow(qimg, cmap='gray')
plt.plot(coordx, coordy, 'r.')
plt.show()
"""

#####################
## End of DATABASE ##
#####################

##########################
## SPATIAL VERIFICATION ##
##########################

print('Start spatial verification.')

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 10
INLIER_THRESHOLD = 62

# for qi, fname in enumerate(gnd['qimlist']):  # query index
for qi in [874]:
    fname = gnd['qimlist'][qi]

    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, query=True)

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):

        if gnd_imid == qi:
            # It should be the query image itself, skip...
            print('Skipping the original image itself.')
            continue
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)

        if not corrs.size:
            print('No corrs found !!')
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        """
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        """
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        """
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        if new_hypotheses.size:
            hypotheses = np.vstack([hypotheses, new_hypotheses])
        """
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=INLIER_THRESHOLD)
        
        # Local optimization of the weighted best model
        weighted_scores = -(verifications * _similarities[np.newaxis]).sum(axis=1)
        sorted_hypothesis_indexes = weighted_scores.argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, INLIER_THRESHOLD
        )

        # Save the support
        _ret1 = total_support
        _ret2 = -weighted_scores[sorted_hypothesis_indexes[0]]
        
        # Local optimization of the not-weighted best model
        not_weighted_scores = (-1 * verifications).sum(axis=1)
        sorted_hypothesis_indexes = not_weighted_scores.argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A2, total_mask2, total_support2 = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, INLIER_THRESHOLD
        )

        # Save the support
        _ret3 = total_mask2.sum()
        _ret4 = -not_weighted_scores[sorted_hypothesis_indexes[0]]

        # Log
        correct_pair = (gnd_imid in gnd['gnd'][qi]['ok'])
        if not correct_pair and gnd_imid in gnd['gnd'][qi]['junk']:
            correct_pair = None
        print('JSON STATS:', json.dumps({
            'qi': qi,
            'dbi': dbi,
            'gnd_imid': int(gnd_imid),
            'corrs': corrs.shape[0],
            'correct_pair': correct_pair,
            'supports_weighted_lo': float(_ret1),
            'supports_weighted': float(_ret2),
            'supports_inliers_lo': float(_ret3),
            'supports_inliers': float(_ret4),
        }))

        DISPLAY = True
        if DISPLAY and gnd_imid in [523, 524, 549]:

            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, score={total_support}')
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
            plt.show()

    break

    print()


#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)
