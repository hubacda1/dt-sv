import json
from pathlib import Path
import numpy as np

COMPUTE_MAP_FIND_INCORRECT_PAIRS = True
if COMPUTE_MAP_FIND_INCORRECT_PAIRS:
    with open('outputs/_compute_map-maxspatial10inlthr62.out.txt', 'r') as f:
        data = [json.loads(d[12:]) for d in f.readlines() if d.startswith('JSON STATS:')]

    filtered_data = {}
    for d in data:
        if d['correct_pair'] == 'junk':
            continue

        if d['qi'] not in filtered_data:
            filtered_data[d['qi']] = []

        filtered_data[d['qi']].append(d)

    count = 0
    ids = []

    for k in filtered_data:
        correct_pairs = np.array([d['correct_pair'] for d in filtered_data[k]])

        for supp_key in ['supports_weighted_lo', 'supports_weighted', 'supports_inliers_lo', 'supports_inliers']:
            score = np.array([d['supports_weighted_lo'] for d in filtered_data[k]])
            sort_keys = np.argsort(-score)

            sorted_correct_pairs = correct_pairs[sort_keys]

            if sorted_correct_pairs[:2].sum() < sorted_correct_pairs.sum():
                print(f'=== {k} - {supp_key} ===')
                for d in filtered_data[k]:
                    print(d)
                print()

                count += 1
                ids.append(k)
                
                break
    
    print('TOTAL COUNT:', count)
    print(ids)
