from collections import defaultdict
from pathlib import Path
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'tokyo'
DATASET = '247tokyo1k'
EVAL_FEATURES = 'features'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
The following was slightly edited for TOKYO data. Especially, queries must be taken from database set of images.

# metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
ri = np.searchsorted(desc['imids'], 1124, 'right')
# ri = np.searchsorted(desc['imids'], 20, 'right')
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['vecs'][:ri], desc['imids'][:ri])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
vecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
centroids = asmk_dataset.codebook.centroids[vecs_centroid_ids]
residual_packs = get_packed_residuals(desc['vecs'], centroids, binary=asmk_dataset.kernel.binary)
io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    vecs_centroid_ids,
    residual_packs,
])
"""

vecs_centroid_ids, residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../247tokyo1k/jpg')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, vecs_centroid_ids, residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = vecs_centroid_ids
        self.residual_packs = residual_packs

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}
        self._image_resolutions = io_helpers.load_pickle('_image_resolutions.pkl')

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        ql = np.searchsorted(self.desc[f'imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'imids'], id, 'right')

        # Again some new format of coords :-(
        width = self._image_resolutions[gnd["imlist"][id]]['width']
        height = self._image_resolutions[gnd["imlist"][id]]['height']

        # 247tokyo1k does not have bounding boxes
        # tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc['vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[ql:qr]
        scales = self.desc['scales'][ql:qr]
        coordx = self.desc['coordx'][ql:qr] * width  # * 16 / scales
        coordy = self.desc['coordy'][ql:qr] * height  # * 16 / scales
        residuals = self.residual_packs[ql:qr]

        # 247tokyo1k does not have bounding boxes
        # if bbox:
        #     mask = is_in_bbox(bbox, np.array([coordx, coordy]))
        #     return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        # else:
        #     return vecs, centroid_ids, coordx, coordy, scales, residuals
        
        return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        return self.imlist_map[fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim


# lines_db = io_helpers.load_pickle('../sift_check/_lines_database.pkl')
lines_db = {}
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, vecs_centroid_ids, residual_packs)

IMID1 = 522
IMID2 = 549
IMID2 = 268

q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(IMID1, query=True)
db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(IMID2)

# Generate correspondences
corrs, similarities = get_tentative_correspondencies(
    q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
)
_similarities = similarities * (similarities > 0)

assert corrs.size

# Pick corresponding data
scales1 = q_scales[corrs[:, 0]]
coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

scales2 = db_scales[corrs[:, 1]]
coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

im1_fname = gnd['imlist'][IMID1]
im2_fname = gnd['imlist'][IMID2]

img1 = plt.imread(IMG_ROOT / f'{im1_fname}.jpg')
img2 = plt.imread(IMG_ROOT / f'{im2_fname}.jpg')

qh, qw, _ = img1.shape
dbh, dbw, _ = img2.shape

img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
img[:qh, :qw] = img1
img[:dbh, qw:] = img2

plt.imshow(img)
plt.title(f'imid1={IMID1}, imid2={IMID2}, #corrs={corrs.shape[0]}')

rnd = np.random.RandomState(10)
for x1, y1, x2, y2 in zip(coordx1, coordy1, coordx2, coordy2):
    if rnd.random() <= 0.1:
        plt.plot([x1, x2 + qw], [y1, y2], linestyle='dashed', marker='.', markersize=2, linewidth=1)

plt.show()
