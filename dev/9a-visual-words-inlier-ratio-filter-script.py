from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from functools import partial
from time import perf_counter

from cirtorch.datasets.testdataset import configdataset
from cirtorch.utils.evaluate import compute_map_and_print
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf


def get_packed_residuals(vecs, centroids, binary=True):
    residuals = vecs - centroids
    
    if binary:
        return hamming.binarize_and_pack_2D(residuals)
    else:
        return functional.normalize_vec_l2(residuals)
    
def generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2):
    # 1 -> 2
    m = np.tile(np.eye(3), [coordx1.size, 1, 1])
    s = scales2 / scales1
    
    m[:, 0, 2] = -1 * coordx1 * s + coordx2
    m[:, 1, 2] = -1 * coordy1 * s + coordy2
    m[:, 0, 0] = m[:, 1, 1] = s
    
    return m


def compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2):
    pts1 = np.vstack([coordx1, coordy1, np.ones_like(coordx1)])
    pts2 = np.vstack([coordx2, coordy2, np.ones_like(coordx2)])
    
    # [number of hypotheses, number of correspondences]
    return np.linalg.norm((hypotheses @ pts1) - pts2, axis=1)


def verify_single_model(errors, corrs, inlier_threshold=32):
    n = corrs.shape[0]
    mask = np.zeros([n], dtype=bool)
    taken = set()

    i = 0
    actual_y, best_index = None, None
    best_error = None
    while i < n:
        actual_y = corrs[i, 1]
        
        best_error = np.inf
        best_index = -1
        while i < n and actual_y == corrs[i, 1]:
            if errors[i] < best_error and corrs[i, 0] not in taken:
                best_error = errors[i]
                best_index = i
            i += 1
        
        if best_error <= inlier_threshold:
            mask[best_index] = True
            taken.add(corrs[best_index, 0])
    
    return mask


def verify_models(errors, corrs, inlier_threshold=32):
    return np.apply_along_axis(
        partial(verify_single_model, corrs=corrs, inlier_threshold=inlier_threshold),
        axis=1,
        arr=errors,
    )


def affine_local_optimization(A, coordx1, coordy1, coordx2, coordy2):
    size = coordx1.size
    
    if size < 3:
        return A
    
    weight = 2 * np.pi if size < 11 else 0
    r2h = 50.0  # half of squared circle radius is integrated over
    
    mx1, my1 = coordx1.mean(), coordy1.mean()
    mx2, my2 = coordx2.mean(), coordy2.mean()
    
    dx1, dy1 = (coordx1 - mx1), (coordy1 - my1)
    dx2, dy2 = (coordx2 - mx2), (coordy2 - my2)
    
    # Compute AtA, AtB1, AtB2
    AtA = [0.0, 0.0, 0.0]
    AtB1 = [0.0, 0.0]
    AtB2 = [0.0, 0.0]
    
    AtA[0] = ((1 + weight) * dx1 * dx1 + weight * r2h).sum()
    AtA[1] = ((1 + weight) * dx1 * dy1).sum()
    AtA[2] = ((1 + weight) * dy1 * dy1 + weight * r2h).sum()
    
    AtB1[0] = ((1 + weight) * dx1 * dx2 + weight * A[0, 0] * r2h).sum()
    AtB1[1] = ((1 + weight) * dy1 * dx2 + weight * A[0, 1] * r2h).sum()
    
    AtB2[0] = ((1 + weight) * dx1 * dy2 + weight * A[1, 0] * r2h).sum()
    AtB2[1] = ((1 + weight) * dy1 * dy2 + weight * A[1, 1] * r2h).sum()
    
    # Final affine transformation
    detAtA = AtA[0] * AtA[2] - AtA[1] * AtA[1]
    if detAtA == 0:
        raise ValueError('Determinant equals to zero.')
        
    norm = 1 / detAtA
    H0 = (AtA[2] * AtB1[0] - AtA[1] * AtB1[1]) * norm
    H1 = (-AtA[1] * AtB1[0] + AtA[0] * AtB1[1]) * norm
    H2 = mx2 - mx1 * H0 - my1 * H1
    H3 = (AtA[2] * AtB2[0] - AtA[1] * AtB2[1]) * norm
    H4 = (-AtA[1] * AtB2[0] + AtA[0] * AtB2[1]) * norm
    H5 = my2 - mx1 * H3 - my1 * H4
    
    H = np.array([
        [H0, H1, H2],
        [H3, H4, H5],
        [0, 0, 1],
    ])
    
    return H


def get_tentative_correspondencies(
    q_ids, db_ids, q_residual_packs, db_residual_packs, similarity_func, max_tc=1500, max_MxN=15,
):
    db_unique, db_counts = np.unique(db_ids, return_counts=True)
    q_unique, q_counts = np.unique(q_ids, return_counts=True)
    
    db_sorted = np.argsort(db_ids)
    q_sorted = np.argsort(q_ids)
    
    ret = []
    similarities = []
    # counts = []
    
    qr_i = 0  # query index
    db_i = 0  # database index
    s_qr_i = 0  # sorted query index
    s_db_i = 0  # sorted database index

    qr_len = q_unique.shape[0]
    db_len = db_unique.shape[0]
    s_qry_len = q_sorted.shape[0]
    s_rel_len = db_sorted.shape[0]
    
    while qr_i < qr_len and db_i < db_len:  # While there are any visual words remaining
        if q_unique[qr_i] == db_unique[db_i]:  # If we encountered the the same visual word
            # count = q_counts[qr_i] * db_counts[db_i]
            # if count <= max_MxN:
            if True:
            
                # Move the sorted indexes forward so we find the beginning of current visual word
                while s_qr_i < s_qry_len and q_ids[q_sorted[s_qr_i]] != q_unique[qr_i]:
                    s_qr_i += 1
                while s_db_i < s_rel_len and db_ids[db_sorted[s_db_i]] != db_unique[db_i]:
                    s_db_i += 1
                
                # Cross products of corresponding visual words
                count = q_counts[qr_i] * db_counts[db_i]
                cross_product_correspondences = np.zeros([count, 2])
                cross_product_similarities = np.zeros([count])
                cross_product_index = 0
                
                s_qr_i_start = s_qr_i
                while s_db_i < s_rel_len and db_ids[db_sorted[s_db_i]] == db_unique[db_i]:
                    s_qr_i = s_qr_i_start
                    while s_qr_i < s_qry_len and q_ids[q_sorted[s_qr_i]] == q_unique[qr_i]:
                        #ret.append([q_sorted[s_qr_i], db_sorted[s_db_i]])
                        #similarities.append(similarity_func(
                        #    q_residual_packs[q_sorted[s_qr_i]], db_residual_packs[db_sorted[s_db_i]]
                        #)[0])
                        #vw_ids.append(q_unique[qr_i])
                        
                        cross_product_correspondences[cross_product_index] = [q_sorted[s_qr_i], db_sorted[s_db_i]]
                        cross_product_similarities[cross_product_index] = similarity_func(
                            q_residual_packs[q_sorted[s_qr_i]], db_residual_packs[db_sorted[s_db_i]]
                        )[0]
                        cross_product_index += 1
                        
                        s_qr_i += 1
                    s_db_i += 1
                    
                if count <= max_MxN:
                    mask = np.ones_like(cross_product_similarities, dtype=bool)
                else:
                    threshold = -np.sort(-cross_product_similarities)[max_MxN]
                    mask = cross_product_similarities >= threshold
                    
                ret.extend(cross_product_correspondences[mask][:max_MxN])
                similarities.extend(cross_product_similarities[mask][:max_MxN])
                    
            qr_i += 1
            db_i += 1
        elif q_unique[qr_i] < db_unique[db_i]:
            qr_i += 1
        else:
            db_i += 1
            
    ret_np = np.array(ret, dtype=int, ndmin=2)
        
    # If there are way too many correspondences, crop the result
    similarities_np = np.array(similarities)
    if ret_np.shape[0] > max_tc:
        # counts_np = np.array(counts)
        # keys = np.argsort(counts, kind='stable')[:max_tc]
        # return ret_np[keys]
        
        # If we do not care about the order
        #
        # keys = np.argsort(-similarities_np, kind='stable')[:max_tc]
        # return ret_np[keys], similarities_np[keys]
        
        # If we want to preserve the order (same visual words in a row)
        
        threshold = -np.sort(-similarities_np)[max_tc]
        mask = similarities_np >= threshold
        
        return ret_np[mask][:max_tc], similarities_np[mask][:max_tc]

    return ret_np, similarities_np


PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])


def kernel_similarity(v1, v2):
    _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
    return sim


t0 = perf_counter()

# Get centroid ids, then centroids and then residual packs
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
# dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = np.load('desc_vecs-quantized-dbvecs_centroid_ids.npy')

# ---
q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

t1 = perf_counter()

print('Centroids ready in:', t1 - t0)

IMAGES_ROOT = Path('.').resolve().parent / 'oxbuild_images'
MAX_SPATIAL = 100

from dataclasses import dataclass
from typing import Any


@dataclass
class DataItem:
    index: int
    vecs: Any
    centroid_ids: Any
    coordx: Any
    coordy: Any
    scales: Any
    residuals: Any
    
    def get(self):
        return (
            self.index,
            self.vecs,
            self.centroid_ids,
            self.coordx,
            self.coordy,
            self.scales,
            self.residuals,
        )
    

class DataIterator:
    def __init__(self, desc, vecs_centroid_ids, residual_packs, prefix='', include_mask=None):
        if include_mask is not None:
            self._imids = desc[f'{prefix}imids'][include_mask]
            self._vecs = desc[f'{prefix}vecs'][include_mask]
            self._coordx = desc[f'{prefix}coordx'][include_mask]
            self._coordy = desc[f'{prefix}coordy'][include_mask]
            self._scales = desc[f'{prefix}scales'][include_mask]
            self._vecs_centroid_ids = vecs_centroid_ids[include_mask]
            self._residual_packs = residual_packs[include_mask]
        else:
            self._imids = desc[f'{prefix}imids']
            self._vecs = desc[f'{prefix}vecs']
            self._coordx = desc[f'{prefix}coordx']
            self._coordy = desc[f'{prefix}coordy']
            self._scales = desc[f'{prefix}scales']
            self._vecs_centroid_ids = vecs_centroid_ids
            self._residual_packs = residual_packs
            
    def get_all_vecs(self):
        return self._vecs
    
    def get_all_imids(self):
        return self._imids
        
        
    def get_iter(self, idxs):
        if isinstance(idxs, int):
            idxs = range(idxs)
            
        for idx in idxs:
            li = np.searchsorted(self._imids, idx, 'left')
            ri = np.searchsorted(self._imids, idx, 'right')
            
            vecs = self._vecs[li:ri]
            centroid_ids = self._vecs_centroid_ids[li:ri]
            coordx = self._coordx[li:ri]
            coordy = self._coordy[li:ri]
            scales = self._scales[li:ri]
            residuals = self._residual_packs[li:ri]
            
            yield DataItem(idx, vecs, centroid_ids, coordx, coordy, scales, residuals)


metadata, images, ranks, _scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])

def sv(query_iterator, db_iterator, ranks):
    
    ranks_copy = ranks.copy()
    
    for qdata in query_iterator.get_iter(70):  # query index
        qi, q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = qdata.get()
        
        supports = np.zeros([MAX_SPATIAL])

        for enum_index, dbdata in enumerate(db_iterator.get_iter(ranks[qi, :MAX_SPATIAL])):
            dbi, db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = dbdata.get()

            # Generate correspondences
            corrs, similarities = get_tentative_correspondencies(
                q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, kernel_similarity
            )
            _similarities = similarities * (similarities > 0)

            if not corrs.size:
                supports[enum_index] = 0
                continue

            # Pick corresponding data
            scales1 = scales[q_scales[corrs[:, 0]]]
            coordx1 = q_coordx[corrs[:, 0]] * 16 / scales1
            coordy1 = q_coordy[corrs[:, 0]] * 16 / scales1
            centroid_ids1 = q_centroid_ids[corrs[:, 0]]

            scales2 = scales[db_scales[corrs[:, 1]]]
            coordx2 = db_coordx[corrs[:, 1]] * 16 / scales2
            coordy2 = db_coordy[corrs[:, 1]] * 16 / scales2
            centroid_ids2 = db_centroid_ids[corrs[:, 1]]

            # Generate hypotheses
            hypotheses = generate_hypotheses(
                coordx1,
                coordy1,
                scales1,
                coordx2,
                coordy2,
                scales2,
            )

            # Compute errors and verify models
            errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
            verifications = verify_models(errors, corrs)

            # Local optimization of the best model
            sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()

            total_support = -1
            total_mask = None
            total_A = None

            for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):
                A = hypotheses[best_hypothesis_index]
                mask = verifications[best_hypothesis_index]

                support = (mask * _similarities).sum()

                while True:
                    new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
                    errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                    new_mask = verify_models(errors, corrs)[0]
                    new_support = (new_mask * _similarities).sum()

                    if new_support > support:
                        support = new_support
                        A = new_A
                        mask = new_mask
                    else:
                        break

                if support > total_support:
                    total_support = support
                    total_A = A
                    total_mask = mask

            # Save data to investigate
            supports[enum_index] = total_support
        
        new_order = (-supports).argsort()
        ranks_copy[qi, :MAX_SPATIAL] = ranks[qi, new_order]
        
    return ranks_copy
    

def evaluate(
    filepath, desc, qvecs_centroid_ids, q_residual_packs, dbvecs_centroid_ids, db_residual_packs, gnd, all_thresholds
):
    data = np.load(filepath)
    vw_tc = data['vw_tc']
    vw_inl = data['vw_inl']
    
    ret = {
        'asmkE': [],
        'asmkM': [],
        'asmkH': [],
        'svE': [],
        'svM': [],
        'svH': [],
    }
    
    inl_tc_ratio = vw_inl / vw_tc.clip(1)
    for threshold in all_thresholds:
        vw_to_remove = np.flatnonzero(inl_tc_ratio < threshold)
        include_mask = ~np.isin(qvecs_centroid_ids, vw_to_remove)

        query_iterator = DataIterator(desc, qvecs_centroid_ids, q_residual_packs, 'q', include_mask=include_mask)
        db_iterator = DataIterator(desc, dbvecs_centroid_ids, db_residual_packs)
    
        metadata, images, ranks, _scores = asmk_dataset.query_ivf(
            query_iterator.get_all_vecs(),
            query_iterator.get_all_imids(),
        )
        asmkE, asmkM, asmkH = compute_map_and_print(DATASET, ranks.T, gnd['gnd'], ret=True)
        ret['asmkE'].append(asmkE)
        ret['asmkM'].append(asmkM)
        ret['asmkH'].append(asmkH)

        sv_ranks = sv(query_iterator, db_iterator, ranks)
        svE, svM, svH = compute_map_and_print(DATASET, sv_ranks.T, gnd['gnd'], ret=True)
        ret['svE'].append(svE)
        ret['svM'].append(svM)
        ret['svH'].append(svH)
        
        print('Done:', threshold)
    
    return ret
    

#
# It takes too much time. If needed, I can compute it on some school servers, hopefully...
#
all_thresholds = [0.02, 0.05, 0.08, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4]
results = evaluate(
    "generated-data/8c-asmk-min-inl.npz",
    desc,
    qvecs_centroid_ids,
    q_residual_packs,
    dbvecs_centroid_ids,
    db_residual_packs,
    gnd,
    all_thresholds,
)

results['asmkE'].insert(0, 94.75)
results['asmkM'].insert(0, 78.19)
results['asmkH'].insert(0, 55.51)

results['svE'].insert(0, 90.59)
results['svM'].insert(0, 75.09)
results['svH'].insert(0, 51.01)

plt.plot([0] + all_thresholds, results['asmkE'], color='b', label='ASMK Easy')
plt.plot([0] + all_thresholds, results['svE'], color='b', linestyle='dashed', label='SV Easy')
plt.legend()
plt.savefig('9a-results-easy.png')

plt.plot([0] + all_thresholds, results['asmkM'], color='g', label='ASMK Medium')
plt.plot([0] + all_thresholds, results['svM'], color='g', linestyle='dashed', label='SV Medium')
plt.legend()
plt.savefig('9a-results-medium.png')

plt.plot([0] + all_thresholds, results['asmkH'], color='r', label='ASMK Hard')
plt.plot([0] + all_thresholds, results['svH'], color='r', linestyle='dashed', label='SV Hard')
plt.legend()
plt.savefig('9a-results-hard.png')
