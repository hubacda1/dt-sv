import matplotlib.pyplot as plt
import numpy as np
import json

with open('./output.txt', 'r') as f:
    data = [json.loads(d[12:]) for d in f.readlines() if d.startswith('JSON STATS:')]

diffs = [[], []]
best_supps = [[], []]
for d in data:
    cp = d['correct_pair']
    if cp is None: 
        continue

    diffs[cp].append(d['all_best_support'] - d['best_support'])
    best_supps[cp].append(d['best_support'])


def test():
    a = np.array(best_supps[1])
    b = np.array(diffs[1])

    print()
    print(b[a >= 20].mean())
    print(b[a < 20].mean())
    print()
    print(b[a >= 20].shape)
    print(b[a < 20].shape)
    print()
    print()

    m = b >= 20
    interest = a[m]

    print('Result:', (interest >= 50).mean())

    assert 0


# test()  

# bins = [-7.5, -5.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 6.5, 9.5, 12.5]
# ticks = [-7, -5, -3, -2, -1, -2, 0, 1, 2, 3, 5, 8, 11]
# labels = ['[...]', '[-6;-4]', -3, -2, -1, -2, 0, 1, 2, 3, '[4;6]', '[7;9]', '[...]']
bins = [-10.5, -5.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 6.5, 9.5, 12.5]
ticks = [-11, -8, -5, -3, -2, -1, -2, 0, 1, 2, 3, 5, 8, 11]
labels = ['[...]', '[-9;-7]', '[-6;-4]', -3, -2, -1, -2, 0, 1, 2, 3, '[4;6]', '[7;9]', '[...]']
fig, ax = plt.subplots(1, 2, figsize=(10, 4))

# [-10.5, -5.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 5.5, 10.5, 20.5]

# ax[0].hist(np.array(diffs[0]), bins=bins, color='#d62728', density=True, alpha=0.5, label='Irrelevant images')
# ax[0].hist(np.array(diffs[1]), bins=bins, color='#2ca02c', density=True, alpha=0.5, label='Relevant images')
ax[0].hist(np.array(diffs[0]).clip(min=-7, max=10), bins=bins, color='#d62728', density=True, alpha=0.5, label='Irrelevant images')
ax[0].hist(np.array(diffs[1]).clip(min=-7, max=10), bins=bins, color='#2ca02c', density=True, alpha=0.5, label='Relevant images')
ax[0].set_xticks(ticks=ticks, labels=labels)
ax[0].set_xlabel('Change in support')
ax[0].set_ylabel('Density')
ax[0].set_title('Without LO')
ax[0].legend()



with open('../_fix_corrs2affine_lines_v2_test__noweight__figure__graph_change_lo/output.txt', 'r') as f:
    data = [json.loads(d[12:]) for d in f.readlines() if d.startswith('JSON STATS:')]

diffs = [[], []]
best_supps = [[], []]
for d in data:
    cp = d['correct_pair']
    if cp is None: 
        continue

    diffs[cp].append(d['all_best_support'] - d['best_support'])
    best_supps[cp].append(d['best_support'])


print(ax[1].hist(np.array(diffs[0]).clip(min=-7, max=10), bins=bins, color='#d62728', density=True, alpha=0.5, label='Irrelevant images'))
print(ax[1].hist(np.array(diffs[1]).clip(min=-7, max=10), bins=bins, color='#2ca02c', density=True, alpha=0.5, label='Relevant images'))
ax[1].set_xticks(ticks=ticks, labels=labels)
ax[1].set_xlabel('Change in support')
ax[1].set_ylabel('Density')
ax[1].set_title('With LO')
ax[1].legend()

fig.tight_layout()
# plt.savefig("inliers-change-with-lines-both.png", bbox_inches='tight')
plt.show()
