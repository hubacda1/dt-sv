from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent))
assert Path('.').resolve() != Path(__file__).parent.resolve(), 'Please, run the script from the first folder, not from the subfolder.'

from collections import defaultdict
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from retrieval_101_bbox_score.local_spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## INPUT ARGS ##
################

assert len(sys.argv) == 3, 'Incorrect args...'

iSTART = int(sys.argv[1])
iEND = int(sys.argv[2])

OUTPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel/mid_results')
OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent.parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
# dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = np.load('../desc_vecs-quantized-dbvecs_centroid_ids.npy')

q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    qvecs_centroid_ids,
    dbvecs_centroid_ids,
    q_residual_packs,
    db_residual_packs,
])
"""
qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs, db_residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../../oxbuild_images')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }

        self.scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])
        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        q = ['', 'q'][query]

        ql = np.searchsorted(self.desc[f'{q}imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'{q}imids'], id, 'right')

        tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc[f'{q}vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[f'{q}vecs_centroid_ids'][ql:qr]
        scales = self.scales[self.desc[f'{q}scales'][ql:qr]]
        coordx = (self.desc[f'{q}coordx'][ql:qr] * 16 / scales) + tx
        coordy = (self.desc[f'{q}coordy'][ql:qr] * 16 / scales) + ty
        residuals = self.residual_packs[f'{q}residual_packs'][ql:qr]

        if bbox:
            mask = is_in_bbox(bbox, np.array([coordx, coordy]))
            return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        else:
            return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        raise KeyError(f'fname={fname} not in line database.')
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim



lines_db = io_helpers.load_pickle('_lines_database.pkl')
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                    db_residual_packs)

"""
qi = 0
q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)
fname = gnd['qimlist'][qi]

qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
plt.imshow(qimg, cmap='gray')
plt.plot(coordx, coordy, 'r.')
plt.show()
"""

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

print('Start spatial verification.')

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100
INLIER_THRESHOLD=62

ranks_weighted_lo = ranks.copy()
scores_weighted_lo = np.zeros([iEND - iSTART, MAX_SPATIAL], dtype=float)

for qi, fname in enumerate(gnd['qimlist'][iSTART:iEND], start=iSTART):  # query index

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)

    supports_weighted_lo = np.zeros([MAX_SPATIAL])

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)

        if not corrs.size:
            supports_weighted_lo[dbi] = 0
            print('[Not expected] No correspondences.')
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        """
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        """
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        """
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        if new_hypotheses.size:
            hypotheses = np.vstack([hypotheses, new_hypotheses])
        """

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        # verifications = verify_models(errors, corrs, inlier_threshold=INLIER_THRESHOLD)
        # Only simple thresholding
        verifications = errors < INLIER_THRESHOLD

        # Local optimization of the weighted best model
        # weighted_scores = -(verifications * _similarities[np.newaxis]).sum(axis=1)
        area_scores = -compute_area_scores(verifications, coordx1, coordy1, q_bbox)
        sorted_hypothesis_indexes = area_scores.argsort()
        keys = sorted_hypothesis_indexes[:10]

        # total_A, total_mask, total_support = local_optimization_of_list_of_models(
        #     hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, INLIER_THRESHOLD
        # )

        # Omit local optimisation
        total_A = hypotheses[keys[0]]
        total_mask = verifications[keys[0]]
        total_support = -area_scores[keys[0]]

        # Save the support
        supports_weighted_lo[dbi] += total_support

        # Log
        correct_pair = (gnd_imid in gnd['gnd'][qi]['easy']) or (gnd_imid in gnd['gnd'][qi]['hard'])
        if gnd_imid in gnd['gnd'][qi]['junk']:
            correct_pair = None
        print('JSON STATS:', json.dumps({
            'qi': qi,
            'dbi': dbi,
            'gnd_imid': int(gnd_imid),
            'correct_pair': correct_pair,
            'corrs': corrs.shape[0],
            'inliers_num': int(total_mask.sum()),
            'supports_weighted_lo': supports_weighted_lo[dbi],
        }))

    scores_weighted_lo[qi - iSTART, :] = supports_weighted_lo

    new_order = (-supports_weighted_lo).argsort(kind='stable')
    ranks_weighted_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]


#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

io_helpers.save_pickle(str(OUTPUT_FOLDER / f'{iSTART}-{iEND}.pkl'), {
    'ranks_weighted_lo': ranks_weighted_lo,
    'scores_weighted_lo': scores_weighted_lo,
})
