from pathlib import Path
from asmk import io_helpers
import numpy as np

print('Starting integration')

INPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel')

first_done = False

ranks_weighted_lo = None
scores_weighted_lo = np.zeros([70, 100], dtype=float)

for file in (INPUT_FOLDER / 'mid_results').glob('*.pkl'):
    ifrom, ito = [int(item) for item in file.stem.split('-')]
    data = io_helpers.load_pickle(file)

    if first_done is False:
        ranks_weighted_lo = data['ranks_weighted_lo']
        first_done = True
    else:
        ranks_weighted_lo[ifrom:ito] = data['ranks_weighted_lo'][ifrom:ito]

    scores_weighted_lo[ifrom:ito, :] = data['scores_weighted_lo']

io_helpers.save_pickle(INPUT_FOLDER / 'ranks_weighted_lo.pkl', ranks_weighted_lo)
io_helpers.save_pickle(INPUT_FOLDER / 'scores_weighted_lo.pkl', scores_weighted_lo)

print('Integration done')
