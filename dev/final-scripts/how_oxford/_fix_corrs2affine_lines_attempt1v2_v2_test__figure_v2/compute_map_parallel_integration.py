from pathlib import Path
from asmk import io_helpers

print('Starting integration')

INPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel')

first_done = False

ranks_results = None

for file in (INPUT_FOLDER / 'mid_results').glob('*.pkl'):
    ifrom, ito = [int(item) for item in file.stem.split('-')]
    data = io_helpers.load_pickle(file)

    if first_done is False:
        ranks_results = data['ranks_results']
        first_done = True
    else:
        ranks_results[ifrom:ito] = data['ranks_results'][ifrom:ito]

io_helpers.save_pickle(INPUT_FOLDER / 'ranks_results.pkl', ranks_results)

print('Integration done')
