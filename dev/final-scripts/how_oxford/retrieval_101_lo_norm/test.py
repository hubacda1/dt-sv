import numpy as np

coordx1 = np.array([1, 2, 3])
coordy1 = np.array([11, 12, 13])

coordx2 = np.array([21, 22, 23])
coordy2 = np.array([31, 32, 33])


def homogeneous(pts):
    # pts.shape == [2, N]
    assert len(pts.shape) == 2
    assert pts.shape[0] == 2
    return np.vstack([pts, np.ones([pts.shape[1]])])


def center_and_normalize_image_points(pts):
    centroid = pts.mean(axis=1)
    rms_mean_dist = np.sqrt(2 * np.mean(np.square(pts - centroid[:, np.newaxis])))
    rms_mean_dist = np.linalg.norm(pts - centroid[:, np.newaxis], axis=0).mean()

    norm_factor = np.sqrt(2) / rms_mean_dist

    transformation = np.array([
        [norm_factor, 0, -norm_factor * centroid[0]],
        [0, norm_factor, -norm_factor * centroid[1]],
        [0, 0, 1],
    ])
    transformed_pts = transformation[:2] @ homogeneous(pts)

    return transformation, transformed_pts

pts = np.vstack([coordx1, coordy1])
# pts.mean(axis=1)

print(pts)
print(pts.mean(axis=1))
print()
print()
norm_trans_1, pts_trans_1 = (center_and_normalize_image_points(pts))
print()
print(pts_trans_1)
print()






# zeros = np.zeros_like(coordx1[:, np.newaxis])
# ones = np.ones_like(coordx1[:, np.newaxis])
# eqs = np.vstack([
#     np.hstack([coordx1[:, np.newaxis], coordy1[:, np.newaxis], zeros, zeros, ones, zeros]),
#     np.hstack([zeros, zeros, coordx1[:, np.newaxis], coordy1[:, np.newaxis], zeros, ones]),
# ])
# sol = np.hstack([coordx2, coordy2])

# print(eqs)
# print()
# print(sol)
