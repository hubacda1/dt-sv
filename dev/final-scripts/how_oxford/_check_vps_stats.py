from asmk import io_helpers
import numpy as np

vps = io_helpers.load_pickle('_vp_database.pkl')

n_vertical = []
n_horizontal = []

no_vps = 0

for imname, vpimg in vps.items():
    nv = 0
    nh = 0

    if len(vpimg) == 0:
        no_vps += 1
        print(imname, 'has no vps.')

    for vp in vpimg:
        
        if vp['direction'] == 'VERTICAL':
            nv += 1
        elif vp['direction'] == 'HORIZONTAL':
            nh += 1

    if nv == 0:
        print('!!!!!!!!!', imname)

    n_vertical.append(nv)
    n_horizontal.append(nh)


n_vertical = np.array(n_vertical)
n_horizontal = np.array(n_horizontal)

print()
print()
print('==============================')
print('n_vertical', n_vertical.min(), n_vertical.max(), n_vertical.mean(), np.median(n_vertical))
print('n_horizontal', n_horizontal.min(), n_horizontal.max(), n_horizontal.mean(), np.median(n_horizontal))
print('no:', no_vps)
print('total:', len(vps))
print(no_vps / len(vps))
print()
print()
print('R1.1:', (n_horizontal == 0).sum())
print('R1.2:', (n_vertical == 0).sum())
print('R2:', ((n_horizontal == 0) & (n_vertical == 0)).sum())
print('R3:', (n_horizontal == 1).sum())
print('R4:', (n_horizontal <= 1).sum())
print('R5:', (n_horizontal > 1).mean())
print()
print()
