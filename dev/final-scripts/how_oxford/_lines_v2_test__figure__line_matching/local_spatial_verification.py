import numpy as np
from functools import partial
from itertools import cycle
from utilities import BBox
import cv2
import matplotlib.pyplot as plt
from asmk import hamming, functional

__all__ = [
    'homogeneous',
    'get_packed_residuals',
    'geometries_to_matrices',
    'generate_hypotheses',
    'generate_hypotheses_with_lines',
    'compute_errors',
    'verify_single_model',
    'verify_models',
    'affine_local_optimization',
    'get_tentative_correspondencies',
    'get_denormalizing_A',
    'AntoRH',
    'transform_points',
    'local_optimization_of_list_of_models',
    'local_optimization_of_list_of_models_thresholding',
    'build_qe_args',
    'is_in_bbox',
    'detect_lines',
    'get_line_equations',
    'project_on_line',
]


def homogeneous(pts):
    # pts.shape == [2, N]
    assert len(pts.shape) == 2
    assert pts.shape[0] == 2
    return np.vstack([pts, np.ones([pts.shape[1]])])


def get_packed_residuals(vecs, centroids, binary=True):
    residuals = vecs - centroids

    if binary:
        return hamming.binarize_and_pack_2D(residuals)
    else:
        return functional.normalize_vec_l2(residuals)


def get_line_equations(pt_lines):
    # Reshape tuples into two 2D points
    pt_lines = pt_lines.reshape(-1, 2, 2)

    # Create norm vectors by a directional vector with swapped axes and one of them multiplied by -1
    norm_vectors = np.roll(pt_lines[:, 1] - pt_lines[:, 0], 1, axis=1).astype(float)
    norm_vectors[:, 0] *= -1
    norm_vectors /= np.linalg.norm(norm_vectors, axis=1, keepdims=True)

    # Compute offsets, c parameter in "ax + by + c = 0"
    offsets = -(pt_lines[:, 0] * norm_vectors).sum(axis=1, keepdims=True)
    lines = np.hstack([norm_vectors, offsets])

    return lines


def detect_lines(image_path, bbox=None, min_length=None, max_length=None, display=False):
    # Read gray image
    img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    # Create default parametrization LSD
    lsd = cv2.createLineSegmentDetector(cv2.LSD_REFINE_NONE)

    # Detect lines in the image
    lines, _, _, _ = lsd.detect(img)
    lines = lines.squeeze()

    if bbox:
        is_in_bbx_1 = is_in_bbox(bbox, lines[:, :2].T)
        is_in_bbx_2 = is_in_bbox(bbox, lines[:, 2:].T)
        lines = lines[is_in_bbx_1 & is_in_bbx_2]

    if min_length:
        lines_2d = lines.reshape(-1, 2, 2)
        lengths = np.linalg.norm(lines_2d[:, 0] - lines_2d[:, 1], axis=1)
        mask = lengths >= min_length
        lines = lines[mask]

    if display:
        fig, ax = plt.subplots(1, 2)
        # plt.figure(figsize=(10, 7))
        ax[0].imshow(img, cmap='gray')
        ax[0].set_title(f'Num of lines: {lines.shape[0]}')
        for x1, y1, x2, y2 in lines.round().astype(int):
            ax[0].plot([x1, x2], [y1, y2])
        # plt.show()

    lines = group_line_segments(lines, img, max_line_distance=9, max_points_distance=5)

    if display:
        # plt.figure(figsize=(10, 7))
        ax[1].imshow(img, cmap='gray')
        ax[1].set_title(f'Num of lines: {lines.shape[0]}')
        for x1, y1, x2, y2 in lines.round().astype(int):
            ax[1].plot([x1, x2], [y1, y2])
        plt.show()

    if max_length:
        if display:
            fig, ax = plt.subplots(1, 2)
            ax[0].imshow(img, cmap='gray')
            ax[0].set_title(f'Num of lines: {lines.shape[0]}')
            for x1, y1, x2, y2 in lines.round().astype(int):
                ax[0].plot([x1, x2], [y1, y2])

        new_lines = []
        lines_2d = lines.reshape(-1, 2, 2)
        lengths = np.linalg.norm(lines_2d[:, 0] - lines_2d[:, 1], axis=1)
        idxs = np.where(lengths >= max_length)[0]
        for idx in idxs:
            x1, y1, x2, y2 = lines[idx]

            splits = int(lengths[idx] // max_length)
            x_splits = np.linspace(start=x1, stop=x2, num=splits + 2)
            y_splits = np.linspace(start=y1, stop=y2, num=splits + 2)

            for i in range(splits + 1):
                new_lines.append([x_splits[i], y_splits[i], x_splits[i + 1], y_splits[i + 1]])

        if idxs.size:
            lines = np.delete(lines, idxs, axis=0)
            lines = np.vstack([lines, new_lines])
            
        if display:
            ax[1].imshow(img, cmap='gray')
            ax[1].set_title(f'Num of lines: {lines.shape[0]}')
            for x1, y1, x2, y2 in lines.round().astype(int):
                ax[1].plot([x1, x2], [y1, y2])
            plt.show()

    return lines


def _detect_lines(image_path, blur_size=7, canny_low_thr=20, canny_high_thr=80, display=False):
    img = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)

    if display:
        img_copy = img.copy()

    if display:
        plt.imshow(img, cmap='gray')
        plt.title('Image in which lines will be found')
        plt.show()
        
    img = cv2.GaussianBlur(img, (blur_size, blur_size), 2)

    if display:
        plt.imshow(img, cmap='gray')
        plt.title('After Gaussian blur')
        plt.show()
        
    edges = cv2.Canny(img, canny_low_thr, canny_high_thr)

    if display:
        plt.imshow(img, cmap='gray')
        plt.imshow(edges, cmap='gray')
        plt.title('Edges')
        plt.show()

    lines = cv2.HoughLinesP(edges, rho=2, theta=2 * np.pi / 180, threshold=30, lines=np.array([]),
                            minLineLength=20, maxLineGap=5)
    assert lines.shape[1] == 1
    lines = lines.squeeze()
    
    if display:
        fig, ax = plt.subplots(1, 2)
        ax[0].imshow(img_copy, cmap='gray')
        for x1, y1, x2, y2 in lines:
            ax[0].plot([x1, x2], [y1, y2])
        ax[1].imshow(edges)
        plt.title(f'Line detection, num_of_lines={lines.shape[0]}')
        plt.show()

    # Group line segments
    assert 0
    lines = group_line_segments(lines, img)

    if display:
        plt.imshow(img_copy, cmap='gray')
        for x1, y1, x2, y2 in lines:
            plt.plot([x1, x2], [y1, y2])
        plt.title(f'Groupped lines, num_of_lines={lines.shape[0]}')
        plt.show()

    return lines


def project_on_line(line_segment, *pts_to_project):
    # line_segment = [[x1 y2], [x2 y2]]
    if line_segment.shape == (4,):
        line_segment = line_segment.reshape(2, 2)
    a, b = line_segment
    lsq = np.sum((b - a) ** 2)

    ret = []
    for pt in pts_to_project:
        t = np.sum((pt - a) * (b - a)) / lsq
        p = a + t * (b - a)

        ret.append((p, t))
    
    return ret


def group_line_segments(lines, img, max_line_distance=10, max_points_distance=100, preserve_original=True):

    while True:
        anything_joined = False

        # Iterate over all line segments and try to find another segment which is close enough
        for i in range(lines.shape[0]):
            db_line_eqs = get_line_equations(lines)
            
            # Compute the distances, ignore distance to the line segment itself
            distances_1 = np.abs(np.sum(np.array([*lines[i, :2], 1]) * db_line_eqs, axis=1))
            distances_2 = np.abs(np.sum(np.array([*lines[i, 2:], 1]) * db_line_eqs, axis=1))
            # assert np.isclose(distances_1[i], 0)
            # assert np.isclose(distances_2[i], 0)
            distances_1[i] = np.inf
            distances_2[i] = np.inf

            # Find planes which are close enough to both points
            close_enough_idxs = np.where((distances_1 <= max_line_distance) & (distances_2 <= max_line_distance))[0]

            # Iterate over the planes
            for ce_idx in close_enough_idxs:

                # Get the four points
                a, b = lines[i].reshape(2, 2)
                l = np.sqrt(np.sum((b - a) ** 2))

                # Project points from the second line segment onto the first line segment
                #
                # projected_point = a + t * (b - a)
                (p1, t1), (p2, t2) = project_on_line(lines[i].reshape(2, 2), *lines[ce_idx].reshape(2, 2))

                # Compute the distance of both points projections from the line segment
                def get_dist(t, l):
                    if t > 1:
                        return l * (t - 1)
                    if t < 0:
                        return - l * t
                    return 0
                
                d1 = get_dist(t1, l)
                d2 = get_dist(t2, l)

                # Check that the line segments are close enough to be joined
                if min(d1, d2) <= max_points_distance:

                    if preserve_original:
                        l1 = np.linalg.norm(lines[i, :2] - lines[i, 2:])
                        l2 = np.linalg.norm(lines[ce_idx, :2] - lines[ce_idx, 2:])
                        if l2 <= l1:
                            lines = np.delete(lines, ce_idx, axis=0)
                            
                            # Mark that two line segments have been corrected and repeat the process again
                            anything_joined = True
                            break
                        continue

                    # Now we have a, b, p1, p2 on one line. Sort them and take the outer most.
                    result = sorted([a, b, p1, p2], key=lambda x: tuple(x))

                    DISPLAY = False
                    if DISPLAY:
                        plt.imshow(img, cmap='gray')
                        x1, y1 = result[0]
                        x2, y2 = result[-1]
                        plt.plot([x1, x2], [y1, y2], color='gold')
                        x1, y1, x2, y2 = lines[i]
                        plt.plot([x1, x2], [y1, y2], color='red')
                        x1, y1, x2, y2 = lines[ce_idx]
                        plt.plot([x1, x2], [y1, y2], color='coral')
                        plt.title(f't1={t1:.4f}, t2={t2:.4f}, d1={d1:.2f}, d2={d2:.2f}')
                        plt.show()

                        cont = input('Continue? [Y/n]')
                        assert cont != 'n'

                    # Take the outer most points and replace the old segment by the new one
                    x1, y1 = result[0]
                    x2, y2 = result[-1]
                    lines[i] = np.array([x1, y1, x2, y2])

                    # Delete the old line segment
                    lines = np.delete(lines, ce_idx, axis=0)
            
                    # Mark that two line segments have been joined and repeat the process again
                    anything_joined = True
                    break

            if anything_joined:
                break

        if not anything_joined:
            break

    # Return the grouped line segments
    return lines


def is_in_bbox(bounding_box: BBox, pts: np.ndarray) -> np.ndarray:
    """Get mask filtering out points outside of the bounding box."""
    x, y, xx, yy = bounding_box.as_tuple
    mask = (
        (pts[0] >= x) &
        (pts[0] <= xx) &
        (pts[1] >= y) &
        (pts[1] <= yy)
    )
    return mask


def build_qe_args(DATABASE, supports, As, dbids, bbox):
    # Supports must be sorted
    QE_MIN_MATCH = 15
    QE_MIN_MATCH_2 = 8

    MAX_QE_KEYPOINTS = 1500  # approximately

    # Filter only those models which we want to use for query expansion
    verified = (supports >= QE_MIN_MATCH)
    num_of_verified_corrs = verified.sum()

    if num_of_verified_corrs >= 3:
        step = max(1, num_of_verified_corrs // 10)
        supports = supports[verified][0:num_of_verified_corrs:step]
        As = As[verified][0:num_of_verified_corrs:step]
        dbids = dbids[verified][0:num_of_verified_corrs:step]
    else:
        verified = (supports >= QE_MIN_MATCH_2)
        supports = supports[verified]
        As = As[verified]
        dbids = dbids[verified]

    # Gather all visual words, which should be used
    eq_visual_words, eq_coordx, eq_coordy, eq_scales, eq_vecs, eq_residuals = [], [], [], [], [], []
    for _supp, A, dbid in zip(supports, As, dbids):
        # Get geometries from the database
        
        vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(dbid)

        # Backproject geometries back to the query image
        backprojected_pts = transform_points(np.linalg.inv(A), coordx, coordy)
        in_bbox = is_in_bbox(bbox, backprojected_pts)

        vecs = vecs[in_bbox]
        centroid_ids = centroid_ids[in_bbox]
        coordx = backprojected_pts[0, in_bbox]
        coordy = backprojected_pts[1, in_bbox]
        scales = scales[in_bbox] / np.linalg.det(A)
        residuals = residuals[in_bbox]

        # Add new geometries to the expanded query
        eq_vecs.extend(vecs)
        eq_visual_words.extend(centroid_ids)
        eq_coordx.extend(coordx)
        eq_coordy.extend(coordy)
        eq_scales.extend(scales)
        eq_residuals.extend(residuals)

    assert eq_visual_words

    step = max(1, len(eq_visual_words) // MAX_QE_KEYPOINTS)
    return (
        np.array(eq_vecs),
        np.array(eq_vecs[::step]),
        np.array(eq_visual_words),
        np.array(eq_visual_words[::step]), 
        np.array(eq_coordx[::step]), 
        np.array(eq_coordy[::step]), 
        np.array(eq_scales[::step]),
        np.array(eq_residuals[::step]),
    )


def local_optimization_of_list_of_models(
    hypotheses, verifications, weights, corrs, coordx1, coordy1, scales1, coordx2, coordy2, scales2,
    inlier_threshold=32, eigenvals_threshold=5, scale_threshold=5, return_index=False
):
    total_support = -1
    total_A = None
    total_mask = None
    total_i = None

    for i, (A, mask) in enumerate(zip(hypotheses, verifications)):
        support = (mask * weights).sum()

        while True:
            new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
            errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
            scale_ratios = scales1 * np.linalg.det(new_A) / scales2
            new_mask = verify_models(errors, scale_ratios[np.newaxis], corrs, inlier_threshold=inlier_threshold,
                                     scale_threshold=scale_threshold)[0]
            new_support = (new_mask * weights).sum()

            if new_support > support:
                support = new_support
                A = new_A
                mask = new_mask
            else:
                break

        e1, e2, _one = np.linalg.eigvals(A)
        eigenvals_check = (1 / eigenvals_threshold <= (e1 / e2) <= eigenvals_threshold)

        if support > total_support and eigenvals_check:
            total_support = support
            total_A = A
            total_mask = mask
            total_i = i

    if total_A is None:
        print('WARNING: None of the models survived local optimisation, probably because of the eigenvalues check.')
        print('         Returning the best base model.')
        total_A = hypotheses[0]
        errors = compute_errors(total_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
        scale_ratios = scales1 * np.linalg.det(total_A) / scales2
        total_mask = verify_models(errors, scale_ratios[np.newaxis], corrs, inlier_threshold=inlier_threshold)[0]
        total_support = (total_mask * weights).sum()
        total_i = 0


    if return_index:
        return total_A, total_mask, total_support, total_i
    return total_A, total_mask, total_support


def local_optimization_of_list_of_models_thresholding(
    hypotheses, verifications, weights, corrs, coordx1, coordy1, scales1, coordx2, coordy2, scales2,
    inlier_threshold=32, eigenvals_threshold=5, scales_threshold=5, return_index=False
):
    total_support = -1
    total_A = None
    total_mask = None
    total_i = None

    for i, (A, mask) in enumerate(zip(hypotheses, verifications)):
        support = (mask * weights).sum()

        while True:
            try:
                new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
            except Exception:
                new_A = A
            errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
            # new_mask = verify_models(errors, corrs, inlier_threshold=inlier_threshold)[0]
            # Naive thresholding verification
            # TODO: This should not be global !!!
            new_mask = (errors < inlier_threshold)[0]
            new_support = (new_mask * weights).sum()

            if new_support > support:
                support = new_support
                A = new_A
                mask = new_mask
            else:
                break

        e1, e2, _one = np.linalg.eigvals(A)
        eigenvals_check = (1 / eigenvals_threshold <= (e1 / e2) <= eigenvals_threshold)

        if support > total_support and eigenvals_check:
            total_support = support
            total_A = A
            total_mask = mask
            total_i = i

    if total_support == -1:
        if return_index:
            return hypotheses[0], verifications[0], (verifications[0] * weights).sum(), 0
        return hypotheses[0], verifications[0], (verifications[0] * weights).sum()

    if return_index:
        return total_A, total_mask, total_support, total_i
    return total_A, total_mask, total_support


def geometries_to_matrices(indices, geometries):
        """This turns out to be the fastest way to construct the affine transformation matrix."""
        out = np.zeros([indices.shape[0], 3, 3])

        out[:, 0, 0] = geometries[indices][:, 2]
        #  [:, 0, 1] = 0
        out[:, 0, 2] = geometries[indices][:, 0]
        out[:, 1, :] = geometries[indices][:, [3, 4, 1]]
        #  [:, 2, 0] = 0
        #  [:, 2, 1] = 0
        out[:, 2, 2] = 1

        return out


def distance_to_line_segments(segments, point):
    ret = []

    for segment in segments:
        [(p, t)] = project_on_line(segment, point)
        if 0 <= t <= 1:
            line_eq = get_line_equations(segment)
            dist = np.abs(np.sum(np.array([*point, 1]) * line_eq))
        else:
            d1 = np.linalg.norm(point - segment[:2])
            d2 = np.linalg.norm(point - segment[2:])
            dist = min(d2, d1)
        
        ret.append(dist)

    return np.array(ret)


def compute_angle(segment1, segment2):
    if segment1.shape == (4,):
        segment1 = segment1.reshape(2, 2)
    if segment2.shape == (4,):
        segment2 = segment2.reshape(2, 2)

    v1 = segment1[0] - segment1[1]
    v2 = segment2[0] - segment2[1]
    cos = np.clip(np.abs(np.sum(v1 * v2) / (np.linalg.norm(v1) * np.linalg.norm(v2))), 0, 1)
    return np.rad2deg(np.arccos(cos))


def compute_segments_distances(segments_1, segments_2, fill=np.inf, max_angle=30, max_dist=100):
    
    
    def append_ones(a):
        shape = list(a.shape)
        shape[-1] += 1
        ret = np.ones(shape)
        ret[..., :-1] = a
        return ret
    

    if len(segments_1.shape) == 2:
        segments_1 = segments_1.reshape(-1, 2, 2)
    if len(segments_2.shape) == 2:
        segments_2 = segments_2.reshape(-1, 2, 2)

    l1 = segments_1.shape[0]
    l2 = segments_2.shape[0]

    line_eq_2 = get_line_equations(segments_2)
    # Shape: [num_1, num_2, 2_points_from_seg_1, 3d]
    distances_from_line = np.abs(np.sum(append_ones(segments_1)[:, np.newaxis, :, :] * line_eq_2[np.newaxis, :, np.newaxis, :], axis=-1))

    distances = np.zeros((l1, 2, l2))
    for i1 in range(l1):
        for i2 in range(l2):
            # Compute the angle
            angle = compute_angle(segments_1[i1], segments_2[i2])
            if angle > max_angle:
                distances[i1, :, i2] = fill
                continue

            # Compute the distance from the line
            max_dist_from_line = np.max(distances_from_line[i1, i2])
            if max_dist_from_line > max_dist:
                distances[i1, :, i2] = fill
                continue

            # Both points are close enough to the line which has similar angle
            [(_, t1), (_, t2)] = project_on_line(segments_2[i2], *segments_1[i1])

            if 0 <= t1 <= 1:
                distances[i1, 0, i2] = distances_from_line[i1, i2, 0]
            else:
                d1 = np.linalg.norm(segments_1[i1, 0] - segments_2[i2, 0])
                d2 = np.linalg.norm(segments_1[i1, 0] - segments_2[i2, 1])
                distances[i1, 0, i2] = min(d1, d2)

            if 0 <= t2 <= 1:
                distances[i1, 1, i2] = distances_from_line[i1, i2, 1]
            else:
                d1 = np.linalg.norm(segments_1[i1, 1] - segments_2[i2, 0])
                d2 = np.linalg.norm(segments_1[i1, 1] - segments_2[i2, 1])
                distances[i1, 1, i2] = min(d1, d2)

            if np.max(distances[i1, :, i2]) > max_dist:
                distances[i1, :, i2] = fill

    return distances


def transform_line_segments(matrix, segments):
    s = segments.reshape(-1, 2)
    transformed = transform_points(matrix, s[:, 0], s[:, 1])
    return transformed.T.reshape(-1, 4)


def match_segments(distances, max_dist=35):
    distances = distances.mean(axis=1)
    corrs = []
    while True:
        position = np.unravel_index(distances.argmin(), distances.shape)
        if distances[position] > max_dist:
            break

        corrs.append(position)
        distances[position[0], :] = np.inf
        distances[:, position[1]] = np.inf

    return np.array(corrs)


def all_parallel(segments, threshold=45):
    n = segments.shape[0]
    assert n >= 2
    for base_i in range(n):
        for other_i in range(n):
            if compute_angle(segments[base_i], segments[other_i]) >= threshold:
                return False
    return True


def find_close_lines(x, y, lines, line_eqs=None, max_dist=50):
    # `lines` and `line_eqs` are supposed to be "the same", but the equations can be precomputed, therefore passed
    # independently
    if line_eqs is None:
        line_eqs = get_line_equations(lines)

    _pt = np.array([x, y])
    pt = np.array([x, y, 1])

    # Find close lines, ignore particular segments
    q_distances = np.abs(np.sum(pt * line_eqs, axis=1))
    close_enough = q_distances <= max_dist

    # Iterate over found close lines and keep only close segments
    close_lines = []
    for idx in np.where(close_enough)[0]:
        [(_, t)] = project_on_line(lines[idx], _pt)

        # If projection right on segment or close enough to a border point
        if 0 <= t <= 1 or np.linalg.norm(_pt - lines[idx, :2]) <= max_dist or np.linalg.norm(_pt - lines[idx, 2:]) <= max_dist:
            close_lines.append(lines[idx])
    
    return np.array(close_lines)


# PointAndLineTransformationEstimator
class PALTE:

    def __init__(self):
        pass

    def create_equestions(self, x1, y1, s1, x2, y2, s2, lines1, lines2):
        raise NotImplemented
    
    def build_matrix(self, *args, **kwargs):
        raise NotImplemented
    
    def estimate(self, x1, y1, s1, x2, y2, s2, lines1, lines2):
        eqs, rh_side = self.create_equestions(x1, y1, s1, x2, y2, s2, lines1, lines2)
        solution = np.linalg.lstsq(eqs, rh_side, rcond=None)[0]
        return self.build_matrix(*solution)
    
    def _solve_by_svd(equations, rh_side):
        """Alternative tp np.linalg.lstsq."""
        assert False, 'Use self.build_matrix'
        u, s, v = np.linalg.svd(equations)
        y = (u.T @ rh_side)[:s.shape[0]] / s
        x = v.T @ y
        return np.array([*x, 0, 0, 1]).reshape(3, 3)
    
    @staticmethod
    def center_and_normalize_image_points(pts):
        centroid = pts.mean(axis=1)
        rms_mean_dist = np.sqrt(2 * np.mean(np.square(pts - centroid[:, np.newaxis])))
        rms_mean_dist = np.linalg.norm(pts - centroid[:, np.newaxis], axis=0).mean()

        norm_factor = np.sqrt(2) / rms_mean_dist

        transformation = np.array([
            [norm_factor, 0, -norm_factor * centroid[0]],
            [0, norm_factor, -norm_factor * centroid[1]],
            [0, 0, 1],
        ])
        transformed_pts = transformation[:2] @ homogeneous(pts)

        return transformation, transformed_pts
    

class PALTE1Line2Scales(PALTE):

    def create_equestions(self, x1, y1, s1, x2, y2, s2, lines1, lines2):
        equations = [
            [x1,  0, 1, 0],
            [ 0, y1, 0, 1],
            [ 0,  0, 1, 0],
            [ 0,  0, 0, 1],
        ]
        rh_side = [x2, y2, s2 / s1, s2 / s1]

        for (u1, v1, u2, v2), (p, q, r) in zip(lines1, get_line_equations(lines2)):
            equations.append([u1 * p, v1 * q, p, q])
            equations.append([u2 * p, v2 * q, p, q])
            rh_side.append(-r)
            rh_side.append(-r)

        return np.array(equations), np.array(rh_side)
    
    def build_matrix(self, sx, sy, tx, ty):
        return np.array([
            [sx, 0, tx],
            [0, sy, ty],
            [0, 0, 1],
        ])


class PALTE1Line1Scale(PALTE):

    def create_equestions(self, x1, y1, s1, x2, y2, s2, lines1, lines2):

        pts_to_normalize1 = np.hstack([lines1.reshape(-1, 2).T, [[x1], [y1]]])
        pts_to_normalize2 = np.hstack([lines2.reshape(-1, 2).T, [[x2], [y2]]])

        self.norm_trans_1, pts_trans_1 = self.center_and_normalize_image_points(pts_to_normalize1)
        self.norm_trans_2, pts_trans_2 = self.center_and_normalize_image_points(pts_to_normalize2)

        lines1 = pts_trans_1[:, :-1].T.reshape(-1, 4)
        x1, y1 = pts_trans_1[:, -1]

        lines2 = pts_trans_2[:, :-1].T.reshape(-1, 4)
        x2, y2 = pts_trans_2[:, -1]

        ##########################################################################################
        
        equations = [
            [x1, 1, 0],
            [y1, 0, 1],
            [0, 0, 1],
        ]
        rh_side = [x2, y2, s2 / s1]

        for (u1, v1, u2, v2), (p, q, r) in zip(lines1, get_line_equations(lines2)):
            equations.append([u1 * p + v1 * q, p, q])
            equations.append([u2 * p + v2 * q, p, q])
            rh_side.append(-r)
            rh_side.append(-r)

        return np.array(equations), np.array(rh_side)
    
    def build_matrix(self, s, tx, ty):
        r = np.array([
            [s, 0, tx],
            [0, s, ty],
            [0, 0, 1],
        ])
        r = np.linalg.inv(self.norm_trans_2) @ r @ self.norm_trans_1
        return r
    

class PALTENLines(PALTE):

    def create_equestions(self, x1, y1, _s1, x2, y2, _s2, lines1, lines2):

        pts_to_normalize1 = np.hstack([lines1.reshape(-1, 2).T, [[x1], [y1]]])
        pts_to_normalize2 = np.hstack([lines2.reshape(-1, 2).T, [[x2], [y2]]])

        self.norm_trans_1, pts_trans_1 = self.center_and_normalize_image_points(pts_to_normalize1)
        self.norm_trans_2, pts_trans_2 = self.center_and_normalize_image_points(pts_to_normalize2)

        lines1 = pts_trans_1[:, :-1].T.reshape(-1, 4)
        x1, y1 = pts_trans_1[:, -1]

        lines2 = pts_trans_2[:, :-1].T.reshape(-1, 4)
        x2, y2 = pts_trans_2[:, -1]

        ##########################################################################################

        equations = [
            [x1, y1, 1,  0,  0, 0],
            [ 0,  0, 0, x1, y1, 1],
        ]
        rh_side = [x2, y2]

        for (u1, v1, u2, v2), (p, q, r) in zip(lines1, get_line_equations(lines2)):
            equations.append([p * u1, p * v1, p, q * u1, q * v1, q])
            equations.append([p * u2, p * v2, p, q * u2, q * v2, q])
            rh_side.append(-r)
            rh_side.append(-r)
        
        return np.array(equations), np.array(rh_side)
    
    def build_matrix(self, a, b, tx, d, e, ty):
        r = np.array([
            [a, b, tx],
            [d, e, ty],
            [0, 0, 1],
        ])
        r = np.linalg.inv(self.norm_trans_2) @ r @ self.norm_trans_1
        return r


def estimate_transformation_from_point_and_lines(
    close_lines_query, 
    close_lines_db,
    A, x1, y1, s1, x2, y2, s2,
):
    def get_PALTE(close_lines, corresponding_pairs, cp_idx=0):
        num_cp = corresponding_pairs.shape[0]
        if num_cp == 0:
            return None
        if num_cp == 1:
            return PALTE1Line1Scale
        if all_parallel(close_lines[corresponding_pairs[:, cp_idx]]):
            return PALTE1Line1Scale
        return PALTENLines


    close_lines_query_in_db = transform_line_segments(A, close_lines_query)
    distances = compute_segments_distances(close_lines_query_in_db, close_lines_db)
    corresponding_pairs = match_segments(distances)

    palte_cls = get_PALTE(close_lines_query, corresponding_pairs)
    if palte_cls is None:
        return None, None
    
    new_trans = palte_cls().estimate(
        x1, y1, s1, x2, y2, s2,
        close_lines_query[corresponding_pairs[:, 0]],
        close_lines_db[corresponding_pairs[:, 1]],
    )

    # Check whether it is possible to even improve
    best_num_cp = corresponding_pairs.shape[0]
    best_transformation = new_trans
    best_corresponding_pairs = corresponding_pairs
    while True:
        close_lines_query_in_db = transform_line_segments(new_trans, close_lines_query)
        distances = compute_segments_distances(close_lines_query_in_db, close_lines_db)
        corresponding_pairs = match_segments(distances)

        if corresponding_pairs.shape[0] <= best_num_cp:
            break

        palte_cls = get_PALTE(close_lines_query, corresponding_pairs)
        if palte_cls is None:
            break

        best_num_cp = corresponding_pairs.shape[0]
        best_transformation = new_trans
        best_corresponding_pairs = corresponding_pairs

        new_trans = palte_cls().estimate(
            x1, y1, s1, x2, y2, s2,
            close_lines_query[corresponding_pairs[:, 0]],
            close_lines_db[corresponding_pairs[:, 1]],
        )
    
    return best_transformation, best_corresponding_pairs
            

def generate_hypotheses_with_lines(
    coordx1, coordy1, scales1, coordx2, coordy2, scales2, lines_1, lines_2, qimg, dbimg, bbx, display=True, display_final=True,
):
    MAX_DIST = 50

    # 1 -> 2
    m = np.tile(np.eye(3), [coordx1.size, 1, 1])
    s = scales2 / scales1

    # s = s * 0.2

    m[:, 0, 2] = -1 * coordx1 * s + coordx2
    m[:, 1, 2] = -1 * coordy1 * s + coordy2
    m[:, 0, 0] = m[:, 1, 1] = s

    qh, qw, _ = qimg.shape
    dbh, dbw, _ = dbimg.shape
    colors = cycle(['#ff7f0e', '#17becf', '#9467bd', '#2ca02c', '#1f77b4', '#d62728', '#8c564b', '#e377c2', '#bcbd22', '#7f7f7f'])

    img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
    img[:qh, :qw] = qimg
    img[:dbh, qw:] = dbimg

    new_hypotheses = []
    q_line_eqs = get_line_equations(lines_1)
    db_line_eqs = get_line_equations(lines_2)
    for __i, (A, x1, y1, s1, x2, y2, s2) in enumerate(zip(m, coordx1, coordy1, scales1, coordx2, coordy2, scales2)):

        # if not 60 <= __i <= 63: continue
        if not 61 <= __i <= 63: continue

        close_lines_query = find_close_lines(x1, y1, lines_1, q_line_eqs, MAX_DIST)
        close_lines_db = find_close_lines(x2, y2, lines_2, db_line_eqs, MAX_DIST)

        if not close_lines_db.size or not close_lines_query.size:
            # print('No detected near lines in once of the images.')
            continue

        #
        # Plot close lines
        #
        if display:
            fig, ax = plt.subplots(1, 2)
            ax[0].plot(x1, y1, 'r.')
            ax[0].imshow(qimg, cmap='gray')
            for _x1, _y1, _x2, _y2 in close_lines_query:
                ax[0].plot([_x1, _x2], [_y1, _y2], 'r-')
            ax[1].plot(x2, y2, 'r.')
            ax[1].imshow(dbimg, cmap='gray')
            for _x1, _y1, _x2, _y2 in close_lines_db:
                ax[1].plot([_x1, _x2], [_y1, _y2], 'r-')
            plt.show()


        #
        # Compute the improved transformation
        #
        new_A, corresponding_pairs = estimate_transformation_from_point_and_lines(
            close_lines_query,
            close_lines_db,
            A, x1, y1, s1, x2, y2, s2,
        )
        if new_A is None:
            continue
        new_hypotheses.append(new_A)

        if corresponding_pairs.shape[0] > 1:
            fig, ax = plt.subplots(1, 2, figsize=(10, 5))

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            PADDING = 75
            
            img = np.ones([max(qh, dbh), qw + dbw + PADDING, 3], dtype=int) * 255
            img[:qh, :qw] = qimg
            img[:dbh, -dbw:] = dbimg

            for xxx in ax:
                xxx.imshow(img, cmap='gray')
                xxx.plot(x1, y1, 'r.')
                xxx.plot(x2 + qw + PADDING, y2, 'r.')
                xxx.set_yticks([])
                xxx.set_xticks([])

            ax[0].set_title('(a)')
            ax[1].set_title('(b)')

            for _x1, _y1, _x2, _y2 in close_lines_query:
                ax[0].plot([_x1, _x2], [_y1, _y2], 'r-')
            for _x1, _y1, _x2, _y2 in close_lines_db:
                ax[0].plot([_x1 + qw + PADDING, _x2 + qw + PADDING], [_y1, _y2], color='red')

            close_lines_query_in_db = transform_line_segments(A, close_lines_query)
            for _x1, _y1, _x2, _y2 in close_lines_query_in_db:
                ax[0].plot([_x1 + qw + PADDING, _x2 + qw + PADDING], [_y1, _y2], color='gold')


        #
        # Plot the correspondences
        #
        if corresponding_pairs.shape[0] > 1:
            # plt.imshow(img)
            # plt.plot(x1, y1, 'r.')
            # plt.plot(x2 + qw, y2, 'r.')
            # for c, (qidx, dbidx) in zip(colors, corresponding_pairs):
            #     _x1, _y1, _x2, _y2 = close_lines_query[qidx]
            #     plt.plot([_x1, _x2], [_y1, _y2], color=c)
            #     _x1, _y1, _x2, _y2 = close_lines_db[dbidx]
            #     plt.plot([_x1 + qw, _x2 + qw], [_y1, _y2], color=c)
            # plt.show()

            for c, (qidx, dbidx) in zip(colors, corresponding_pairs):
                _x1, _y1, _x2, _y2 = close_lines_query[qidx]
                ax[1].plot([_x1, _x2], [_y1, _y2], color=c)
                _x1, _y1, _x2, _y2 = close_lines_db[dbidx]
                ax[1].plot([_x1 + qw + PADDING, _x2 + qw + PADDING], [_y1, _y2], color=c)

            fig.tight_layout()
            plt.savefig("line-matching-2.png", bbox_inches='tight')
            assert 0

            print('__i', __i)
            plt.show()
            assert 0
            cont = input('Continue? ')
            assert cont != 'n'

            # assert 0

        #
        # Plot the transformed points
        #
        def plot_bbox(bbox, ax1, ax2, A=None, Acol='red'):
            x1, y1, x2, y2 = bbox.as_tuple
            ax1.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

            if A is not None:
                tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                ax2.plot(tr_coords_A[0], tr_coords_A[1], color=Acol)


        if display or display_final:
            fig, ax = plt.subplots(1, 2)
            ax[0].imshow(qimg)
            ax[0].plot([x1], [y1], 'r.')
            plot_bbox(bbx, ax[0], ax[1], A, 'gold')
            plot_bbox(bbx, ax[0], ax[1], new_A, 'lime')
            for _x1, _y1, _x2, _y2 in close_lines_query[corresponding_pairs[:, 0]]:
                ax[0].plot([_x1, _x2], [_y1, _y2], 'r-')

                tx2, ty2 = transform_points(A, [_x1, _x2], [_y1, _y2])
                ax[1].plot(tx2, ty2, color='gold')

                tx2, ty2 = transform_points(new_A, [_x1, _x2], [_y1, _y2])
                ax[1].plot(tx2, ty2, color='lime')

            ax[1].imshow(dbimg)
            ax[1].plot([x2], [y2], 'r.')
            ax[1].plot(*transform_points(new_A, [x1], [y1]), color='lime', marker='.')
            for _x1, _y1, _x2, _y2 in close_lines_db[corresponding_pairs[:, 1]]:
                ax[1].plot([_x1, _x2], [_y1, _y2], 'r-')
            plt.show()


    return m, np.array(new_hypotheses)


def generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2):
    # 1 -> 2
    m = np.tile(np.eye(3), [coordx1.size, 1, 1])
    s = scales2 / scales1

    m[:, 0, 2] = -1 * coordx1 * s + coordx2
    m[:, 1, 2] = -1 * coordy1 * s + coordy2
    m[:, 0, 0] = m[:, 1, 1] = s

    return m


def transform_points(matrix, coordx, coordy):
    pts = np.vstack([coordx, coordy, np.ones_like(coordx)])
    transformed_pts = matrix @ pts
    transformed_pts /= transformed_pts[..., -1:, :]
    return transformed_pts[..., :-1, :]


def compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2):
    pts2 = np.vstack([coordx2, coordy2])
    
    # [number of hypotheses, 3, number of correspondences]
    transformed_pts1 = transform_points(hypotheses, coordx1, coordy1)
    
    # [number of hypotheses, number of correspondences]
    return np.linalg.norm(transformed_pts1 - pts2, axis=1)


def verify_single_model(errors, scale_ratios, corrs, inlier_threshold=32, scale_threshold=11):
    n = corrs.shape[0]
    mask = np.zeros([n], dtype=bool)
    taken = set()

    i = 0
    actual_y, best_index = None, None
    best_error = None
    while i < n:
        actual_y = corrs[i, 1]

        best_error = np.inf
        best_index = -1
        while i < n and actual_y == corrs[i, 1]:
            if errors[i] < best_error and (1 / scale_threshold <= scale_ratios[i] <= scale_threshold) and corrs[i, 0] not in taken:
                best_error = errors[i]
                best_index = i
            i += 1

        if best_error <= inlier_threshold:
            mask[best_index] = True
            taken.add(corrs[best_index, 0])

    return mask


def verify_models(errors, scales_ratios, corrs, inlier_threshold=32, scale_threshold=11):
    # ret = np.apply_along_axis(
    #     partial(verify_single_model, corrs=corrs, inlier_threshold=inlier_threshold),
    #     axis=1,
    #     arr=errors,
    # )

    n_hypotheses = errors.shape[0]
    ret = np.zeros_like(errors, dtype=bool)
    for i in range(n_hypotheses):
        ret[i] = verify_single_model(errors[i], scales_ratios[i], corrs=corrs, inlier_threshold=inlier_threshold, scale_threshold=scale_threshold)

    return ret


def affine_local_optimization(A, coordx1, coordy1, coordx2, coordy2):
    size = coordx1.size

    if size < 3:
        return A

    weight = 2 * np.pi if size < 11 else 0
    r2h = 50.0  # half of squared circle radius is integrated over

    mx1, my1 = coordx1.mean(), coordy1.mean()
    mx2, my2 = coordx2.mean(), coordy2.mean()

    dx1, dy1 = (coordx1 - mx1), (coordy1 - my1)
    dx2, dy2 = (coordx2 - mx2), (coordy2 - my2)

    # Compute AtA, AtB1, AtB2
    AtA = [0.0, 0.0, 0.0]
    AtB1 = [0.0, 0.0]
    AtB2 = [0.0, 0.0]

    AtA[0] = ((1 + weight) * dx1 * dx1 + weight * r2h).sum()
    AtA[1] = ((1 + weight) * dx1 * dy1).sum()
    AtA[2] = ((1 + weight) * dy1 * dy1 + weight * r2h).sum()

    AtB1[0] = ((1 + weight) * dx1 * dx2 + weight * A[0, 0] * r2h).sum()
    AtB1[1] = ((1 + weight) * dy1 * dx2 + weight * A[0, 1] * r2h).sum()

    AtB2[0] = ((1 + weight) * dx1 * dy2 + weight * A[1, 0] * r2h).sum()
    AtB2[1] = ((1 + weight) * dy1 * dy2 + weight * A[1, 1] * r2h).sum()

    # Final affine transformation
    detAtA = AtA[0] * AtA[2] - AtA[1] * AtA[1]
    if detAtA == 0:
        raise ValueError('Determinant equals to zero.')

    norm = 1 / detAtA
    H0 = (AtA[2] * AtB1[0] - AtA[1] * AtB1[1]) * norm
    H1 = (-AtA[1] * AtB1[0] + AtA[0] * AtB1[1]) * norm
    H2 = mx2 - mx1 * H0 - my1 * H1
    H3 = (AtA[2] * AtB2[0] - AtA[1] * AtB2[1]) * norm
    H4 = (-AtA[1] * AtB2[0] + AtA[0] * AtB2[1]) * norm
    H5 = my2 - mx1 * H3 - my1 * H4

    H = np.array([
        [H0, H1, H2],
        [H3, H4, H5],
        [0, 0, 1],
    ])

    return H


def get_tentative_correspondencies(
    q_ids, db_ids, q_residual_packs, db_residual_packs, similarity_func, max_tc=1500, max_MxN=15,
):
    db_unique, db_counts = np.unique(db_ids, return_counts=True)
    q_unique, q_counts = np.unique(q_ids, return_counts=True)

    db_sorted = np.argsort(db_ids)
    q_sorted = np.argsort(q_ids)

    ret = []
    similarities = []
    # counts = []

    qr_i = 0  # query index
    db_i = 0  # database index
    s_qr_i = 0  # sorted query index
    s_db_i = 0  # sorted database index

    qr_len = q_unique.shape[0]
    db_len = db_unique.shape[0]
    s_qry_len = q_sorted.shape[0]
    s_rel_len = db_sorted.shape[0]

    while qr_i < qr_len and db_i < db_len:  # While there are any visual words remaining
        if q_unique[qr_i] == db_unique[db_i]:  # If we encountered the the same visual word
            # count = q_counts[qr_i] * db_counts[db_i]
            # if count <= max_MxN:
            if True:

                # Move the sorted indexes forward so we find the beginning of current visual word
                while s_qr_i < s_qry_len and q_ids[q_sorted[s_qr_i]] != q_unique[qr_i]:
                    s_qr_i += 1
                while s_db_i < s_rel_len and db_ids[db_sorted[s_db_i]] != db_unique[db_i]:
                    s_db_i += 1

                # Cross products of corresponding visual words
                count = q_counts[qr_i] * db_counts[db_i]
                cross_product_correspondences = np.zeros([count, 2])
                cross_product_similarities = np.zeros([count])
                cross_product_index = 0

                s_qr_i_start = s_qr_i
                while s_db_i < s_rel_len and db_ids[db_sorted[s_db_i]] == db_unique[db_i]:
                    s_qr_i = s_qr_i_start
                    while s_qr_i < s_qry_len and q_ids[q_sorted[s_qr_i]] == q_unique[qr_i]:
                        #ret.append([q_sorted[s_qr_i], db_sorted[s_db_i]])
                        #similarities.append(similarity_func(
                        #    q_residual_packs[q_sorted[s_qr_i]], db_residual_packs[db_sorted[s_db_i]]
                        #)[0])
                        #vw_ids.append(q_unique[qr_i])

                        cross_product_correspondences[cross_product_index] = [q_sorted[s_qr_i], db_sorted[s_db_i]]
                        cross_product_similarities[cross_product_index] = similarity_func(
                            q_residual_packs[q_sorted[s_qr_i]], db_residual_packs[db_sorted[s_db_i]]
                        )[0]
                        cross_product_index += 1

                        s_qr_i += 1
                    s_db_i += 1

                if count <= max_MxN:
                    mask = np.ones_like(cross_product_similarities, dtype=bool)
                else:
                    threshold = -np.sort(-cross_product_similarities)[max_MxN]
                    mask = cross_product_similarities >= threshold

                ret.extend(cross_product_correspondences[mask][:max_MxN])
                similarities.extend(cross_product_similarities[mask][:max_MxN])

            qr_i += 1
            db_i += 1
        elif q_unique[qr_i] < db_unique[db_i]:
            qr_i += 1
        else:
            db_i += 1

    ret_np = np.array(ret, dtype=int, ndmin=2)

    # If there are way too many correspondences, crop the result
    similarities_np = np.array(similarities)
    if ret_np.shape[0] > max_tc:
        # counts_np = np.array(counts)
        # keys = np.argsort(counts, kind='stable')[:max_tc]
        # return ret_np[keys]

        # If we do not care about the order
        #
        # keys = np.argsort(-similarities_np, kind='stable')[:max_tc]
        # return ret_np[keys], similarities_np[keys]

        # If we want to preserve the order (same visual words in a row)

        threshold = -np.sort(-similarities_np)[max_tc]
        mask = similarities_np >= threshold

        return ret_np[mask][:max_tc], similarities_np[mask][:max_tc]

    return ret_np, similarities_np


def get_denormalizing_A(x, y, s):
    return np.array([
        [s, 0, x],
        [0, s, y],
        [0, 0, 1],
    ])


def AntoRH(Ns, Ds):
    """
    ┌ COPIED METHOD 
    │
    ├ Webpage: https://cmp.felk.cvut.cz/~chum/pub_all.html
    ├ Article: Homography Estimation from Correspondences of Local Elliptical Features
    ├ Link:    [Code]
    └ File:    AntoRH.m
    """
    # Ds.shape = Ns.shape = [N, 3, 3]
    
    
    def trA(A):
        return np.array([*A[:2].ravel(), A[2, 2]])


    def getZ(u):
        Z = np.zeros((7, 9))
        Z[0, [0, 6]] = [-1, u[3]]
        Z[1, [1, 7]] = [-1, u[3]]
        Z[2, [2, 6, 7]] = [-1, -u[0] * u[3], -u[1] * u[3]]
        Z[3, [3, 6]] = [-1, u[4]]
        Z[4, [4, 7]] = [-1, u[4]]
        Z[5, [5, 6, 7]] = [-1, -u[0] * u[4], -u[1] * u[4]]
        Z[6, [8, 6, 7]] = [-1, -u[0], -u[1]]
        return Z
    
    def nd(A, B):
        A = A.ravel()
        B = B.ravel()
        Z = np.zeros((7, 3))
        Z[:, 2] = [0, 0, A[2], 0, 0, A[5], 1]
        Z[:, 0] = [A[1] * B[0] - A[0] * B[3], A[1] * B[1] - A[0] * B[4],
                   A[1] * B[2] - A[0] * B[5], A[4] * B[0] - A[3] * B[3],
                   A[4] * B[1] - A[3] * B[4], A[4] * B[2] - A[3] * B[5], 0]
        Z[:, 1] = [A[0] * B[0] + A[1] * B[3], A[0] * B[1] + A[1] * B[4],
                   A[0] * B[2] + A[1] * B[5], A[3] * B[0] + A[4] * B[3],
                   A[3] * B[1] + A[4] * B[4], A[3] * B[2] + A[4] * B[5], 0]
        return Z

    
    def p2e(u):
        e = u[:, 0:2] / (np.ones([1, 2]) * u[:, 2:3])
        return e
    
    def normu(u):
        if u.shape[1] == 3:
            u = p2e(u)

        m = np.mean(u, axis=0)
        u = u - m
        distu = np.sqrt(np.square(u).sum(axis=1))
        r = distu.mean() / np.sqrt(2)

        A = np.diag([1/r, 1/r, 1])
        A[:2, 2] = - m / r

        return A
    

    do_norm = True
    
    n = Ds.shape[0]
    u = np.hstack((np.linalg.inv(Ns)[:, :, 2], Ds[:, :, 2]))
    
    if do_norm:
        T1 = normu(u[:, :2])
        iT1 = np.linalg.inv(T1)
        T2 = normu(u[:, 3:5])
        
        Ns = Ns @ iT1
        Ds = T2 @ Ds
        
        u[:, :3] = (T1 @ u[:, :3].T).T
        u[:, 3:] = (T1 @ u[:, 3:].T).T

    Z = []
    for i in range(n):
        Zi = np.hstack([getZ(u[i]), np.zeros([7, 3 * i]), nd(Ds[i], Ns[i]), np.zeros([7, 3 * (n - i - 1)])])
        Z.append(Zi)
        
    Z = np.vstack(Z)
    _, _, v = np.linalg.svd(Z)
    
    H = v[-1, :9].reshape((3, 3))
    
    if do_norm:
        H = np.linalg.inv(T2) @ H @ T1
    
    return H
