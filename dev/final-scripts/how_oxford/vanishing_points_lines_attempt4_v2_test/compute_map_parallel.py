from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent))
assert Path('.').resolve() != Path(__file__).parent.resolve(), 'Please, run the script from the first folder, not from the subfolder.'

from collections import defaultdict
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from vanishing_points_lines_attempt4_v2_test.local_spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## INPUT ARGS ##
################

assert len(sys.argv) == 3, 'Incorrect args...'

iSTART = int(sys.argv[1])
iEND = int(sys.argv[2])

OUTPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel/mid_results')
OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent.parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
# dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = np.load('../desc_vecs-quantized-dbvecs_centroid_ids.npy')

q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    qvecs_centroid_ids,
    dbvecs_centroid_ids,
    q_residual_packs,
    db_residual_packs,
])
"""
qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs, db_residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../../oxbuild_images')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs, vps):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }
        self.vps = vps

        self.scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])
        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        q = ['', 'q'][query]

        ql = np.searchsorted(self.desc[f'{q}imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'{q}imids'], id, 'right')

        tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc[f'{q}vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[f'{q}vecs_centroid_ids'][ql:qr]
        scales = self.scales[self.desc[f'{q}scales'][ql:qr]]
        coordx = (self.desc[f'{q}coordx'][ql:qr] * 16 / scales) + tx
        coordy = (self.desc[f'{q}coordy'][ql:qr] * 16 / scales) + ty
        residuals = self.residual_packs[f'{q}residual_packs'][ql:qr]

        if bbox:
            mask = is_in_bbox(bbox, np.array([coordx, coordy]))
            return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        else:
            return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        raise KeyError(f'fname={fname} not in line database.')
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    def get_vps(self, fname):
        if fname not in self.vps:
            print(f'WARNING: VPs for {fname} not pre-computed.')
            return []
        return self.vps[fname]
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim



lines_db = io_helpers.load_pickle('_lines_database.pkl')
vps = io_helpers.load_pickle('_vp_database.pkl')
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                    db_residual_packs, vps)

"""
qi = 0
q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)
fname = gnd['qimlist'][qi]

qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
plt.imshow(qimg, cmap='gray')
plt.plot(coordx, coordy, 'r.')
plt.show()
"""

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

print('Start spatial verification.')

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100
INLIER_THRESHOLD = 62
SCALE_THRESHOLD = 11_000_000
VP_BONUS_SCORE = 4

all_total_masks = io_helpers.load_pickle('./prepare_masks_lines_attempt4_v2_test/compute_map_parallel/total_masks.pkl')
all_transformations = io_helpers.load_pickle('./prepare_masks_lines_attempt4_v2_test/compute_map_parallel/total_transformations.pkl')

ranks_results = ranks.copy()

for qi, fname in enumerate(gnd['qimlist'][iSTART:iEND], start=iSTART):  # query index

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)

    supports_results = np.zeros([MAX_SPATIAL])

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)

        if not corrs.size:
            supports_results[dbi] = 0
            print('[Not expected] No correspondences.')
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        # MIN_LINE_LENGTHS = 40
        # MAX_LINE_LENGTH = 110
        # q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        # db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        # qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        # dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        
        # Generate hypotheses
        # hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        # hypotheses, new_hypotheses = generate_hypotheses_with_lines(
        #     coordx1,
        #     coordy1,
        #     scales1,
        #     coordx2,
        #     coordy2,
        #     scales2,
        #     q_lines,
        #     db_lines,
        #     qimg,
        #     dbimg,
        #     q_bbox,
        #     display=False,
        #     display_final=False,
        # )
        # if new_hypotheses.size:
        #     hypotheses = np.vstack([hypotheses, new_hypotheses])

        # Compute errors and verify models
        # errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        # determinants = np.linalg.det(hypotheses)
        # scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
        # verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD, scale_threshold=SCALE_THRESHOLD)

        # # Local optimization of the weighted best model
        # weighted_scores = -(verifications * _similarities[np.newaxis]).sum(axis=1)
        # sorted_hypothesis_indexes = weighted_scores.argsort()
        # keys = sorted_hypothesis_indexes[:10]

        # total_A, total_mask, total_support = local_optimization_of_list_of_models(
        #     hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, scales1,
        #     coordx2, coordy2, scales2, inlier_threshold=INLIER_THRESHOLD, eigenvals_threshold=5, scale_threshold=SCALE_THRESHOLD
        # )

        # Save the support
        # supports_results[dbi] += total_support

        total_A = all_transformations[qi, dbi]
        total_mask = all_total_masks[qi][dbi]

        score_before_vps = (total_mask * _similarities).sum()
        supports_results[dbi] = score_before_vps

        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

        if False:

            def plot_bbox(bbox, ax, A=None, qw=None, Acol='red'):
                x1, y1, x2, y2 = bbox.as_tuple
                ax.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

                if A is not None and qw is not None:
                    tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                    ax.plot(tr_coords_A[0] + qw, tr_coords_A[1], color=Acol)


            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            PADDING = 75
            
            img = np.zeros([max(qh, dbh), qw + dbw + PADDING, 3], dtype=int) + 255
            img[:qh, :qw] = qimg
            img[:dbh, -dbw:] = dbimg

            plt.imshow(img)
            plot_bbox(q_bbox, plt, total_A, qw, Acol='red')
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
                plt.plot(
                    [qx, dbx + qw + PADDING],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

            assert 0



        ################################################################################################################
        ### VANISHING POINTS
        ################################################################################################################
        def get_kernel_from_vps(qhvp, qvvp, dbhvp, dbvvp):
            A_vp = np.array([
                [-qhvp[0], -qhvp[1], -1, 0, 0, 0, qhvp[0] * dbhvp[0], qhvp[1] * dbhvp[0], dbhvp[0]],
                [0, 0, 0, -qhvp[0], -qhvp[1], -1, qhvp[0] * dbhvp[1], qhvp[1] * dbhvp[1], dbhvp[1]],
                
                [-qvvp[0], -qvvp[1], -1, 0, 0, 0, qvvp[0] * dbvvp[0], qvvp[1] * dbvvp[0], dbvvp[0]],
                [0, 0, 0, -qvvp[0], -qvvp[1], -1, qvvp[0] * dbvvp[1], qvvp[1] * dbvvp[1], dbvvp[1]],
            ], dtype=float)
            U, S, Vh = np.linalg.svd(A_vp)

            return Vh.T[:, -5:]
        

        def center_and_normalize_image_points(pts):
            centroid = pts.mean(axis=1)
            # rms_mean_dist = np.sqrt(2 * np.mean(np.square(pts - centroid[:, np.newaxis])))
            rms_mean_dist = np.linalg.norm(pts - centroid[:, np.newaxis], axis=0).mean()

            norm_factor = np.sqrt(2) / rms_mean_dist

            transformation = np.array([
                [norm_factor, 0, -norm_factor * centroid[0]],
                [0, norm_factor, -norm_factor * centroid[1]],
                [0, 0, 1],
            ])
            transformed_pts = transformation[:2] @ homogeneous(pts)

            return transformation, transformed_pts
        

        def estimate_homography_with_hard_constraint_supernorm(vps1, vps2, cx1, cy1, cx2, cy2):

            pts_to_norm1 = np.hstack([
                np.array([cx1, cy1]),
                vps1,
            ])
            pts_to_norm2 = np.hstack([
                np.array([cx2, cy2]),
                vps2,
            ])

            norm_trans1, norm_pts1 = center_and_normalize_image_points(pts_to_norm1)
            norm_trans2, norm_pts2 = center_and_normalize_image_points(pts_to_norm2)

            constraint_matrix = get_kernel_from_vps(norm_pts1[:, -2], norm_pts1[:, -1], norm_pts2[:, -2], norm_pts2[:, -1])

            A_pts = []
            for x1, y1, x2, y2 in zip(*norm_pts1[:, :-2], *norm_pts2[:, :-2]):
                A_pts.append([-x1, -y1, -1, 0, 0, 0, x1 * x2, y1 * x2, x2])
                A_pts.append([0, 0, 0, -x1, -y1, -1, x1 * y2, y1 * y2, y2])
            A_pts = np.array(A_pts)

            A = A_pts @ constraint_matrix
            U, S, Vh = np.linalg.svd(A)
            alpha = Vh.T[:, -1]
            H = constraint_matrix @ alpha
            H = H.reshape(3, 3)

            model = np.linalg.inv(norm_trans2) @ H @ norm_trans1

            return model
        

        def generate_vps_combinations(vps1, vps2, min_support=7):
            vps1 = [vp for vp in vps1 if vp['coords'][-1] == 1 and vp['support'] >= min_support]
            vps2 = [vp for vp in vps2 if vp['coords'][-1] == 1 and vp['support'] >= min_support]

            if not vps1 or not vps2:
                return

            if vps1[0]['direction'] != 'VERTICAL' or vps2[0]['direction'] != 'VERTICAL':
                return
            
            v1 = vps1[0]
            v2 = vps2[0]

            for h1 in vps1[1:]:
                for h2 in vps2[1:]:
                    assert h1['direction'] == 'HORIZONTAL' and h2['direction'] == 'HORIZONTAL'
                    yield (
                        np.array(v1['coords'][:2]), 
                        np.array(h1['coords'][:2]), 
                        np.array(v2['coords'][:2]), 
                        np.array(h2['coords'][:2]),
                    )

        
        def estimate_homography_with_hard_constraint_supernorm_new(cx1, cy1, cx2, cy2, v1, h1, v2, h2):

            pts_to_norm1 = np.hstack([np.array([cx1, cy1]), h1[:, np.newaxis], v1[:, np.newaxis]])
            pts_to_norm2 = np.hstack([np.array([cx2, cy2]), h2[:, np.newaxis], v2[:, np.newaxis]])

            norm_trans1, norm_pts1 = center_and_normalize_image_points(pts_to_norm1)
            norm_trans2, norm_pts2 = center_and_normalize_image_points(pts_to_norm2)

            constraint_matrix = get_kernel_from_vps(norm_pts1[:, -2], norm_pts1[:, -1], norm_pts2[:, -2], norm_pts2[:, -1])

            A_pts = []
            for x1, y1, x2, y2 in zip(*norm_pts1[:, :-2], *norm_pts2[:, :-2]):
                A_pts.append([-x1, -y1, -1, 0, 0, 0, x1 * x2, y1 * x2, x2])
                A_pts.append([0, 0, 0, -x1, -y1, -1, x1 * y2, y1 * y2, y2])
            A_pts = np.array(A_pts)

            A = A_pts @ constraint_matrix
            U, S, Vh = np.linalg.svd(A)
            alpha = Vh.T[:, -1]
            H = constraint_matrix @ alpha
            H = H.reshape(3, 3)

            model = np.linalg.inv(norm_trans2) @ H @ norm_trans1

            model /= model[-1, -1]

            return model
        

        total_metadata = {
            'score_before_iters': None,
            'inliers_before_iters': None,
            'score_after': None,
            'inliers_after': None,
            'niter': None,
        }
        niter = None
        vanishing_points_used = False
        if total_mask.sum() >= 5:

            best_vp_score = -1
            best_vp_model = None
            best_vp_mask = None
            best_metadata = None

            for v1, h1, v2, h2 in generate_vps_combinations(DATABASE.get_vps(fname), DATABASE.get_vps(db_fname)):
                
                model = estimate_homography_with_hard_constraint_supernorm_new(
                    coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask], v1, h1, v2, h2
                )

                errors = compute_errors(model[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=INLIER_THRESHOLD)[0]
                weighted_score = (mask * _similarities).sum()

                local_metadata = {
                    'score_before_iters': float(weighted_score),
                    'inliers_before_iters': int(mask.sum()),
                }
                niter = 0

                while True:

                    if mask.sum() < 5:
                        break
                    
                    new_model = estimate_homography_with_hard_constraint_supernorm_new(
                        coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask], v1, h1, v2, h2
                    )

                    new_errors = compute_errors(new_model[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                    new_mask = verify_models(new_errors, corrs, inlier_threshold=INLIER_THRESHOLD)[0]
                    new_weighted_score = (new_mask * _similarities).sum()

                    if new_weighted_score > weighted_score:
                        niter += 1
                        weighted_score = new_weighted_score
                        model = new_model
                        mask = new_mask
                    else:
                        break

                local_metadata['score_after'] = float(weighted_score)
                local_metadata['inliers_after'] = int(mask.sum())
                local_metadata['niter'] = niter
                
                if weighted_score > best_vp_score:
                    best_vp_score = weighted_score
                    best_vp_model = model
                    best_vp_mask = mask
                    best_metadata = local_metadata

            if best_vp_model is not None:
                vanishing_points_used = True
                supports_results[dbi] = max(supports_results[dbi], best_vp_score + VP_BONUS_SCORE)
                total_mask = best_vp_mask
                total_metadata = best_metadata

        
        if False:

            print('Metadata')
            print(total_metadata)
            print()

            def plot_bbox(bbox, ax, A=None, qw=None, Acol='red'):
                x1, y1, x2, y2 = bbox.as_tuple
                ax.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

                if A is not None and qw is not None:
                    tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                    ax.plot(tr_coords_A[0] + qw, tr_coords_A[1], color=Acol)


            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            PADDING = 75
            
            img = np.zeros([max(qh, dbh), qw + dbw + PADDING, 3], dtype=int) + 255
            img[:qh, :qw] = qimg
            img[:dbh, -dbw:] = dbimg

            plt.imshow(img)
            plot_bbox(q_bbox, plt, total_A, qw, Acol='red')
            plot_bbox(q_bbox, plt, best_vp_model, qw, Acol='gold')
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
                plt.plot(
                    [qx, dbx + qw + PADDING],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
            plt.show()


        # Log
        correct_pair = (gnd_imid in gnd['gnd'][qi]['easy']) or (gnd_imid in gnd['gnd'][qi]['hard'])
        if gnd_imid in gnd['gnd'][qi]['junk']:
            correct_pair = None
        print('JSON STATS:', json.dumps({
            'qi': qi,
            'dbi': dbi,
            'gnd_imid': int(gnd_imid),
            'correct_pair': correct_pair,
            'corrs': corrs.shape[0],

            'score_before_vps': score_before_vps,
            'best_vp_score': best_vp_score,
            'niter': niter,
            'supports_results': supports_results[dbi],
            'metadata': total_metadata,
        }))

    new_order = (-supports_results).argsort()
    ranks_results[qi, :MAX_SPATIAL] = ranks[qi, new_order]

#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

io_helpers.save_pickle(str(OUTPUT_FOLDER / f'{iSTART}-{iEND}.pkl'), {
    'ranks_results': ranks_results,
})

