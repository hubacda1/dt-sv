items=(
    # "fix_corrs2affine_lines_attempt2_thresholding"
    "fix_corrs2affine_lines_attempt2_v2_test"
    # "fix_corrs2affine_lines_attempt3_thresholding"
    "fix_corrs2affine_lines_attempt3_v2_test"
    # "fix_corrs2affine_lines_thresholding"
    "fix_corrs2affine_lines_v2_test"
    # "fix_lines_attempt2_thresholding"
    "fix_lines_attempt2_v2_test"
    # "fix_lines_attempt3_thresholding"
    "fix_lines_attempt3_v2_test"
    # "fix_lines_thresholding"
    "fix_lines_v2_test"
    # "lines_attempt2_thresholding"
    "lines_attempt2_v2_test"
    # "lines_attempt3_thresholding"
    "lines_attempt3_v2_test"
    # "lines_thresholding"
    "lines_v2_test"
    # "retrieval_qe"
    "retrieval_v2_test"
)

for item in "${items[@]}"
do
    rm ./$item/output.txt
    rm -rf ./$item/compute_map_parallel/

    ./$item/compute_map_parallel_pipeline.sh > ./$item/output.txt
    
    echo "Done $item"
done


for item in "${items[@]}"
do
    echo "====================================================================="
    echo "=== $item"
    tail ./$item/output.txt
    echo "====================================================================="
    echo ""
done
