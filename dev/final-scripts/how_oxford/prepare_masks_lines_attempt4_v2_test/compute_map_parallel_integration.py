from pathlib import Path
from asmk import io_helpers
import numpy as np

print('Starting integration')

INPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel')

first_done = False

ranks_results = None
all_total_masks = [None] * 70
all_transformations = np.zeros([70, 100, 3, 3])

for file in (INPUT_FOLDER / 'mid_results').glob('*.pkl'):
    ifrom, ito = [int(item) for item in file.stem.split('-')]
    data = io_helpers.load_pickle(file)

    if first_done is False:
        ranks_results = data['ranks_results']
        first_done = True
    else:
        ranks_results[ifrom:ito] = data['ranks_results'][ifrom:ito]

    all_transformations[ifrom:ito] = data['total_transformations']

    for i, total_mask in zip(range(ifrom, ito), data['total_masks']):
        all_total_masks[i] = total_mask

io_helpers.save_pickle(INPUT_FOLDER / 'ranks_results.pkl', ranks_results)
io_helpers.save_pickle(INPUT_FOLDER / 'total_masks.pkl', all_total_masks)
io_helpers.save_pickle(INPUT_FOLDER / 'total_transformations.pkl', all_transformations)

print('Integration done')
