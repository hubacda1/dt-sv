import yaml

from .dotdict import dotdict


def read_yaml(file_name: str) -> dotdict:
    """
    Reads a YAML file and returns a dictionary.
    """
    with open(file_name, "r") as f:
        return dotdict.from_dict(yaml.safe_load(f))
