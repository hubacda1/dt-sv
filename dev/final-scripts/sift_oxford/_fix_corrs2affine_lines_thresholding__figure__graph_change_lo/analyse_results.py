import matplotlib.pyplot as plt
import numpy as np
import json

with open('./output.txt', 'r') as f:
    data = [json.loads(d[12:]) for d in f.readlines() if d.startswith('JSON STATS:')]

diffs = [[], []]
best_supps = [[], []]
for d in data:
    cp = d['correct_pair']
    if cp is None: 
        continue

    diffs[cp].append(d['all_best_support'] - d['best_support'])
    best_supps[cp].append(d['best_support'])


def test():
    a = np.array(best_supps[1])
    b = np.array(diffs[1])

    print()
    print(b[a >= 20].mean())
    print(b[a < 20].mean())
    print()
    print(b[a >= 20].shape)
    print(b[a < 20].shape)
    print()
    print()
    assert 0


# test()  


bins = [-10.5, -5.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 6.5, 9.5, 12.5]
plt.figure(figsize=(8, 4))
plt.hist(np.array(diffs[0]).clip(min=-10, max=10), bins=bins, color='#d62728', density=True, alpha=0.5, label='Irrelevant pairs')
plt.hist(np.array(diffs[1]).clip(min=-10, max=10), bins=bins, color='#2ca02c', density=True, alpha=0.5, label='Relevant pairs')
plt.xticks([-11, -8, -5, -3, -2, -1, -2, 0, 1, 2, 3, 5, 8, 11], labels=['[-inf, -10]', '[-9, -7]', '[-6, -4]', -3, -2, -1, -2, 0, 1, 2, 3, '[4, 6]', '[7, 9]', '[10, inf]'])
plt.xlabel(r'Change in support, $supp_{all\_hypotheses} - supp_{naive\_only}$')
plt.ylabel('Density')
plt.legend()
plt.tight_layout()
plt.savefig("inliers-change-with-lines-lo.png", bbox_inches='tight')
# plt.show()
