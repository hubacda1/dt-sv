DIRECTORY='fix_corrs2affine_lines_attempt4_v2_test'

pwd=$(pwd)
pwd=${pwd##*/}

if [[ $pwd = $DIRECTORY ]]; then
    echo "Run outside of $DIRECTORY dir!"
    exit 1
fi

MAX=70
CORES=48

STEP=$(($MAX/$CORES))

for i in $(seq 0 $STEP $(($MAX-1))); do
    max=$(( ($i+$STEP) > $MAX ? $MAX : ($i+$STEP) ))

    from=$i
    to=$max

    echo $from $to
    python $DIRECTORY/compute_map_parallel.py $from $to &
done

echo "Tasks assigned, waiting"

wait

echo "All done, run integration"
python $DIRECTORY/compute_map_parallel_integration.py
echo "Integration done"

ls -lah "$DIRECTORY/compute_map_parallel/mid_results/" | wc -l

echo "Evaluating"

echo "=== ranks_results ==="
python analyze_map.py "$DIRECTORY/compute_map_parallel/ranks_results.pkl"
