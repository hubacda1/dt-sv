import struct
from typing import Tuple

import numpy as np
from utilities import sizeof_uint64_t

masks = np.array([0] + [
        (1 << (i + 1)) - 1 for i in range(64)
], dtype=np.uint64)


class InvertedChunk:
    def __init__(self,
                 data: bytes,
                 offset: int,
                 n_words: int,
                 code_bits: Tuple[int, int, int],
                 max_bit: int):
        self.data = data
        self.offset = int(offset * 8)
        self.n_words = n_words

        self.bits = [
                *code_bits[:3],
                max_bit
        ]

        self.shifts = np.zeros(4, dtype=np.uint64)
        for i in range(1, 4):
            self.shifts[i] = (1 << self.bits[i - 1]) + self.shifts[i - 1]

        self.current = 0
        self.result = 0
        self.remaining = 64

    def decode(self):
        # Unpack a single uint64_t
        self.current = np.uint64(struct.unpack("<Q", self.data[self.offset:self.offset + sizeof_uint64_t])[0])
        self.offset += sizeof_uint64_t

        result = np.zeros(self.n_words, dtype=int)
        current = np.uint64(0)

        for i in range(self.n_words):
            decoded = self._decode_next()
            current = decoded + current
            result[i] = current

        return result

    def _decode_next(self):
        box = self._read_bits(2)
        result = self._read_bits(self.bits[box]) + self.shifts[box]

        return result

    def _read_bits(self, n_bits):
        if self.remaining >= n_bits:
            self.remaining -= n_bits
            return (self.current >> np.uint64(self.remaining)) & masks[n_bits]

        result = (self.current << np.uint64(n_bits - self.remaining)) & masks[n_bits]

        # Unpack a single uint64_t
        self.current = np.uint64(struct.unpack("<Q", self.data[self.offset:self.offset + sizeof_uint64_t])[0])
        self.offset += sizeof_uint64_t

        self.remaining += 64 - n_bits
        result += self.current >> np.uint64(self.remaining)

        return result
