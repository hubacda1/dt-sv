from pathlib import Path
import sys

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent.parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])

gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

# Load the results
ranks = io_helpers.load_pickle('_computed_ranks.pkl')

print('Original ranks:')
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])

for i in range(ranks.shape[0]):
    tp = []
    tn = []

    for j in range(100):
        if ranks[i, j] in gnd['gnd'][i]['easy'] or ranks[i, j] in gnd['gnd'][i]['hard']:
            tp.append(ranks[i, j])
        else:
            tn.append(ranks[i, j])

    ranks[i, :100] = [*tp, *tn]

print('Optimal ranks:')
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
print()
