from collections import namedtuple

import h5py
import numpy as np

from loading import Data
from utilities import BBox, cid2filename
from .inverted_chunk import InvertedChunk


class LambdaDict:
    def __init__(self, access_method):
        self.access_method = access_method

    def __getitem__(self, key):
        return self.access_method(key)


# Suggestion: we can have this class, then use engine.images[imid].stuff
class Image:
    cid: str
    imid: int
    file_path: str

    width: int
    height: int
    area: int

    visual_words: np.ndarray
    geometries: np.ndarray
    tfidf: np.ndarray

    # also unique


Geometry = namedtuple('Geometry', ['labels', 'unique_labels', 'positions', 'unique_positions', 'tfidf'])


class Engine:
    """Corresponds to the engine3 in cpp files"""

    def __init__(self, data: Data):
        self.version = data.engine_config.version
        self.n_vw = data.engine_config.n_vw
        self.n_documents = data.engine_config.n_documents
        self.invfile_bytes = data.invfile_bytes
        self.invfile_head = data.invfile_head
        self.code_bits = data.engine_config.code_bits
        self.idf = data.idf
        self.cid2id = data.cid2id
        self.id2cid = data.id2cid
        self.n_vw_per_document = np.zeros(self.n_documents, dtype=np.uint32)
        self.lengths = data.lengths
        self.geometries_path = data.geometries_path

        self.image_sizes = data.image_sizes
        self.image_area = LambdaDict(lambda imid: self.image_sizes[imid].prod())

        self.options = data.engine_config.options

        # Compatibility with MPV API
        self.visual_words = LambdaDict(lambda vw: self.get_documents_with_vw(vw))
        self.geometries = LambdaDict(lambda imid: self.get_geometries(imid)[1])

        self.geom_file = h5py.File(self.geometries_path, "r")

    def image_paths(self, size=100, random=True):
        size = min(size, self.n_documents)

        if not random:
            indices = np.arange(size)
        else:
            indices = np.random.choice(self.n_documents, size, replace=False)

        cids = [self.id2cid[i] for i in indices]
        paths = [cid2filename(cid) for cid in cids]

        return paths

    def get_documents_with_vw(self, vw: int):
        """
        Returns the documents that contain the given visual word.
        :param vw: The visual word to look for.
        :return: A list of document ids.
        """
        offset, wf, max_bit = self.invfile_head[vw]

        return InvertedChunk(self.invfile_bytes, offset, wf, self.code_bits, max_bit).decode()

    def get_geometries(self, imid: int, bbox: BBox = None):
        """
        Returns the geometries of the given document.
        :param bbox: If provided, only the geometries are inside this bounding box are returned.
        :param imid: The image id in range: [0, n_documents - 1].
        :return: [labels, geometries, tfidf] where labels are unique visual words in the image,
                 geometries is an array of corresponding geometries, and tfidf is tf-idf weights of the visual words.
        """
        labels = self.geom_file[f"geom/{imid}/labels"][()].squeeze()
        positions = self.geom_file[f"geom/{imid}/pos"][()]

        # inverting of the transformation matrix of the ellipses from the initial de-normalizing to the normalizing one
        positions[:, 2] = 1. / positions[:, 2]
        positions[:, 4] = 1. / positions[:, 4]
        positions[:, 3] = - positions[:, 3] * positions[:, 2] * positions[:, 4]

        if bbox is not None:
            x = positions[:, 0]
            y = positions[:, 1]

            mask = (x > bbox.x1) & (y > bbox.y1) & (x < bbox.x2) & (y < bbox.y2)

            labels = labels[mask]
            positions = positions[mask]

        tfidf, unique_labels, unique_indices, unique_inverse, unique_counts = self.get_idf(labels)

        return Geometry(labels, unique_labels, positions, positions[unique_indices], tfidf)

    def get_idf(self, labels: np.ndarray):
        unique_labels, unique_indices, unique_inverse, unique_counts = np.unique(
            labels, return_index=True, return_inverse=True, return_counts=True
        )

        tfidf = self.idf[unique_labels].squeeze() * unique_counts
        tfidf = tfidf / np.linalg.norm(tfidf)

        return tfidf, unique_labels, unique_indices, unique_inverse, unique_counts

    def get_vw_in_image(self, imid: int) -> np.ndarray:
        """
        Returns the visual words in the given image.
        :param imid: The image id in range: [0, n_documents - 1].
        :return: A list of visual words.
        """
        with h5py.File(self.geometries_path, "r") as f:
            return f[f"geom/{imid}/labels"][()].squeeze()

    def query(self, visual_words, top_k=100):
        tfidf, unique_vw, *_ = self.get_idf(visual_words)
        return self.custom_query(tfidf, unique_vw, top_k)

    def custom_query(self, tfidf, unique_vw, top_k=100):
        scores = np.zeros(self.n_documents)
        for vw, tfidf_vw in zip(unique_vw, tfidf):
            offset, wf, max_bit = self.invfile_head[vw]

            weight = self.idf[vw] * tfidf_vw

            chunk = InvertedChunk(self.invfile_bytes, offset, wf, self.code_bits, max_bit)
            documents = chunk.decode()

            documents_unique, documents_counts = np.unique(documents, return_counts=True)
            scores[documents_unique] += documents_counts * weight * self.lengths[documents_unique]

        indices = np.argsort(scores)[::-1][:top_k]

        scores = scores[indices]

        return indices, scores
