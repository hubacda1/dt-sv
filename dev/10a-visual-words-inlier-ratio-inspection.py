from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from functools import partial
from itertools import cycle
from time import perf_counter
from tabulate import tabulate
from dataclasses import dataclass
from typing import Any


from cirtorch.datasets.testdataset import configdataset
from cirtorch.utils.evaluate import compute_map_and_print
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf


def get_packed_residuals(vecs, centroids, binary=True):
    residuals = vecs - centroids
    
    if binary:
        return hamming.binarize_and_pack_2D(residuals)
    else:
        return functional.normalize_vec_l2(residuals)
    
def generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2):
    # 1 -> 2
    m = np.tile(np.eye(3), [coordx1.size, 1, 1])
    s = scales2 / scales1
    
    m[:, 0, 2] = -1 * coordx1 * s + coordx2
    m[:, 1, 2] = -1 * coordy1 * s + coordy2
    m[:, 0, 0] = m[:, 1, 1] = s
    
    return m


def compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2):
    pts1 = np.vstack([coordx1, coordy1, np.ones_like(coordx1)])
    pts2 = np.vstack([coordx2, coordy2, np.ones_like(coordx2)])
    
    # [number of hypotheses, number of correspondences]
    return np.linalg.norm((hypotheses @ pts1) - pts2, axis=1)


def verify_single_model(errors, corrs, inlier_threshold=32):
    n = corrs.shape[0]
    mask = np.zeros([n], dtype=bool)
    taken = set()

    i = 0
    actual_y, best_index = None, None
    best_error = None
    while i < n:
        actual_y = corrs[i, 1]
        
        best_error = np.inf
        best_index = -1
        while i < n and actual_y == corrs[i, 1]:
            if errors[i] < best_error and corrs[i, 0] not in taken:
                best_error = errors[i]
                best_index = i
            i += 1
        
        if best_error <= inlier_threshold:
            mask[best_index] = True
            taken.add(corrs[best_index, 0])
    
    return mask


def verify_models(errors, corrs, inlier_threshold=32):
    return np.apply_along_axis(
        partial(verify_single_model, corrs=corrs, inlier_threshold=inlier_threshold),
        axis=1,
        arr=errors,
    )


def affine_local_optimization(A, coordx1, coordy1, coordx2, coordy2):
    size = coordx1.size
    
    if size < 3:
        return A
    
    weight = 2 * np.pi if size < 11 else 0
    r2h = 50.0  # half of squared circle radius is integrated over
    
    mx1, my1 = coordx1.mean(), coordy1.mean()
    mx2, my2 = coordx2.mean(), coordy2.mean()
    
    dx1, dy1 = (coordx1 - mx1), (coordy1 - my1)
    dx2, dy2 = (coordx2 - mx2), (coordy2 - my2)
    
    # Compute AtA, AtB1, AtB2
    AtA = [0.0, 0.0, 0.0]
    AtB1 = [0.0, 0.0]
    AtB2 = [0.0, 0.0]
    
    AtA[0] = ((1 + weight) * dx1 * dx1 + weight * r2h).sum()
    AtA[1] = ((1 + weight) * dx1 * dy1).sum()
    AtA[2] = ((1 + weight) * dy1 * dy1 + weight * r2h).sum()
    
    AtB1[0] = ((1 + weight) * dx1 * dx2 + weight * A[0, 0] * r2h).sum()
    AtB1[1] = ((1 + weight) * dy1 * dx2 + weight * A[0, 1] * r2h).sum()
    
    AtB2[0] = ((1 + weight) * dx1 * dy2 + weight * A[1, 0] * r2h).sum()
    AtB2[1] = ((1 + weight) * dy1 * dy2 + weight * A[1, 1] * r2h).sum()
    
    # Final affine transformation
    detAtA = AtA[0] * AtA[2] - AtA[1] * AtA[1]
    if detAtA == 0:
        raise ValueError('Determinant equals to zero.')
        
    norm = 1 / detAtA
    H0 = (AtA[2] * AtB1[0] - AtA[1] * AtB1[1]) * norm
    H1 = (-AtA[1] * AtB1[0] + AtA[0] * AtB1[1]) * norm
    H2 = mx2 - mx1 * H0 - my1 * H1
    H3 = (AtA[2] * AtB2[0] - AtA[1] * AtB2[1]) * norm
    H4 = (-AtA[1] * AtB2[0] + AtA[0] * AtB2[1]) * norm
    H5 = my2 - mx1 * H3 - my1 * H4
    
    H = np.array([
        [H0, H1, H2],
        [H3, H4, H5],
        [0, 0, 1],
    ])
    
    return H


def get_tentative_correspondencies(
    q_ids, db_ids, q_residual_packs, db_residual_packs, similarity_func, max_tc=1500, max_MxN=15,
):
    db_unique, db_counts = np.unique(db_ids, return_counts=True)
    q_unique, q_counts = np.unique(q_ids, return_counts=True)
    
    db_sorted = np.argsort(db_ids, kind='stable')
    q_sorted = np.argsort(q_ids, kind='stable')
    
    ret = []
    similarities = []
    # counts = []
    
    qr_i = 0  # query index
    db_i = 0  # database index
    s_qr_i = 0  # sorted query index
    s_db_i = 0  # sorted database index

    qr_len = q_unique.shape[0]
    db_len = db_unique.shape[0]
    s_qry_len = q_sorted.shape[0]
    s_rel_len = db_sorted.shape[0]
    
    while qr_i < qr_len and db_i < db_len:  # While there are any visual words remaining
        if q_unique[qr_i] == db_unique[db_i]:  # If we encountered the the same visual word
            # count = q_counts[qr_i] * db_counts[db_i]
            # if count <= max_MxN:
            if True:
            
                # Move the sorted indexes forward so we find the beginning of current visual word
                while s_qr_i < s_qry_len and q_ids[q_sorted[s_qr_i]] != q_unique[qr_i]:
                    s_qr_i += 1
                while s_db_i < s_rel_len and db_ids[db_sorted[s_db_i]] != db_unique[db_i]:
                    s_db_i += 1
                
                # Cross products of corresponding visual words
                count = q_counts[qr_i] * db_counts[db_i]
                cross_product_correspondences = np.zeros([count, 2])
                cross_product_similarities = np.zeros([count])
                cross_product_index = 0
                
                s_qr_i_start = s_qr_i
                while s_db_i < s_rel_len and db_ids[db_sorted[s_db_i]] == db_unique[db_i]:
                    s_qr_i = s_qr_i_start
                    while s_qr_i < s_qry_len and q_ids[q_sorted[s_qr_i]] == q_unique[qr_i]:
                        #ret.append([q_sorted[s_qr_i], db_sorted[s_db_i]])
                        #similarities.append(similarity_func(
                        #    q_residual_packs[q_sorted[s_qr_i]], db_residual_packs[db_sorted[s_db_i]]
                        #)[0])
                        #vw_ids.append(q_unique[qr_i])
                        
                        cross_product_correspondences[cross_product_index] = [q_sorted[s_qr_i], db_sorted[s_db_i]]
                        cross_product_similarities[cross_product_index] = similarity_func(
                            q_residual_packs[q_sorted[s_qr_i]], db_residual_packs[db_sorted[s_db_i]]
                        )[0]
                        cross_product_index += 1
                        
                        s_qr_i += 1
                    s_db_i += 1
                    
                if count <= max_MxN:
                    mask = np.ones_like(cross_product_similarities, dtype=bool)
                else:
                    threshold = -np.sort(-cross_product_similarities)[max_MxN]
                    mask = cross_product_similarities >= threshold
                    
                ret.extend(cross_product_correspondences[mask][:max_MxN])
                similarities.extend(cross_product_similarities[mask][:max_MxN])
                    
            qr_i += 1
            db_i += 1
        elif q_unique[qr_i] < db_unique[db_i]:
            qr_i += 1
        else:
            db_i += 1
            
    ret_np = np.array(ret, dtype=int, ndmin=2)
        
    # If there are way too many correspondences, crop the result
    similarities_np = np.array(similarities)
    if ret_np.shape[0] > max_tc:
        # counts_np = np.array(counts)
        # keys = np.argsort(counts, kind='stable')[:max_tc]
        # return ret_np[keys]
        
        # If we do not care about the order
        #
        # keys = np.argsort(-similarities_np, kind='stable')[:max_tc]
        # return ret_np[keys], similarities_np[keys]
        
        # If we want to preserve the order (same visual words in a row)
        
        threshold = -np.sort(-similarities_np)[max_tc]
        mask = similarities_np >= threshold
        
        return ret_np[mask][:max_tc], similarities_np[mask][:max_tc]

    return ret_np, similarities_np


PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)


scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])


def kernel_similarity(v1, v2):
    _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
    return sim


t0 = perf_counter()

# Get centroid ids, then centroids and then residual packs
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
# dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = np.load('desc_vecs-quantized-dbvecs_centroid_ids.npy')

# ---
q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

t1 = perf_counter()

print('Centroids loaded in:', t1 - t0)

IMAGES_ROOT = Path('.').resolve().parent / 'oxbuild_images'
MAX_SPATIAL = 100


@dataclass
class DataItem:
    index: int
    vecs: Any
    centroid_ids: Any
    coordx: Any
    coordy: Any
    scales: Any
    residuals: Any
    
    def get(self):
        return (
            self.index,
            self.vecs,
            self.centroid_ids,
            self.coordx,
            self.coordy,
            self.scales,
            self.residuals,
        )
    

class DataIterator:
    def __init__(self, desc, vecs_centroid_ids, residual_packs, prefix='', include_mask=None):
        if include_mask is not None:
            self._imids = desc[f'{prefix}imids'][include_mask]
            self._vecs = desc[f'{prefix}vecs'][include_mask]
            self._coordx = desc[f'{prefix}coordx'][include_mask]
            self._coordy = desc[f'{prefix}coordy'][include_mask]
            self._scales = desc[f'{prefix}scales'][include_mask]
            self._vecs_centroid_ids = vecs_centroid_ids[include_mask]
            self._residual_packs = residual_packs[include_mask]
        else:
            self._imids = desc[f'{prefix}imids']
            self._vecs = desc[f'{prefix}vecs']
            self._coordx = desc[f'{prefix}coordx']
            self._coordy = desc[f'{prefix}coordy']
            self._scales = desc[f'{prefix}scales']
            self._vecs_centroid_ids = vecs_centroid_ids
            self._residual_packs = residual_packs
            
    def get_all_vecs(self):
        return self._vecs
    
    def get_all_imids(self):
        return self._imids
        
        
    def get_iter(self, idxs):
        if isinstance(idxs, int):
            idxs = range(idxs)
            
        for idx in idxs:
            yield self.get_item(idx)
            
    def get_item(self, idx):
        li = np.searchsorted(self._imids, idx, 'left')
        ri = np.searchsorted(self._imids, idx, 'right')
            
        vecs = self._vecs[li:ri]
        centroid_ids = self._vecs_centroid_ids[li:ri]
        coordx = self._coordx[li:ri]
        coordy = self._coordy[li:ri]
        scales = self._scales[li:ri]
        residuals = self._residual_packs[li:ri]
            
        return DataItem(idx, vecs, centroid_ids, coordx, coordy, scales, residuals)


print('>>> ASMK Original')
metadata, images, ranks, _scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])

filepath = 'generated-data/8c-asmk-min-inl.npz'

data = np.load(filepath)
vw_tc = data['vw_tc']
vw_inl = data['vw_inl']
inl_tc_ratio = vw_inl / vw_tc.clip(1)

threshold = 0.1
print('Using threshold:', threshold)

vw_to_remove = np.flatnonzero(inl_tc_ratio < threshold)
include_mask = ~np.isin(qvecs_centroid_ids, vw_to_remove)

all_query_iterator = DataIterator(desc, qvecs_centroid_ids, q_residual_packs, 'q')
limited_query_iterator = DataIterator(desc, qvecs_centroid_ids, q_residual_packs, 'q', include_mask=include_mask)
db_iterator = DataIterator(desc, dbvecs_centroid_ids, db_residual_packs)

def sv(query_iterator, db_iterator, queries, ranks):
    
    supports = np.zeros([len(queries), MAX_SPATIAL])
    inliers = np.zeros([len(queries), MAX_SPATIAL])
    
    for query_enum_index, qdata in enumerate(query_iterator.get_iter(queries)):  # query index
        qi, q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = qdata.get()

        for db_enum_index, dbdata in enumerate(db_iterator.get_iter(ranks[qi, :MAX_SPATIAL])):
            dbi, db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = dbdata.get()

            # Generate correspondences
            corrs, similarities = get_tentative_correspondencies(
                q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, kernel_similarity
            )
            _similarities = similarities * (similarities > 0)

            if not corrs.size:
                supports[query_enum_index, db_enum_index] = 0
                inliers[query_enum_index, db_enum_index] = 0
                continue

            # Pick corresponding data
            scales1 = scales[q_scales[corrs[:, 0]]]
            coordx1 = q_coordx[corrs[:, 0]] * 16 / scales1
            coordy1 = q_coordy[corrs[:, 0]] * 16 / scales1
            centroid_ids1 = q_centroid_ids[corrs[:, 0]]

            scales2 = scales[db_scales[corrs[:, 1]]]
            coordx2 = db_coordx[corrs[:, 1]] * 16 / scales2
            coordy2 = db_coordy[corrs[:, 1]] * 16 / scales2
            centroid_ids2 = db_centroid_ids[corrs[:, 1]]

            # Generate hypotheses
            hypotheses = generate_hypotheses(
                coordx1,
                coordy1,
                scales1,
                coordx2,
                coordy2,
                scales2,
            )

            # Compute errors and verify models
            errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
            verifications = verify_models(errors, corrs)

            # Local optimization of the best model
            sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort(kind='stable')

            total_support = -1
            total_mask = None
            total_A = None

            for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):
                A = hypotheses[best_hypothesis_index]
                mask = verifications[best_hypothesis_index]

                support = (mask * _similarities).sum()

                while True:
                    new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
                    errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                    new_mask = verify_models(errors, corrs)[0]
                    new_support = (new_mask * _similarities).sum()

                    if new_support > support:
                        support = new_support
                        A = new_A
                        mask = new_mask
                    else:
                        break

                if support > total_support:
                    total_support = support
                    total_A = A
                    total_mask = mask

            # Save data to investigate
            supports[query_enum_index, db_enum_index] = total_support
            inliers[query_enum_index, db_enum_index] = total_mask.sum()
        
    return supports, inliers


def pure_sv(qdata, dbdata):
    qi, q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = qdata.get()
    dbi, db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = dbdata.get()

    # Generate correspondences
    corrs, similarities = get_tentative_correspondencies(
        q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, kernel_similarity
    )
    _similarities = similarities * (similarities > 0)

    assert corrs.size

    # Pick corresponding data
    scales1 = scales[q_scales[corrs[:, 0]]]
    coordx1 = q_coordx[corrs[:, 0]] * 16 / scales1
    coordy1 = q_coordy[corrs[:, 0]] * 16 / scales1
    centroid_ids1 = q_centroid_ids[corrs[:, 0]]

    scales2 = scales[db_scales[corrs[:, 1]]]
    coordx2 = db_coordx[corrs[:, 1]] * 16 / scales2
    coordy2 = db_coordy[corrs[:, 1]] * 16 / scales2
    centroid_ids2 = db_centroid_ids[corrs[:, 1]]

    # Generate hypotheses
    hypotheses = generate_hypotheses(
        coordx1,
        coordy1,
        scales1,
        coordx2,
        coordy2,
        scales2,
    )

    # Compute errors and verify models
    errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
    verifications = verify_models(errors, corrs)

    # Local optimization of the best model
    sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()

    total_support = -1
    total_mask = None
    total_A = None

    for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):
        A = hypotheses[best_hypothesis_index]
        mask = verifications[best_hypothesis_index]

        support = (mask * _similarities).sum()

        while True:
            new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
            errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
            new_mask = verify_models(errors, corrs)[0]
            new_support = (new_mask * _similarities).sum()

            if new_support > support:
                support = new_support
                A = new_A
                mask = new_mask
            else:
                break

        if support > total_support:
            total_support = support
            total_A = A
            total_mask = mask
    
    return total_support, total_A, total_mask


queries = list(range(0, 60, 8))

print('Start the computation of feature limitation differences.')
t0 = perf_counter()
all_query_results_supports, all_query_results_inliers = sv(
    all_query_iterator, db_iterator, queries, ranks
)
limited_query_results_supports, limited_query_results_inliers = sv(
    limited_query_iterator, db_iterator, queries, ranks
)
t1 = perf_counter()
print('Supports and inliers for un/limited query features computed. Time: ', t1 - t0)


def signatures(xx, yy, scales, centers):
    return [f'x{x}y{y}s{s}c{c}' for x, y, s, c in zip(xx, yy, scales, centers)]


def join_signatures(s1, s2):
    return [f'{s}-{t}' for s, t in zip(s1, s2)]


def isin(source, container):
    c = set(container)
    return [(s in c) for s in source]


def plot_pair(qimg, dbimg, qbbx, qdata1, qdata2, dbdata):
    score1, _, mask1 = pure_sv(qdata1, dbdata)
    score2, _, mask2 = pure_sv(qdata2, dbdata)
    
    qid, _, q_centroid_ids1, qx1, qy1, qs1, q_residuals1 = qdata1.get()
    qid, _, q_centroid_ids2, qx2, qy2, qs2, q_residuals2 = qdata2.get()
    dbid, _, db_centroid_ids, dbx, dby, dbs, db_residuals = dbdata.get()
    
    corrs1, _ = get_tentative_correspondencies(
        q_centroid_ids1, db_centroid_ids, q_residuals1, db_residuals, kernel_similarity
    )
    corrs2, _ = get_tentative_correspondencies(
        q_centroid_ids2, db_centroid_ids, q_residuals2, db_residuals, kernel_similarity
    )
    
    _scales = scales[qs1[corrs1[mask1, 0]]]
    qx1 = qx1[corrs1[mask1, 0]] * 16 / _scales
    qy1 = qy1[corrs1[mask1, 0]] * 16 / _scales
    qc1 = q_centroid_ids1[corrs1[mask1, 0]]
    qsign1 = signatures(qx1, qy1, _scales, qc1)
    
    _scales = scales[qs2[corrs2[mask2, 0]]]
    qx2 = qx2[corrs2[mask2, 0]] * 16 / _scales
    qy2 = qy2[corrs2[mask2, 0]] * 16 / _scales
    qc2 = q_centroid_ids2[corrs2[mask2, 0]]
    qsign2 = signatures(qx2, qy2, _scales, qc2)

    _scales = scales[dbs[corrs1[mask1, 1]]]
    dbx1 = dbx[corrs1[mask1, 1]] * 16 / _scales
    dby1 = dby[corrs1[mask1, 1]] * 16 / _scales
    dbc1 = db_centroid_ids[corrs1[mask1, 1]]
    dbsign1 = signatures(dbx1, dby1, _scales, dbc1)

    _scales = scales[dbs[corrs2[mask2, 1]]]
    dbx2 = dbx[corrs2[mask2, 1]] * 16 / _scales
    dby2 = dby[corrs2[mask2, 1]] * 16 / _scales
    dbc2 = db_centroid_ids[corrs2[mask2, 1]]
    dbsign2 = signatures(dbx2, dby2, _scales, dbc2)
    
    #
    # Compute the common matches
    #
    sign1 = join_signatures(qsign1, dbsign1)
    sign2 = join_signatures(qsign2, dbsign2)
    
    common_mask_1 = np.array(isin(sign1, sign2))
    common_mask_2 = np.array(isin(sign2, sign1))
    
    #
    # Display
    #
    fig, ax = plt.subplots(2, 1, figsize=(12, 9))
    
    qh, qw, _ = qimg.shape
    dbh, dbw, _ = dbimg.shape
    
    img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
    img[:qh, :qw] = qimg
    img[:dbh, qw:] = dbimg
    
    bbx, bby, *_ = qbbx if qbbx is not None else [0, 0]
    
    ax[0].imshow(img)
    ax[0].set_title(f'[1] qid={qid} dbid={dbid} score={score1:.3f} support={mask1.sum()} shared={common_mask_1.sum()}')
    for x1, y1, x2, y2, common in zip(qx1, qy1, dbx1, dby1, common_mask_1):
        c = 'gold' if common else 'green'
        ax[0].plot(
            [int(x1 + bbx), int(x2 + qw)],
            [int(y1 + bby), int(y2)],
            color=c,
            marker='o',
            linestyle='dashed',
            linewidth=1,
            markersize=3,
        )
        
    
    ax[1].imshow(img)
    ax[1].set_title(f'[2] qid={qid} dbid={dbid} score={score2:.3f} support={mask2.sum()} shared={common_mask_2.sum()}')
    for x1, y1, x2, y2, common in zip(qx2, qy2, dbx2, dby2, common_mask_2):
        c = 'gold' if common else 'green'
        ax[1].plot(
            [int(x1 + bbx), int(x2 + qw)],
            [int(y1 + bby), int(y2)],
            color=c,
            marker='o',
            linestyle='dashed',
            linewidth=1,
            markersize=3,
        )
    
    plt.show()


print('Start the slideshow')
MIN_DIFF = 7
cont = True
for QUERIES_CURRENT_ID, query_id in enumerate(queries):
    diff = np.abs(all_query_results_inliers[QUERIES_CURRENT_ID] - limited_query_results_inliers[QUERIES_CURRENT_ID])

    print(f'qid={query_id}, #largeDiff={(diff >= MIN_DIFF).sum()}')

    for idx in np.nonzero(diff >= MIN_DIFF)[0]:
        query_img_name = gnd['qimlist'][query_id]
        result_img_name = gnd['imlist'][ranks[query_id, idx]]

        query_img = plt.imread(IMAGES_ROOT / f'{query_img_name}.jpg')
        result_img = plt.imread(IMAGES_ROOT / f'{result_img_name}.jpg')

        plot_pair(
            query_img,
            result_img,
            gnd['gnd'][queries[QUERIES_CURRENT_ID]]['bbx'],
            all_query_iterator.get_item(queries[QUERIES_CURRENT_ID]),
            limited_query_iterator.get_item(queries[QUERIES_CURRENT_ID]),
            db_iterator.get_item(ranks[queries[QUERIES_CURRENT_ID], idx]),
        )

        inp = input('Continue? [Y/n] ')
        if inp.upper() not in ('Y', ''):
            cont = False
            break
    
    if not cont:
        break
