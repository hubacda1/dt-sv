import h5py
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from functools import partial
from itertools import cycle
from time import perf_counter
from tabulate import tabulate

from cirtorch.datasets.testdataset import configdataset
from cirtorch.utils.evaluate import compute_map_and_print
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf

#
# Load the data
#
with h5py.File("../jenicto-new-data/delf_gld/oxford.5k_revisited.db_revop_none.descriptors_1_0.h5", "r") as f:
    print("Keys: %s" % f.keys())
    
    db_locations = f['locations'][()]
    db_scales = f['scales'][()]
    db_descriptors = f['descriptors'][()]
    db_attentions = f['attentions'][()]
    db_index = f['index'][()]

with h5py.File("../jenicto-new-data/delf_gld/oxford.5k_revisited.queries_revop_none.descriptors_1_0.h5", "r") as f:
    print("Keys: %s" % f.keys())
    
    q_locations = f['locations'][()]
    q_scales = f['scales'][()]
    q_descriptors = f['descriptors'][()]
    q_attentions = f['attentions'][()]
    q_index = f['index'][()]


#
# Create the ASMK object
#
PARAMETERS_PATH = 'delf_gld'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

asmk_dataset_old = build_ivf(asmk, DATASET, desc, globals, logger)
