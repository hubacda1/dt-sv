from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle(
    '/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl'
)
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

def show_image(supp, A, mask, color):
    if 1:
        if 1:
            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, supp={supp}')
            tr_coords_A = transform_points(A, coordx1[mask], coordy1[mask])
            # for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
            for qx, qy, dbx, dby in zip(coordx1[mask], coordy1[mask], *tr_coords_A):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color=color,
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )

            #bbx, bby, bbx2, bby2 = gnd['gnd'][qi]['bbx']
            #tr_coords_A = transform_points(total_A, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            #plt.plot([bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby], color='red')
            #plt.plot(tr_coords_A[0] + qw, tr_coords_A[1], color='red')

            plt.show()

###############
###############
###############

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100

# scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])

# for qi, fname in enumerate(gnd['qimlist']):  # query index
# qi = 34

# 2596 & 4288

THR = 10

# qi = 3129
# fname = gnd['imlist'][qi]

# fname = 'ashmolean_000269'
# qid = engine.cid2id[f'oxb-complete/0000/{fname}']
qid = 1241
fname = engine.id2cid[qid].rsplit('/', 1)[1]
geometry = engine.get_geometries(qid)#, bbox=BBox(*gnd['gnd'][qi]['bbx']))

# Load bounding box
# bbx, bby, bbx2, bby2 = gnd['gnd'][qi]['bbx']

if 1:

    q_centroid_ids = geometry.labels
    q_coordx = geometry.positions[:, 0]
    q_coordy = geometry.positions[:, 1]
    q_scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])
    # q_scales = (geometry.positions[:, 2] + geometry.positions[:, 4]) / 2

    # for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
    # gnd_imid = gnd['gnd'][qi]['easy'][0]
    # gnd_imid = 4170
    gnd_imid = 2468
    if 1:
        
        db_fname = gnd['imlist'][gnd_imid]
        # db_fname = 'oxford_000601'
        if f'oxb-complete/0000/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0000/{db_fname}']
        elif f'oxb-complete/0001/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0001/{db_fname}']
        else:
            raise Exception(f'Unknown image name: {db_fname}')
        db_geometry = engine.get_geometries(dbid)

        db_centroid_ids = db_geometry.labels
        db_coordx = db_geometry.positions[:, 0]
        db_coordy = db_geometry.positions[:, 1]
        db_scales = np.sqrt(db_geometry.positions[:, 2] * db_geometry.positions[:, 4])
        # db_scales = (db_geometry.positions[:, 2] + db_geometry.positions[:, 4]) / 2

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        assert corrs.size, "No correspondences"
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]  # * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]  # * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]  # * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]  # * 16 / scales2
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=THR)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()

        total_support = -1
        total_A = None
        total_mask = None

        for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):
            A = hypotheses[best_hypothesis_index]
            mask = verifications[best_hypothesis_index]

            support = (mask * _similarities).sum()

            while True:
                new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
                errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                new_mask = verify_models(errors, corrs, inlier_threshold=THR)[0]
                new_support = (new_mask * _similarities).sum()

                if new_support > support:
                    support = new_support
                    A = new_A
                    mask = new_mask
                else:
                    break

            if support > total_support:
                total_support = support
                total_A = A
                total_mask = mask

        DISPLAY = True
        if DISPLAY:
            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, supp={total_support}')
            tr_coords_A = transform_points(total_A, coordx1[total_mask], coordy1[total_mask])
            # for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], *tr_coords_A):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )

            #bbx, bby, bbx2, bby2 = gnd['gnd'][qi]['bbx']
            #tr_coords_A = transform_points(total_A, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            #plt.plot([bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby], color='red')
            #plt.plot(tr_coords_A[0] + qw, tr_coords_A[1], color='red')

            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

        print('Round 1:')
        print('tot mask', total_mask.sum())
        print('qscales', q_scales.shape, 'corrs', corrs.shape)
        print('------------')
        print()

        q_nd_mask = np.ones_like(q_centroid_ids, dtype=bool)
        q_nd_mask[corrs[total_mask, 0]] = False
        q_centroid_ids = q_centroid_ids[q_nd_mask]
        q_coordx = q_coordx[q_nd_mask]
        q_coordy = q_coordy[q_nd_mask]
        q_scales = q_scales[q_nd_mask]

        db_nd_mask = np.ones_like(db_centroid_ids, dtype=bool)
        db_nd_mask[corrs[total_mask, 1]] = False
        db_centroid_ids = db_centroid_ids[db_nd_mask]
        db_coordx = db_coordx[db_nd_mask]
        db_coordy = db_coordy[db_nd_mask]
        db_scales = db_scales[db_nd_mask]

        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        assert corrs.size, "No correspondences"
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]  # * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]  # * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]  # * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]  # * 16 / scales2
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=THR)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()

        total_support = -1
        total_A = None
        total_mask = None

        for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):
            A = hypotheses[best_hypothesis_index]
            mask = verifications[best_hypothesis_index]

            support = (mask * _similarities).sum()

            while True:
                new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
                errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                new_mask = verify_models(errors, corrs, inlier_threshold=THR)[0]
                new_support = (new_mask * _similarities).sum()

                if new_support > support:
                    support = new_support
                    A = new_A
                    mask = new_mask
                else:
                    break

            if support > total_support:
                total_support = support
                total_A = A
                total_mask = mask
        
        print('Round 2:')
        print('tot mask', total_mask.sum())
        print('qscales', q_scales.shape, 'corrs', corrs.shape)
        print('------------')
        print()

        if DISPLAY:
            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, supp={total_support}')
            tr_coords_A = transform_points(total_A, coordx1[total_mask], coordy1[total_mask])
            # for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], *tr_coords_A):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )

            #bbx, bby, bbx2, bby2 = gnd['gnd'][qi]['bbx']
            #tr_coords_A = transform_points(total_A, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            #plt.plot([bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby], color='red')
            #plt.plot(tr_coords_A[0] + qw, tr_coords_A[1], color='red')

            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()
