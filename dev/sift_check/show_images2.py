from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

lines_db = io_helpers.load_pickle('_lines_database.pkl')
gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')

###############
###############
###############

class Database:

    def __init__(self, engine, gnd, lines_db):
        self.engine = engine
        self.gnd = gnd
        self.lines_db = lines_db

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)


DATABASE = Database(engine, gnd, lines_db)
IMG_ROOT = Path('../../oxbuild_images')

for qi, gnd_imid, num_inl in [
    (0, 2096, '?'),
    (69, 3269, '?'),
]:

    fname = gnd['qimlist'][qi]
    qid = DATABASE.get_image_id(fname)

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)
    shortlist, shortlist_scores = DATABASE.get_shortlist(q_centroid_ids)

    db_fname = gnd['imlist'][gnd_imid]
    dbid = DATABASE.get_image_id(db_fname)
    db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

    # Generate correspondences
    corrs, similarities = get_tentative_correspondencies(
        q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
    )
    _similarities = similarities * (similarities > 0)
    assert corrs.size

    # Pick corresponding data
    scales1 = q_scales[corrs[:, 0]]
    coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
    coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1
    centroids = q_centroid_ids[corrs[:, 0]]

    scales2 = db_scales[corrs[:, 1]]
    coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
    coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

    # Load images
    qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
    dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

    _, centroids = np.unique(centroids, return_inverse=True)
    colors = list(mcolors.CSS4_COLORS.values())
    np.random.shuffle(colors)
    colors = [colors[cid % len(colors)] for cid in centroids]

    fig, ax = plt.subplots(1, 2)
    fig.suptitle(f'corrs={corrs.shape[0]}, qi={qi}, gnd_imid={gnd_imid}, num_inl={num_inl}')
    ax[0].imshow(qimg, cmap='gray')
    for x, y, c in zip(coordx1, coordy1, colors):
        ax[0].plot(x, y, marker='x', color=c)
    # ax[0].plot(coordx1, coordy1, marker='x', color=c)
    ax[1].imshow(dbimg, cmap='gray')
    for x, y, c in zip(coordx2, coordy2, colors):
        ax[1].plot(x, y, marker='x', color=c)
    # ax[1].plot(coordx2, coordy2, marker='x', color=c)

    plt.show()
