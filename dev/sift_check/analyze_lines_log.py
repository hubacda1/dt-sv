import numpy as np
import matplotlib.pyplot as plt

stats = {}

with open('_hypothesis_with_lines_compare_top_models.out.txt', 'r') as f:
    while line := f.readline():
        if '---' in line or line.startswith('Script took:'):
            continue

        data = line.split()
        for key, value in (d.split('=') for d in data):
            
            if key not in stats:
                stats[key] = []

            func = int if key != 'correct_pair' else lambda x: x == 'True'
            stats[key].append(func(value))

for k in stats:
    stats[k] = np.array(stats[k])


print(stats.keys())

# mask = ~stats['correct_pair'] & (stats['max_#_inl'] >= 5)
# mask = stats['correct_pair'] & (stats['max_#_inl'] <= 5)
# mask = ~stats['correct_pair'] & (stats['diff'] > 1) & (stats['max_#_inl'] <= 5)
# mask = stats['correct_pair'] & (stats['max_#_inl'] <= 5) & (stats['new_max_#_inl'] <= 5)
mask = ~stats['correct_pair']
for v in zip(
    stats['qi'][mask],
    stats['gnd_imid'][mask],
    # stats['#_hyp'][mask],
    stats['max_#_inl'][mask],
    # stats['diff'][mask],
):
    print(v, ',')
