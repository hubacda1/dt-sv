from dataclasses import dataclass
from typing import Tuple

from utilities import dotdict


@dataclass
class EngineConfig:
    version: str
    n_vw: int
    n_documents: int
    code_bits: Tuple[int, int, int]
    options: dotdict
