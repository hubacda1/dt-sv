from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent))
assert Path('.').resolve() != Path(__file__).parent.resolve(), 'Please, run the script from the first folder, not from the subfolder.'

from collections import defaultdict
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from investigate_the_features_parameters_v2.local_spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

from loading import load_data
from engine import Engine

################
## INPUT ARGS ##
################

# assert len(sys.argv) == 3, 'Incorrect args...'

iSTART = 0  # int(sys.argv[1])
iEND = 1  # int(sys.argv[2])

# OUTPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel/mid_results')
# OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

################
## SETUP COSS ##
################

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

"""
ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    # q_vw = engine.get_vw_in_image(qid)
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
"""
ranks = io_helpers.load_pickle('_computed_ranks.pkl')
lines_db = io_helpers.load_pickle('_lines_database.pkl')
print('=== benchmark ===')
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])
print()

##############
## DATABASE ##
##############

class Database:

    def __init__(self, engine, gnd, lines_db):
        self.engine = engine
        self.gnd = gnd
        self.lines_db = lines_db

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_original_geometries(self, id, bbox=None, query=False):
        if query:
            fname = self.gnd['qimlist'][id]
            id = engine.cid2id[f'oxb-complete/0000/{fname}']

        geometry = engine.get_geometries(id, bbox=bbox)
        return geometry
    
    def get_geometries(self, id, bbox=None, query=False):

        if query:
            fname = self.gnd['qimlist'][id]
            id = engine.cid2id[f'oxb-complete/0000/{fname}']

        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = 1 / np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        # Fake unused variables, just to satisfy the API
        vecs = None
        residuals = np.ones_like(scales)

        return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(arg1, arg2):
        return [1]



DATABASE = Database(engine, gnd, lines_db)

IMG_ROOT = Path('../../../oxbuild_images')

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

print('Start spatial verification.')

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100
INLIER_THRESHOLD = 62
SCALE_THRESHOLD = 11_000_000

ranks_results = ranks.copy()

INLIER_THRESHOLD = 20
for qi, fname in enumerate(gnd['qimlist'][iSTART:iEND], start=iSTART):  # query index

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)

    supports_results = np.zeros([MAX_SPATIAL])

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)

        if not corrs.size:
            supports_results[dbi] = 0
            print('[Not expected] No correspondences.')
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        """
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        """
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        """
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        if new_hypotheses.size:
            hypotheses = np.vstack([hypotheses, new_hypotheses])
        """

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        determinants = np.linalg.det(hypotheses)
        scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
        verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD, scale_threshold=SCALE_THRESHOLD)

        # Local optimization of the weighted best model
        weighted_scores = -(verifications * _similarities[np.newaxis]).sum(axis=1)
        sorted_hypothesis_indexes = weighted_scores.argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, scales1,
            coordx2, coordy2, scales2, inlier_threshold=INLIER_THRESHOLD, eigenvals_threshold=5, scale_threshold=SCALE_THRESHOLD
        )

        print('Total A')
        print(total_A)
        print()

        # Save the support
        supports_results[dbi] += total_support

        # Log
        correct_pair = (gnd_imid in gnd['gnd'][qi]['easy']) or (gnd_imid in gnd['gnd'][qi]['hard'])
        if gnd_imid in gnd['gnd'][qi]['junk']:
            correct_pair = None
        print('JSON STATS:', json.dumps({
            'qi': qi,
            'dbi': dbi,
            'gnd_imid': int(gnd_imid),
            'correct_pair': correct_pair,
            'corrs': corrs.shape[0],
            'supports_results': supports_results[dbi],
        }))

        DISPLAY = True
        if DISPLAY:


            def plot_bbox(bbox, ax, A=None, qw=None, Acol='red'):
                x1, y1, x2, y2 = bbox.as_tuple
                ax.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

                if A is not None and qw is not None:
                    tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                    ax.plot(tr_coords_A[0] + qw, tr_coords_A[1], color=Acol)


            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            # qh, qw, _ = qimg.shape
            # dbh, dbw, _ = dbimg.shape
            
            # img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            # img[:qh, :qw] = qimg
            # img[:dbh, qw:] = dbimg

            # plt.imshow(img)
            # # plt.title(f'q={fname}, db={db_fname}, score={aws_lo}')
            # plot_bbox(q_bbox, plt, hypotheses[keys[0]], qw, Acol='red')
            # for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
            #     break
            #     plt.plot(
            #         [qx, dbx + qw],
            #         [qy, dby],
            #         color='green',
            #         marker='o',
            #         linestyle='dashed',
            #         linewidth=1,
            #         markersize=3,
            #     )
            #     break

            from itertools import cycle

            colors = cycle(['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'])

            def plot_bbox(bbox, axs, A=None, Acol='red'):
                x1, y1, x2, y2 = bbox.as_tuple
                axs[0].plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

                if A is not None:
                    tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                    axs[1].plot(tr_coords_A[0], tr_coords_A[1], color=Acol)


            # x, y, a, b, c
            q_geometries = DATABASE.get_original_geometries(qi, bbox=q_bbox, query=True)
            db_geometries = DATABASE.get_original_geometries(dbid)

            fig, ax = plt.subplots(1, 2, figsize=(10, 5))
            ax[0].imshow(qimg)
            ax[1].imshow(dbimg)
            # plot_bbox(q_bbox, ax, total_A)

            # Get an inlier
            for inlier_corr_key in total_mask.nonzero()[0]:#[::2]:
                q_key, db_key = corrs[inlier_corr_key]

                qx, qy, qa, qb, qc = q_geometries.positions[q_key]
                dbx, dby, dba, dbb, dbc = db_geometries.positions[db_key]

                qA = np.array([
                    [qa, 0, qx],
                    [qb, qc, qy],
                    [0, 0, 1],
                ])
                print('=== qA ===')
                print(qA)
                print()
                dbA = np.array([
                    [dba, 0, dbx],
                    [dbb, dbc, dby],
                    [0, 0, 1],
                ])
                print('=== dbA ===')
                print(dbA)
                print()
                print('=== dbA @ qA^-1 ===')
                print(dbA @ np.linalg.inv(qA))
                print()

                errors = compute_errors((dbA @ np.linalg.inv(qA))[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                determinants = np.linalg.det(hypotheses)
                scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
                verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD, scale_threshold=SCALE_THRESHOLD)

                c = next(colors)
                plot_bbox(q_bbox, ax, dbA @ np.linalg.inv(qA), c)

                qA = qA[:-1]
                dbA = dbA[:-1]

                pt = np.array([0, 0, 1])
                ax[0].plot(*(qA @ pt), color=c, marker='x')
                ax[1].plot(*(dbA @ pt), color=c, marker='x')

                N = 100
                circle = np.array([
                    np.sin(np.linspace(0, 2 * np.pi, num=N)) * 300,
                    np.cos(np.linspace(0, 2 * np.pi, num=N)) * 300,
                    np.ones([N]),
                ])
                # plt.plot(circle[0] + 100, circle[1] + 100, color='gold')
                ax[0].plot(*(qA @ circle), color=c)
                ax[1].plot(*(dbA @ circle), color=c)

                ax[0].plot(*(circle[:2] + 330), color='gold')

                break
            

            # plt.plot(
            #     [qx, dbx + qw],
            #     [qy, dby],
            #     color='green',
            #     marker='o',
            #     linestyle='dashed',
            #     linewidth=1,
            #     markersize=3,
            # )

            plt.show()

            print()
            print()
            print()
            print()
            print()
            assert 0


            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

            assert 0



#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)
