from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

ranks = io_helpers.load_pickle('_computed_ranks.pkl')
lines_db = io_helpers.load_pickle('_lines_database.pkl')
gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')

###############
###############
###############

def plot_bbox(bbox, A=None, qw=None):
    x1, y1, x2, y2 = bbox.as_tuple
    plt.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

    if A is not None:
        assert qw is not None
        tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
        plt.plot(tr_coords_A[0] + qw, tr_coords_A[1], color='red')


class Database:

    def __init__(self, engine, gnd, lines_db):
        self.engine = engine
        self.gnd = gnd
        self.lines_db = lines_db

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)


DATABASE = Database(engine, gnd, lines_db)
IMG_ROOT = Path('../../oxbuild_images')

MIN_LINE_LENGTHS = 40
MAX_LINE_LENGTH = 110
MAX_SPATIAL = 100

cache = {}

for qi, fname in enumerate(gnd['qimlist']):  # query index
    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    qid = DATABASE.get_image_id(fname)

    lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
    cache[fname] = lines
    print('Query done', qi)

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        db_fname = gnd['imlist'][gnd_imid]
        if db_fname in cache:
            continue
        lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[db_fname] = lines

    print('Shortlist done', qi)

    for gnd_imid in (gnd['gnd'][qi]['easy'] + gnd['gnd'][qi]['hard']):
        db_fname = gnd['imlist'][gnd_imid]
        if db_fname in cache:
            continue
        lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[db_fname] = lines
    
    print('Gnd truth done', qi)

# io_helpers.save_pickle('_lines_database.pkl', cache)
