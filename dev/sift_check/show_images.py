from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

lines_db = io_helpers.load_pickle('_lines_database.pkl')
gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')

###############
###############
###############

class Database:

    def __init__(self, engine, gnd, lines_db):
        self.engine = engine
        self.gnd = gnd
        self.lines_db = lines_db

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)


DATABASE = Database(engine, gnd, lines_db)
IMG_ROOT = Path('../../oxbuild_images')

qi = 0
gnd_imid = 4700

for qi, gnd_imid, num_inl, num_line_inl in [
(0, 2354, 4, 4) ,
(0, 1687, 3, 3) ,
(0, 3349, 4, 1) ,
(0, 3784, 3, 3) ,
(0, 4700, 3, 2) ,
(0, 322, 2, 1) ,
(0, 4052, 1, 1) ,
(0, 2221, 2, 1) ,
(2, 660, 4, 1) ,
(2, 3458, 3, 2) ,
(2, 3620, 2, 1) ,
(2, 3998, 2, 1) ,
(2, 1619, 4, 1) ,
(2, 3268, 5, 2) ,
(2, 691, 4, 4) ,
(3, 69, 2, 2) ,
(3, 2956, 2, 2) ,
(3, 1232, 3, 4) ,
(4, 1643, 4, 1) ,
(4, 3458, 5, 2) ,
(4, 3998, 5, 3) ,
(4, 4960, 3, 4) ,
(4, 185, 3, 5) ,
(4, 92, 4, 1) ,
(4, 953, 2, 1) ,
(4, 4052, 4, 1) ,
(4, 2204, 3, 2) ,
(10, 245, 5, 1) ,
(10, 3971, 5, 1) ,
(10, 1059, 4, 1) ,
(12, 933, 4, 4) ,
(12, 2205, 3, 4) ,
(12, 3829, 4, 4) ,
(12, 4498, 4, 3) ,
(12, 3152, 2, 1) ,
(13, 933, 5, 5) ,
(13, 3971, 4, 4) ,
(13, 607, 3, 2) ,
(13, 605, 2, 3) ,
(14, 605, 2, 3) ,
(14, 3971, 4, 3) ,
(15, 4758, 3, 2) ,
(16, 39, 3, 4) ,
(17, 1364, 5, 5) ,
(17, 4758, 3, 3) ,
(17, 233, 3, 1) ,
(18, 4758, 4, 5) ,
(19, 3100, 5, 1) ,
(19, 905, 5, 2) ,
(19, 314, 5, 5) ,
(20, 4992, 5, 5) ,
(20, 3452, 4, 4) ,
(20, 4123, 3, 2) ,
(21, 2274, 4, 3) ,
(21, 3158, 4, 3) ,
(22, 1119, 3, 3) ,
(22, 3332, 3, 3) ,
(22, 999, 2, 3) ,
(22, 4704, 2, 1) ,
(22, 2058, 5, 5) ,
(22, 3237, 3, 2) ,
(22, 629, 4, 4) ,
(22, 3158, 2, 2) ,
(22, 2820, 3, 3) ,
(22, 4735, 2, 2) ,
(22, 3080, 1, 2) ,
(22, 2637, 3, 3) ,
(22, 1808, 3, 3) ,
(22, 3304, 1, 2) ,
(22, 2735, 2, 2) ,
(23, 437, 4, 4) ,
(23, 2744, 2, 2) ,
(25, 3840, 4, 1) ,
(25, 1625, 3, 3) ,
(26, 4965, 3, 3) ,
(26, 2468, 3, 3) ,
(27, 4965, 3, 3) ,
(27, 4182, 3, 3) ,
(27, 1625, 2, 2) ,
(28, 3751, 5, 4) ,
(28, 3840, 4, 2) ,
(29, 4965, 4, 4) ,
(29, 2155, 3, 2) ,
(29, 4182, 2, 3) ,
(30, 3653, 5, 4) ,
(30, 3166, 2, 2) ,
(30, 1009, 4, 5) ,
(30, 2722, 4, 2) ,
(30, 4739, 2, 4) ,
(30, 344, 4, 3) ,
(31, 3166, 3, 1) ,
(31, 344, 4, 1) ,
(31, 2722, 3, 5) ,
(31, 4739, 3, 3) ,
(31, 1009, 3, 3) ,
(32, 2722, 2, 1) ,
(32, 1571, 4, 3) ,
(33, 4061, 5, 1) ,
(33, 344, 2, 1) ,
(34, 3166, 3, 1) ,
(34, 1571, 3, 2) ,
(34, 2722, 3, 1) ,
(34, 344, 4, 1) ,
(40, 4693, 4, 2) ,
(40, 788, 3, 3) ,
(40, 1533, 4, 3) ,
(40, 1254, 3, 2) ,
(40, 1986, 5, 5) ,
(41, 4623, 5, 5) ,
(42, 2903, 3, 4) ,
(42, 214, 1, 2) ,
(42, 719, 3, 1) ,
(42, 788, 2, 1) ,
(42, 3427, 4, 3) ,
(43, 1533, 1, 2) ,
(44, 4354, 2, 1) ,
(44, 2903, 2, 2) ,
(44, 4623, 3, 1) ,
(44, 2236, 3, 1) ,
(44, 2030, 5, 5) ,
(53, 3325, 5, 2) ,
(55, 660, 5, 4) ,
(55, 1424, 4, 5) ,
(55, 4644, 3, 4) ,
(55, 3268, 3, 5) ,
(55, 2847, 3, 3) ,
(55, 1754, 2, 3) ,
(56, 4290, 5, 1) ,
(56, 3303, 5, 1) ,
(56, 3907, 4, 2) ,
(56, 4634, 5, 1) ,
(56, 1392, 5, 1) ,
(56, 801, 2, 2) ,
(56, 2354, 4, 1) ,
(56, 69, 2, 1) ,
(62, 4595, 3, 2) ,
(62, 132, 5, 2) ,
(62, 2744, 3, 2) ,
(62, 437, 2, 3) ,
(62, 3942, 3, 2) ,
(63, 2274, 5, 4) ,
(63, 4402, 4, 3) ,
(63, 999, 3, 3) ,
(63, 2368, 4, 4) ,
(63, 506, 4, 3) ,
(65, 3281, 3, 1) ,
(66, 1733, 4, 2) ,
(66, 972, 3, 1) ,
(66, 3779, 3, 1) ,
(66, 88, 2, 2) ,
(68, 2268, 5, 2) ,
(68, 4240, 4, 1) ,
(68, 306, 4, 1) ,
(68, 3269, 5, 1) ,
(68, 3962, 4, 1) ,
(68, 1964, 5, 1) ,
(68, 1606, 5, 1) ,
(68, 2932, 4, 1) ,
(68, 3225, 4, 1) ,
(68, 3674, 4, 2) ,
(68, 446, 4, 1) ,
(68, 2968, 5, 1) ,
(68, 2910, 4, 3) ,
(68, 4034, 4, 1) ,
(68, 3780, 4, 4) ,
(68, 4384, 4, 2) ,
(68, 256, 4, 2) ,
(69, 2435, 1, 1) ,
(69, 2641, 4, 1) ,
(69, 3796, 2, 1) ,
]:

    fname = gnd['qimlist'][qi]
    qid = DATABASE.get_image_id(fname)

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)
    shortlist, shortlist_scores = DATABASE.get_shortlist(q_centroid_ids)

    db_fname = gnd['imlist'][gnd_imid]
    dbid = DATABASE.get_image_id(db_fname)
    db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

    # Generate correspondences
    corrs, similarities = get_tentative_correspondencies(
        q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
    )
    _similarities = similarities * (similarities > 0)
    assert corrs.size

    # Pick corresponding data
    scales1 = q_scales[corrs[:, 0]]
    coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
    coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1
    centroids = q_centroid_ids[corrs[:, 0]]

    scales2 = db_scales[corrs[:, 1]]
    coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
    coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

    # Load images
    qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
    dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

    _, centroids = np.unique(centroids, return_inverse=True)
    colors = list(mcolors.CSS4_COLORS.values())
    np.random.shuffle(colors)
    colors = [colors[cid % len(colors)] for cid in centroids]

    fig, ax = plt.subplots(1, 2)
    fig.suptitle(f'corrs={corrs.shape[0]} num_inl={num_inl} num_inl_lines={num_line_inl}')
    ax[0].imshow(qimg, cmap='gray')
    for x, y, c in zip(coordx1, coordy1, colors):
        ax[0].plot(x, y, marker='x', color=c)
    # ax[0].plot(coordx1, coordy1, marker='x', color=c)
    ax[1].imshow(dbimg, cmap='gray')
    for x, y, c in zip(coordx2, coordy2, colors):
        ax[1].plot(x, y, marker='x', color=c)
    # ax[1].plot(coordx2, coordy2, marker='x', color=c)

    plt.savefig(f'garbage/qi{qi}gnd_imid={gnd_imid}.jpg', dpi=200)
    plt.close(fig)
