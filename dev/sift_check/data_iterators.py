from dataclasses import dataclass
from typing import Any
import abc
import numpy as np


@dataclass
class DataItem:
    index: int
    vecs: Any
    centroid_ids: Any
    coordx: Any
    coordy: Any
    scales: Any
    residuals: Any
    
    def get(self):
        return (
            self.index,
            self.vecs,
            self.centroid_ids,
            self.coordx,
            self.coordy,
            self.scales,
            self.residuals,
        )
    
    def get_coords(self, keys):
        return (
            self.scales[keys],
            self.coordx[keys],
            self.coordy[keys],
        )
    

class DataIterator(abc.ABC):
    
    @abc.abstractmethod
    def get_all_vecs(self):
        ...
        
    @abc.abstractmethod
    def get_all_imids(self):
        ...
        
    @abc.abstractmethod
    def get_iter(self, idxs):
        ...
        
    @abc.abstractmethod
    def get_item(self, idx):
        ...
    

class GeneralDataIterator(DataIterator, abc.ABC):

    def get_all_vecs(self):
        return self._vecs
    
    def get_all_imids(self):
        return self._imids
        
    def get_iter(self, idxs):
        if isinstance(idxs, int):
            idxs = range(idxs)
            
        for idx in idxs:
            yield self.get_item(idx)


class HOWDataIterator(GeneralDataIterator):

    SCALES = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])

    def __init__(self, desc, vecs_centroid_ids, residual_packs, gnd=None, prefix='', include_mask=None):
        self._imids = desc[f'{prefix}imids']
        self._vecs = desc[f'{prefix}vecs']
        self._scales = self.SCALES[desc[f'{prefix}scales']]
        
        if gnd is not None:
            bbx = np.array([qgnd['bbx'][0] for qgnd in gnd['gnd']])
            bbx = bbx[self._imids]
            bby = np.array([qgnd['bbx'][1] for qgnd in gnd['gnd']])
            bby = bby[self._imids]
        else:
            bbx = bby = np.zeros_like(self._scales)
        
        self._coordx = (desc[f'{prefix}coordx'] * 16 / self._scales) + bbx
        self._coordy = (desc[f'{prefix}coordy'] * 16 / self._scales) + bby
        self._vecs_centroid_ids = vecs_centroid_ids
        self._residual_packs = residual_packs
        
        if include_mask is not None:
            self._imids = self._imids[include_mask]
            self._vecs = self._vecs[include_mask]
            self._coordx = self._coordx[include_mask]
            self._coordy = self._coordy[include_mask]
            self._scales = self._scales[include_mask]
            self._vecs_centroid_ids = self._vecs_centroid_ids[include_mask]
            self._residual_packs = self._residual_packs[include_mask]

    def get_item(self, idx):
        li = np.searchsorted(self._imids, idx, 'left')
        ri = np.searchsorted(self._imids, idx, 'right')

        vecs = self._vecs[li:ri]
        centroid_ids = self._vecs_centroid_ids[li:ri]
        coordx = self._coordx[li:ri]
        coordy = self._coordy[li:ri]
        scales = self._scales[li:ri]
        residuals = self._residual_packs[li:ri]
            
        return DataItem(idx, vecs, centroid_ids, coordx, coordy, scales, residuals)
