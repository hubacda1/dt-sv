from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle(
    '/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl'
)
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

###############
###############
###############

qi = 969
fname = gnd['imlist'][qi]

# fname = 'ashmolean_000269'

qid = engine.cid2id[f'oxb-complete/0000/{fname}']

print(qid)

geometry = engine.get_geometries(qid)#, bbox=BBox(*gnd['gnd'][qi]['bbx']))
q_centroid_ids = geometry.labels
q_coordx = geometry.positions[:, 0]
q_coordy = geometry.positions[:, 1]

if 1:
    if 1:
        if 1:
            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')

            plt.imshow(qimg)
            plt.plot(q_coordx, q_coordy, 'rx')
            plt.show()
