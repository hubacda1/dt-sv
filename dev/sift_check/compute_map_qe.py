from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

"""
ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
print(ranks.shape)
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

io_helpers.save_pickle('_computed_ranks.pkl', ranks)
"""
ranks = io_helpers.load_pickle('_computed_ranks.pkl')

###############
###############
###############

class Database:

    def __init__(self, engine, gnd):
        self.engine = engine
        self.gnd = gnd

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid


DATABASE = Database(engine, gnd)

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100
PRINT = True

ranks_copy = ranks.copy()

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = DATABASE.get_image_id(fname)

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)
    shortlist, shortlist_scores = DATABASE.get_shortlist(q_centroid_ids)

    supports = []
    As = []
    dbids = []
    gndids = []
    for dbi, gnd_imid in enumerate(shortlist[:MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        if not corrs.size:
            supports.append(0)
            As.append(np.eye(3))
            dbids.append(dbid)
            gndids.append(gnd_imid)
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, 62
        )

        # Save the support
        supports.append(total_support)
        As.append(total_A)
        dbids.append(dbid)
        gndids.append(gnd_imid)

    supports = np.array(supports)
    As = np.array(As)
    dbids = np.array(dbids)
    gndids = np.array(gndids)
    score = shortlist_scores[:MAX_SPATIAL] + supports

    #
    # QUERY EXPANSION...
    #
    sort_keys = (-score).argsort()
    sorted_supports = supports[sort_keys]
    sorted_As = As[sort_keys]
    sorted_dbids = dbids[sort_keys]
    sorted_gndids = gndids[sort_keys]

    if PRINT:
        print('=== START', qi, '===')
        print('Before QE: num_of_vw =', q_centroid_ids.size, ', max_supp =', supports.max(), ', supp_mean =', supports.mean())

        correct_all = 0
        correct_ez = 0
        correct_hard = 0
        string = ''
        for r in sorted_gndids:
            if r in gnd['gnd'][qi]['easy']:
                correct_all += 1
                correct_ez += 1
                string += '.'
            elif r in gnd['gnd'][qi]['hard']:
                correct_all += 1
                correct_hard += 1
                string += '.'
            else:
                string += 'X'
        print(
            f"In top {MAX_SPATIAL}: ",
            f"\t EZ: {(correct_ez / len(gnd['gnd'][qi]['easy'])):.2f} " if gnd['gnd'][qi]['easy'] else "\t EZ: --.-- ",
            f"\t HD: {(correct_hard / len(gnd['gnd'][qi]['hard'])):.2f} " if gnd['gnd'][qi]['hard'] else "\t HD: --.-- ",
            f"\t AL: {(correct_all / (len(gnd['gnd'][qi]['easy']) + len(gnd['gnd'][qi]['hard'])))}",
        )
        print(string)
        print()

    try:
        eq_all_visual_words, eq_visual_words, eq_coordx, eq_coordy, eq_scales = build_qe_args(
            DATABASE, sorted_supports, sorted_As, sorted_dbids, q_bbox
        )
    except AssertionError:
        print('Assertion error for qi =', qi)

        # new_order = scores?? (-supports).argsort()
        ranks_copy[qi] = shortlist
        ranks_copy[qi, :MAX_SPATIAL] = gndids[sort_keys]

        continue

    eq_visual_words = np.hstack([q_centroid_ids, eq_visual_words])
    eq_coordx = np.hstack([q_coordx, eq_coordx])
    eq_coordy = np.hstack([q_coordy, eq_coordy])
    eq_scales = np.hstack([q_scales, eq_scales])

    # We have the data, let's run the SV again
    shortlist2, shortlist2_scores = DATABASE.get_shortlist(eq_all_visual_words)

    supports = []
    As = []
    dbids = []
    gndids = []
    for dbi, gnd_imid in enumerate(shortlist2[:MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            eq_visual_words, db_centroid_ids, eq_visual_words, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        if not corrs.size:
            supports.append(0)
            As.append(np.eye(3))
            dbids.append(dbid)
            gndids.append(gnd_imid)
            continue
        
        # Pick corresponding data
        scales1 = eq_scales[corrs[:, 0]]
        coordx1 = eq_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = eq_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, 62
        )

        # Save the support
        supports.append(total_support)
        As.append(total_A)
        dbids.append(dbid)
        gndids.append(gnd_imid)

    supports = np.array(supports)
    As = np.array(As)
    dbids = np.array(dbids)
    gndids = np.array(gndids)
    scores = shortlist2_scores[:MAX_SPATIAL] + supports

    #
    # RE-RANKING
    #
    new_order = (-scores).argsort()
    ranks_copy[qi] = shortlist2
    ranks_copy[qi, :MAX_SPATIAL] = gndids[new_order]

    if PRINT:
        print('After QE: num_of_vw =', eq_visual_words.size, ', max_supp =', supports.max(), ', supp_mean =', supports.mean())

        correct_all = 0
        correct_ez = 0
        correct_hard = 0
        string = ''
        for r in ranks_copy[qi][:MAX_SPATIAL]:
            if r in gnd['gnd'][qi]['easy']:
                correct_all += 1
                correct_ez += 1
                string += '.'
            elif r in gnd['gnd'][qi]['hard']:
                correct_all += 1
                correct_hard += 1
                string += '.'
            else:
                string += 'X'
        print(
            f"In top {MAX_SPATIAL}: ",
            f"\t EZ: {(correct_ez / len(gnd['gnd'][qi]['easy'])):.2f} " if gnd['gnd'][qi]['easy'] else "\t EZ: --.-- ",
            f"\t HD: {(correct_hard / len(gnd['gnd'][qi]['hard'])):.2f} " if gnd['gnd'][qi]['hard'] else "\t HD: --.-- ",
            f"\t AL: {(correct_all / (len(gnd['gnd'][qi]['easy']) + len(gnd['gnd'][qi]['hard'])))}",
        )
        print(string)
        print()
        print()

    if not PRINT:
        if qi % 10 == 0:
            print('Done #', qi)


#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

compute_map_and_print('roxford5k', ranks_copy.T, gnd['gnd'])

"""
Assertion error for qi = 59
Assertion error for qi = 60
Assertion error for qi = 69
SV finished 269.4905219999782
>> roxford5k: mAP E: 85.84, M: 70.88, H: 48.51
>> roxford5k: mP@k[1, 5, 10] E: [97.06 90.   85.88], M: [97.14 93.71 89.71], H: [90.   76.57 68.86]

For MAX_SPATIAL=400 it is a bit better:
    SV finished 370.6447263750015
    >> roxford5k: mAP E: 86.86, M: 71.97, H: 49.1
    >> roxford5k: mP@k[1, 5, 10] E: [97.06 90.59 86.62], M: [97.14 94.29 90.29], H: [92.86 78.   69.29]

For MAX_SPATIAL=400 and MAX_QE_KEYPOINTS=2500:
    SV finished 477.8574640839943
    >> roxford5k: mAP E: 87.46, M: 72.49, H: 50.27
    >> roxford5k: mP@k[1, 5, 10] E: [97.06 91.47 87.72], M: [97.14 94.   90.43], H: [92.86 79.14 70.43]

For MAX_SPATIAL=400 and MAX_QE_KEYPOINTS=2500 and tentative_corrs_max*=[2500, 30]:

"""
