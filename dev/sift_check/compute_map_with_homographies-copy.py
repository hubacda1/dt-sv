from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    # q_vw = engine.get_vw_in_image(qid)
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
print(ranks.shape)
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

###############
###############
###############

t0 = perf_counter()
#  =========================
# MAX_SPATIAL = 100
MAX_SPATIAL = 100

ranks_copy = ranks.copy()
# scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    geometry = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][qi]['bbx']))

    # Load bounding box
    bbx, bby, bbx2, bby2 = gnd['gnd'][qi]['bbx']

    q_centroid_ids = geometry.labels
    q_coordx = geometry.positions[:, 0]
    q_coordy = geometry.positions[:, 1]
    q_scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])
    # q_scales = (geometry.positions[:, 2] + geometry.positions[:, 4]) / 2

    supports = np.zeros([MAX_SPATIAL])
    for dbi, gnd_imid in enumerate(ranks[qi, 5:MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        if f'oxb-complete/0000/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0000/{db_fname}']
        elif f'oxb-complete/0001/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0001/{db_fname}']
        else:
            raise Exception(f'Unknown image name: {db_fname}')
        db_geometry = engine.get_geometries(dbid)

        db_centroid_ids = db_geometry.labels
        db_coordx = db_geometry.positions[:, 0]
        db_coordy = db_geometry.positions[:, 1]
        db_scales = np.sqrt(db_geometry.positions[:, 2] * db_geometry.positions[:, 4])
        # db_scales = (db_geometry.positions[:, 2] + db_geometry.positions[:, 4]) / 2

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        if not corrs.size:
            supports[dbi] = 0
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]  # * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]  # * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]  # * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]  # * 16 / scales2
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()

        total_support = -1
        total_A = None
        total_mask = None

        for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):
            A = hypotheses[best_hypothesis_index]
            mask = verifications[best_hypothesis_index]

            support = (mask * _similarities).sum()

            while True:
                new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
                errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                new_mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                new_support = (new_mask * _similarities).sum()

                if new_support > support:
                    support = new_support
                    A = new_A
                    mask = new_mask
                else:
                    break

            if support > total_support:
                total_support = support
                total_A = A
                total_mask = mask

        #"""
        # After LO, proceed to homography estimation

        inliers_count = total_mask.sum()
        use_AntoRH = False
        do_homography = True

        orig_total_mask = total_mask.copy()
        orig_total_support = total_support
        print('Basic A supp:', total_support)

        ALWAYS = True
        if ALWAYS or (do_homography and inliers_count >= 5):
            i = 1
            while True:
                _x = coordx1[total_mask]
                _y = coordy1[total_mask]
                _x_prime = coordx2[total_mask]
                _y_prime = coordy2[total_mask]

                # Extend by the bounding box
                ADD_BBX = False
                if ADD_BBX:
                    _x = np.hstack([_x, [bbx, bbx2]])
                    _y = np.hstack([_y, [bby, bby2]])
                    tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
                    _x_prime = np.hstack([_x_prime, tr_coords_A[0]])
                    _y_prime = np.hstack([_y_prime, tr_coords_A[1]])

                _c1 = np.vstack([_x, _y]).T
                _c2 = np.vstack([_x_prime, _y_prime]).T

                H, homo_mask = cv.findHomography(_c1, _c2)#, method=cv.RANSAC)
                if H is None:
                    use_AntoRH = True
                    break

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)

                # print('inl-count', inliers_count, 'homosupp', support)

                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break
                i += 1
            H_sq = H.copy()

        ALWAYS = True
        if ALWAYS or (do_homography and inliers_count >= 5):
            i = 1
            total_mask = orig_total_mask.copy()
            while True:
                _x = coordx1[total_mask]
                _y = coordy1[total_mask]
                _x_prime = coordx2[total_mask]
                _y_prime = coordy2[total_mask]

                # Extend by the bounding box
                ADD_BBX = True
                if ADD_BBX:
                    _x = np.hstack([_x, [bbx, bbx2]])
                    _y = np.hstack([_y, [bby, bby2]])
                    tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
                    _x_prime = np.hstack([_x_prime, tr_coords_A[0]])
                    _y_prime = np.hstack([_y_prime, tr_coords_A[1]])

                _c1 = np.vstack([_x, _y]).T
                _c2 = np.vstack([_x_prime, _y_prime]).T

                H, homo_mask = cv.findHomography(_c1, _c2)#, method=cv.RANSAC)
                if H is None:
                    use_AntoRH = True
                    break

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)

                # print('inl-count', inliers_count, 'homosupp', support)

                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break
                i += 1
            H_sq_reg = H.copy()

        ALWAYS = True
        if ALWAYS or (do_homography and inliers_count >= 5):
            i = 1
            total_mask = orig_total_mask.copy()
            while True:
                _x = coordx1[total_mask]
                _y = coordy1[total_mask]
                _x_prime = coordx2[total_mask]
                _y_prime = coordy2[total_mask]

                # Extend by the bounding box
                ADD_BBX = False
                if ADD_BBX:
                    _x = np.hstack([_x, [bbx, bbx2]])
                    _y = np.hstack([_y, [bby, bby2]])
                    tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
                    _x_prime = np.hstack([_x_prime, tr_coords_A[0]])
                    _y_prime = np.hstack([_y_prime, tr_coords_A[1]])

                _c1 = np.vstack([_x, _y]).T
                _c2 = np.vstack([_x_prime, _y_prime]).T

                H, homo_mask = cv.findHomography(_c1, _c2, method=cv.RANSAC)
                if H is None:
                    use_AntoRH = True
                    break

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)

                # print('inl-count', inliers_count, 'homosupp', support)

                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break
                i += 1
            H_ran = H.copy()

        ALWAYS = True
        if ALWAYS or (do_homography and inliers_count >= 5):
            i = 1
            total_mask = orig_total_mask.copy()
            while True:
                _x = coordx1[total_mask]
                _y = coordy1[total_mask]
                _x_prime = coordx2[total_mask]
                _y_prime = coordy2[total_mask]

                # Extend by the bounding box
                ADD_BBX = True
                if ADD_BBX:
                    _x = np.hstack([_x, [bbx, bbx2]])
                    _y = np.hstack([_y, [bby, bby2]])
                    tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
                    _x_prime = np.hstack([_x_prime, tr_coords_A[0]])
                    _y_prime = np.hstack([_y_prime, tr_coords_A[1]])

                _c1 = np.vstack([_x, _y]).T
                _c2 = np.vstack([_x_prime, _y_prime]).T

                H, homo_mask = cv.findHomography(_c1, _c2, method=cv.RANSAC)
                if H is None:
                    use_AntoRH = True
                    break

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)

                # print('inl-count', inliers_count, 'homosupp', support)

                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break
                i += 1
            H_ran_reg = H.copy()

        print('=== H ===')
        print(H)
        print('i =', i)
        print()

        total_support = orig_total_support
        if ALWAYS or (do_homography and inliers_count >= 2 and (use_AntoRH or inliers_count < 5)):
            i = 1
            while True:
                keys = orig_total_mask.nonzero()[0]

                tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
            
                Ns = np.stack([
                    np.linalg.inv(get_denormalizing_A(coordx1[k], coordy1[k], scales1[k])) for k in keys
                ] + [get_denormalizing_A(bbx, bby, 1), get_denormalizing_A(bbx2, bby2, 1)])
                Ds = np.stack([
                    get_denormalizing_A(coordx2[k], coordy2[k], scales2[k]) for k in keys
                ] + [get_denormalizing_A(tr_coords_A[0, 0], tr_coords_A[1, 0], 1), get_denormalizing_A(tr_coords_A[0, 1], tr_coords_A[1, 1], 1)])
                    

                H2 = AntoRH(Ns, Ds)
                H2 /= H2[2, 2]

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)
                
                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break

                if support <= total_support and use_AntoRH:
                    total_support = support
                    total_mask = mask
                    break
                i += 1
            H2_reg = H2.copy()

        total_support = orig_total_support
        if ALWAYS or (do_homography and inliers_count >= 2 and (use_AntoRH or inliers_count < 5)):
            i = 1
            while True:
                keys = orig_total_mask.nonzero()[0]

                tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
            
                Ns = np.stack([
                    np.linalg.inv(get_denormalizing_A(coordx1[k], coordy1[k], scales1[k])) for k in keys
                ])
                Ds = np.stack([
                    get_denormalizing_A(coordx2[k], coordy2[k], scales2[k]) for k in keys
                ])
                    

                H2 = AntoRH(Ns, Ds)
                H2 /= H2[2, 2]

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)
                
                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break

                if support <= total_support and use_AntoRH:
                    total_support = support
                    total_mask = mask
                    break
                i += 1
            
            H2_rh_orig = H2.copy()
            

        print('=== H2 ===')
        print(H2)
        print('i =', i)
        print()

        total_support = orig_total_support
        if ALWAYS or (do_homography and inliers_count >= 2 and (use_AntoRH or inliers_count < 5)):
            i = 1
            while True:
                keys = orig_total_mask.nonzero()[0]

                tr_coords_A = transform_points(total_A, [bbx, bbx2], [bby, bby2])
            
                Ns = np.stack([
                    np.linalg.inv(get_denormalizing_A(coordx1[k], coordy1[k], scales1[k])) for k in keys
                ] + [
                    np.linalg.inv(get_denormalizing_A(bbx, bby, 1))
                ] + [
                    np.linalg.inv(get_denormalizing_A(bbx2, bby2, 1))
                ])
                Ds = np.stack([
                    get_denormalizing_A(coordx2[k], coordy2[k], scales2[k]) for k in keys
                ] + [
                    get_denormalizing_A(tr_coords_A[0, 0], tr_coords_A[1, 0], 1),
                ] + [
                    get_denormalizing_A(tr_coords_A[0, 1], tr_coords_A[1, 1], 1),
                ])
                    

                H2 = AntoRH(Ns, Ds)
                H2 /= H2[2, 2]

                errors = compute_errors(H[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                support = (mask * _similarities).sum()
                print('New H supp:', support)
                
                if support > total_support:
                    total_support = support
                    total_mask = mask
                else:
                    break

                if support <= total_support and use_AntoRH:
                    total_support = support
                    total_mask = mask
                    break
                i += 1

            H2_rh_reg = H2.copy()

        print('=== H2 ===')
        print(H2)
        print('i =', i)
        print()
        #"""

        # Save the support
        supports[dbi] += total_support

        DISPLAY = True
        if DISPLAY:
            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            PADDING = 75
            
            img = np.ones([max(qh, dbh), qw + dbw + PADDING, 3], dtype=int) * 255
            img[:qh, :qw] = qimg
            img[:dbh, -dbw:] = dbimg

            fig, ax = plt.subplots(1, 2, figsize=(10, 5))

            ax[0].imshow(img)
            ax[0].set_title('Simple homography')
            tr_coords_A = transform_points(total_A, coordx1[total_mask], coordy1[total_mask])
            # for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], *tr_coords_A):
                ax[0].plot(
                    [qx, dbx + qw + PADDING],
                    [qy, dby],
                    color='red',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
                ax[1].plot(
                    [qx, dbx + qw + PADDING],
                    [qy, dby],
                    color='red',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )

            bbx, bby, bbx2, bby2 = gnd['gnd'][qi]['bbx']
            tr_coords_A = transform_points(total_A, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            tr_coords_Hsq = transform_points(H_sq, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            tr_coords_H2 = transform_points(H2_rh_orig, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            tr_coords_Hran = transform_points(H_ran, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            ax[0].plot([bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby], color='red')
            ax[0].plot(tr_coords_A[0] + qw + PADDING, tr_coords_A[1], color='red', label='Affine')
            ax[0].plot(tr_coords_Hsq[0] + qw + PADDING, tr_coords_Hsq[1], color='blue', label='Least squares')
            ax[0].plot(tr_coords_H2[0] + qw + PADDING, tr_coords_H2[1], color='gold', label='Elliptical features')
            ax[0].plot(tr_coords_Hran[0] + qw + PADDING, tr_coords_Hran[1], color='green', label='RANSAC')
            # ax[0].set_xlabel('x')
            # ax[0].set_ylabel('y')
            ax[0].set_xticks([])
            ax[0].set_yticks([])
            # ax[0].legend(loc='upper left')



            ax[1].imshow(img)
            ax[1].set_title('Regularised homography')
            tr_coords_A = transform_points(total_A, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            tr_coords_Hsq = transform_points(H_sq_reg, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            tr_coords_H2 = transform_points(H2_rh_reg, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            tr_coords_Hran = transform_points(H_ran_reg, [bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby])
            ax[1].plot([bbx, bbx2, bbx2, bbx, bbx], [bby, bby, bby2, bby2, bby], color='red')
            ax[1].plot(tr_coords_A[0] + qw + PADDING, tr_coords_A[1], color='red', label='Affine')
            ax[1].plot(tr_coords_Hsq[0] + qw + PADDING, tr_coords_Hsq[1], color='blue', label='Least squares')
            ax[1].plot(tr_coords_H2[0] + qw + PADDING, tr_coords_H2[1], color='gold', label='Elliptical features')
            ax[1].plot(tr_coords_Hran[0] + qw + PADDING, tr_coords_Hran[1], color='green', label='RANSAC')
            # ax[1].set_xlabel('x')
            # ax[1].set_ylabel('y')
            ax[1].set_xticks([])
            ax[1].set_yticks([])
            ax[1].set_anchor('N')
            # ax[1].legend()

            handles, labels = ax[0].get_legend_handles_labels()
            ax[1].legend(handles, labels, loc='lower center', ncol=2, bbox_to_anchor=(.5, -.25))

            fig.tight_layout(w_pad=3)

            plt.savefig("compute_map_with_homographies-copy.png", bbox_inches='tight')
            # plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

            assert 0
            cont = input('Continue? [Y/n]')
            assert cont.lower() != 'n'


    new_order = (-supports).argsort()
    ranks_copy[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    if qi % 10 == 0:
        print('Done #', qi)
    
    
#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

compute_map_and_print('roxford5k', ranks_copy.T, gnd['gnd'])
