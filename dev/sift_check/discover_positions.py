from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

ranks = []
for fname in gnd['qimlist']:
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    q_vw = engine.get_vw_in_image(qid)

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
print(ranks.shape)
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

###############
###############
###############

t0 = perf_counter()
#  =========================
# MAX_SPATIAL = 100
MAX_SPATIAL = 100

ranks_copy = ranks.copy()
# scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])

scales_x = []
scales_y = []
scales_skew = []

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    geometry = engine.get_geometries(qid)

    q_centroid_ids = geometry.labels
    q_coordx = geometry.positions[:, 0]
    q_coordy = geometry.positions[:, 1]
    q_scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])
    # q_scales = (geometry.positions[:, 2] + geometry.positions[:, 4]) / 2

    supports = np.zeros([MAX_SPATIAL])
    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        if f'oxb-complete/0000/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0000/{db_fname}']
        elif f'oxb-complete/0001/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0001/{db_fname}']
        else:
            raise Exception(f'Unknown image name: {db_fname}')
        db_geometry = engine.get_geometries(dbid)

        db_centroid_ids = db_geometry.labels
        db_coordx = db_geometry.positions[:, 0]
        db_coordy = db_geometry.positions[:, 1]
        db_scales = np.sqrt(db_geometry.positions[:, 2] * db_geometry.positions[:, 4])
        # db_scales = (db_geometry.positions[:, 2] + db_geometry.positions[:, 4]) / 2

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        if not corrs.size:
            supports[dbi] = 0
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2
        
        # Generate hypotheses
        _hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )
        qr_matrix = geometries_to_matrices(corrs[:, 0], geometry.positions)
        db_matrix = geometries_to_matrices(corrs[:, 1], db_geometry.positions)
        hypotheses = db_matrix @ np.linalg.inv(qr_matrix)
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()

        total_support = -1
        total_A = None
        total_mask = None

        im_db_lo_supp = []
        
        for i, best_hypothesis_index in enumerate(sorted_hypothesis_indexes[:10]):

            total_A = hypotheses[best_hypothesis_index]
            total_mask = verifications[best_hypothesis_index]
            total_support = (total_mask * _similarities).sum()

            if total_support >= 8:
                scales_x.append(total_A[0, 0])
                scales_y.append(total_A[1, 1])
                scales_skew.append(total_A[1, 0])

            break

            A = hypotheses[best_hypothesis_index]
            mask = verifications[best_hypothesis_index]

            support = (mask * _similarities).sum()

            while True:
                new_A = affine_local_optimization(A, coordx1[mask], coordy1[mask], coordx2[mask], coordy2[mask])
                errors = compute_errors(new_A[np.newaxis], coordx1, coordy1, coordx2, coordy2)
                new_mask = verify_models(errors, corrs, inlier_threshold=62)[0]
                new_support = (new_mask * _similarities).sum()

                if new_support > support:
                    support = new_support
                    A = new_A
                    mask = new_mask
                else:
                    break

            if support > total_support:
                total_support = support
                total_A = A
                total_mask = mask
        
        # Save the support
        supports[dbi] += total_support

        DISPLAY = False
        if DISPLAY and Path(f'ellipse-vs-circles/{fname}.png').exists():
            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, supp={total_support}')
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

            cont = input('Continue? [Y/n]')
            assert cont.lower() != 'n'

            break
    
    

    if qi % 10 == 0:
        print('Done #', qi)
    
    
#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

plt.hist(scales_x)
plt.show()

plt.hist(scales_y)
plt.show()

plt.hist(np.array(scales_y) - np.array(scales_x), bins=20)
plt.show()

plt.hist(scales_skew, bins=20)
plt.show()

if True:
    import pickle
    
    with open('_discover_positions.pkl', 'wb') as f:
        pickle.dump({
            'scales_x': np.array(scales_x),
            'scales_y': np.array(scales_y),
            'scales_skew': np.array(scales_skew),
        }, f)
