from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

"""
ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
print(ranks.shape)
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

io_helpers.save_pickle('_computed_ranks.pkl', ranks)
"""
ranks = io_helpers.load_pickle('_computed_ranks.pkl')

###############
###############
###############


class Database:

    def __init__(self, engine, gnd):
        self.engine = engine
        self.gnd = gnd

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def gndid2fname(self, gnd_id):
        return self.gnd['imlist'][gnd_id]
    
    def get_lines(self, fname, **kwargs):
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)


DATABASE = Database(engine, gnd)

t0 = perf_counter()
#  =========================
IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
MAX_SPATIAL = 60
PRINT = True

stats = {
    False: [],
    True: [],  # inlier pairs
}

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = DATABASE.get_image_id(fname)

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)
    shortlist, shortlist_scores = DATABASE.get_shortlist(q_centroid_ids)

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):

        if gnd_imid in gnd['gnd'][qi]['junk']:
            # print('Junk, skippping')
            continue

        correct_pair = (gnd_imid in gnd['gnd'][qi]['easy']) or (gnd_imid in gnd['gnd'][qi]['hard'])
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

        # Detect lines too
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        assert corrs.size

        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Generate hypotheses
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        max_num_inliers = (verifications).sum(axis=1).max()

        if not new_hypotheses.size:
            print(f'qi={qi: <2} \t dbi={dbi: <2} \t gnd_imid={gnd_imid: <5} \t correct_pair={correct_pair} \t #_hyp={hypotheses.shape[0]: <3} \t max_#_inl={max_num_inliers: <3} \t new_#_hyp=--- \t new_max_#_inl=--- \t diff=---')
            continue

        # Compute errors and verify models
        new_errors = compute_errors(new_hypotheses, coordx1, coordy1, coordx2, coordy2)
        new_verifications = verify_models(new_errors, corrs, inlier_threshold=62)
        new_max_num_inliers = (new_verifications).sum(axis=1).max()

        print(f'qi={qi: <2} \t dbi={dbi: <2} \t gnd_imid={gnd_imid: <5} \t correct_pair={correct_pair} \t #_hyp={hypotheses.shape[0]: <3} \t max_#_inl={max_num_inliers: <3} \t new_#_hyp={new_hypotheses.shape[0]: <3} \t new_max_#_inl={new_max_num_inliers: <3} \t diff={(new_max_num_inliers - max_num_inliers): <2}')
        stats[correct_pair].append(new_max_num_inliers - max_num_inliers)
    

t1 = perf_counter()
print('Script took:', (t1 - t0))

io_helpers.save_pickle('_hypothesis_with_lines_compare_top_models__stats.pkl', stats)

fig, ax = plt.subplots(1, 2, figsize=(10, 7))
ax[0].hist(stats[0], bins=20)
ax[0].set_title('False pairs')
ax[1].hist(stats[1], bins=20)
ax[1].set_title('True pairs')
plt.show()
