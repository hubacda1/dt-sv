from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

"""
ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    # q_vw = engine.get_vw_in_image(qid)
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
"""
ranks = io_helpers.load_pickle('_computed_ranks.pkl')
lines_db = io_helpers.load_pickle('_lines_database.pkl')
print(ranks.shape)
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

###############
###############
###############

class Database:

    def __init__(self, engine, gnd, lines_db):
        self.engine = engine
        self.gnd = gnd
        self.lines_db = lines_db

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)



DATABASE = Database(engine, gnd, lines_db)

IMG_ROOT = Path('../../oxbuild_images')
MAX_DIST = 10

####################
####################
####################

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100

ranks_no_line_no_lo = ranks.copy()
ranks_no_line_lo = ranks.copy()
ranks_line_no_lo = ranks.copy()
ranks_line_lo = ranks.copy()
# scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)

    supports_no_line_no_lo = np.zeros([MAX_SPATIAL])
    supports_no_line_lo = np.zeros([MAX_SPATIAL])
    supports_line_no_lo = np.zeros([MAX_SPATIAL])
    supports_line_lo = np.zeros([MAX_SPATIAL])
    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):

        correct_pair = (gnd_imid in gnd['gnd'][qi]['easy']) or (gnd_imid in gnd['gnd'][qi]['hard'])
        
        db_fname = gnd['imlist'][gnd_imid]
        if f'oxb-complete/0000/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0000/{db_fname}']
        elif f'oxb-complete/0001/{db_fname}' in engine.cid2id:
            dbid = engine.cid2id[f'oxb-complete/0001/{db_fname}']
        else:
            raise Exception(f'Unknown image name: {db_fname}')
        
        db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        if not corrs.size:
            print('!!! No correspondences')
            supports_no_line_no_lo[dbi] = 0
            supports_no_line_lo[dbi] = 0
            supports_line_no_lo[dbi] = 0
            supports_line_lo[dbi] = 0
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        
        # Generate hypotheses
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        if new_hypotheses.size:
            all_hypotheses = np.vstack([hypotheses, new_hypotheses])

        ####################
        ### Point hypotheses
        ####################

        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        
        # Local optimization of the best model
        num_of_inliers = (-(verifications * _similarities[np.newaxis]).sum(axis=1))
        sorted_hypothesis_indexes = num_of_inliers.argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, 62
        )

        # Save the support
        supports_no_line_no_lo[dbi] += -num_of_inliers[sorted_hypothesis_indexes[0]]
        supports_no_line_lo[dbi] += total_support

        ####################
        ### Lines hypotheses
        ####################
        errors = compute_errors(all_hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)

        num_of_inliers = (-(verifications * _similarities[np.newaxis]).sum(axis=1))
        sorted_hypothesis_indexes = num_of_inliers.argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support, total_index = local_optimization_of_list_of_models(
            all_hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, 62,
            return_index=True
        )

        best_lo_from_lines = keys[total_index] < hypotheses.shape[0]

        supports_line_no_lo[dbi] += -num_of_inliers[sorted_hypothesis_indexes[0]]
        supports_line_lo[dbi] += total_support

        print(f'qi={qi}, dbi={dbi}, gnd_imid={gnd_imid}, #h={hypotheses.shape[0]}, h_best={supports_no_line_lo[dbi]}/{supports_no_line_no_lo[dbi]}, #lh={new_hypotheses.shape[0]}, lh_best={supports_line_lo[dbi]}/{supports_line_no_lo[dbi]}, line_used={best_lo_from_lines}, correct_pair={correct_pair}')


    new_order = (-supports_no_line_no_lo).argsort()
    ranks_no_line_no_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_no_line_lo).argsort()
    ranks_no_line_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_line_no_lo).argsort()
    ranks_line_no_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_line_lo).argsort()
    ranks_line_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    
#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

print()
print('ranks_no_line_no_lo')
compute_map_and_print('roxford5k', ranks_no_line_no_lo.T, gnd['gnd'])

print()
print('ranks_no_line_lo')
compute_map_and_print('roxford5k', ranks_no_line_lo.T, gnd['gnd'])

print()
print('ranks_line_no_lo')
compute_map_and_print('roxford5k', ranks_line_no_lo.T, gnd['gnd'])

print()
print('ranks_line_lo')
compute_map_and_print('roxford5k', ranks_line_lo.T, gnd['gnd'])
