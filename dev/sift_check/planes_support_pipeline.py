from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl')

###############
###############
###############

def plot_bbox(bbox, A=None, qw=None):
    x1, y1, x2, y2 = bbox.as_tuple
    plt.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

    if A is not None:
        assert qw is not None
        tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
        plt.plot(tr_coords_A[0] + qw, tr_coords_A[1], color='red')


class Database:

    def __init__(self, engine, gnd):
        self.engine = engine
        self.gnd = gnd

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)



DATABASE = Database(engine, gnd)

IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
MAX_DIST = 10

qi = 0
gnd_imid = 69

# Rewrite the gnd_imid
[69, 1688, 616, 1745]
gnd_imid = 616

fname = gnd['qimlist'][qi]
qid = DATABASE.get_image_id(fname)

q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)
shortlist, shortlist_scores = DATABASE.get_shortlist(q_centroid_ids)

db_fname = gnd['imlist'][gnd_imid]
dbid = DATABASE.get_image_id(db_fname)
db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

# Generate correspondences
corrs, similarities = get_tentative_correspondencies(
    q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
)
_similarities = similarities * (similarities > 0)
assert corrs.size

# Pick corresponding data
scales1 = q_scales[corrs[:, 0]]
coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

scales2 = db_scales[corrs[:, 1]]
coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

# Generate hypotheses
hypotheses = generate_hypotheses(
    coordx1,
    coordy1,
    scales1,
    coordx2,
    coordy2,
    scales2,
)

# Compute errors and verify models
errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
verifications = verify_models(errors, corrs, inlier_threshold=62)

# Local optimization of the best model
sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()
keys = sorted_hypothesis_indexes[:10]

total_A, total_mask, total_support = local_optimization_of_list_of_models(
    hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, 62
)

################################################################################################################
# Run the planes & lines verification
colors = cycle(['red', 'green', 'blue', 'yellow'])
qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

qh, qw, _ = qimg.shape
dbh, dbw, _ = dbimg.shape

img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
img[:qh, :qw] = qimg
img[:dbh, qw:] = dbimg

"""
plt.imshow(img)
for x1, y1, x2, y2 in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
    plt.plot([x1, x2 + qw], [y1, y2], color='red', linestyle='dashed', linewidth=1)

plt.show()
assert 0
"""

MIN_LINE_LENGTHS = 40
MAX_LINE_LENGTH = 110
q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=True)
db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=True)

db_line_eqs = get_line_equations(db_lines)

if 0:
    trans_pts_1 = transform_points(total_A, q_lines[:, 0], q_lines[:, 1])
    trans_pts_1 = np.vstack([*trans_pts_1, np.ones_like(trans_pts_1[0])])
    trans_pts_2 = transform_points(total_A, q_lines[:, 2], q_lines[:, 3])
    trans_pts_2 = np.vstack([*trans_pts_2, np.ones_like(trans_pts_2[0])])

    distances_1 = np.abs(np.sum(trans_pts_1[np.newaxis, :, :] * db_line_eqs[:, :, np.newaxis], axis=1))
    distances_2 = np.abs(np.sum(trans_pts_2[np.newaxis, :, :] * db_line_eqs[:, :, np.newaxis], axis=1))

    close_enough = np.any((distances_1 <= MAX_DIST) & (distances_2 <= MAX_DIST), axis=0)
    idxs = np.where(close_enough)[0]

    plt.figure(figsize=(10, 7))
    plt.title(f'lines_support={idxs.size}')
    plt.imshow(img)
    npr = np.random.RandomState(1234)
    for i in idxs:
        if not npr.rand() <= 0.2:
            continue

        x1, y1, x2, y2 = q_lines[i]

        trans_pt_1 = trans_pts_1[:, i]
        trans_pt_2 = trans_pts_2[:, i]

        c = next(colors)
        plt.plot([x1, x2], [y1, y2], color=c)
        plt.plot([x1, qw + trans_pt_1[0]], [y1, trans_pt_1[1]], color=c, linestyle='dashed', linewidth=1)
        plt.plot([x2, qw + trans_pt_2[0]], [y2, trans_pt_2[1]], color=c, linestyle='dashed', linewidth=1)

    plot_bbox(q_bbox, total_A, qw)
    plt.show()


PLOT_ALL_INLIER_MUTUAL_CORRESPONDENCES = False
if PLOT_ALL_INLIER_MUTUAL_CORRESPONDENCES:
    colors = cycle(['red', 'green', 'blue', 'yellow'])
    n_inliers = 0
    npr = np.random.RandomState(1234)
    plt.figure(figsize=(10, 7))
    plt.imshow(img)
    for x1, y1, x2, y2 in q_lines:

        transformed_pts = transform_points(total_A, [x1, x2], [y1, y2])
        trans_pt_1, trans_pt_2 = np.vstack([*transformed_pts, [1, 1]]).T

        distances_1 = np.abs(np.sum(trans_pt_1 * db_line_eqs, axis=1))
        distances_2 = np.abs(np.sum(trans_pt_2 * db_line_eqs, axis=1))

        d1am = distances_1.argmin()
        d2am = distances_2.argmin()

        if d1am == d2am and distances_1[d1am] <= MAX_DIST and distances_2[d2am] <= MAX_DIST:
            (_, t1), (_, t2) = project_on_line(db_lines[d1am], trans_pt_1[:2], trans_pt_2[:2])
            if 0 <= t1 <= 1 or 0 <= t2 <= 1 or np.sign([t1, t2]).sum() == 0:
                n_inliers += 1
                plt.plot([x1, x2], [y1, y2], color='red')
                plt.plot([x1, qw + trans_pt_1[0]], [y1, trans_pt_1[1]], color='red', linestyle='dashed', linewidth=1)
                plt.plot([x2, qw + trans_pt_2[0]], [y2, trans_pt_2[1]], color='red', linestyle='dashed', linewidth=1)

    plt.title(f'Number of correspondences: {n_inliers}')
    plot_bbox(q_bbox, total_A, qw)
    plt.show()


PLOT_ALL_INLIER_CORRESPONDENCES = False
if PLOT_ALL_INLIER_CORRESPONDENCES:
    colors = cycle(['red', 'green', 'blue', 'yellow'])
    n_inliers = 0
    npr = np.random.RandomState(1234)
    plt.figure(figsize=(10, 7))
    plt.imshow(img)
    for x1, y1, x2, y2 in q_lines:

        transformed_pts = transform_points(total_A, [x1, x2], [y1, y2])
        trans_pt_1, trans_pt_2 = np.vstack([*transformed_pts, [1, 1]]).T

        distances_1 = np.abs(np.sum(trans_pt_1 * db_line_eqs, axis=1))
        distances_2 = np.abs(np.sum(trans_pt_2 * db_line_eqs, axis=1))

        inlier = (distances_1 <= MAX_DIST) & (distances_2 <= MAX_DIST)
        inlier_idxs = np.where(inlier)[0]
        if inlier_idxs.size:

            for idx in inlier_idxs:
                (_, t1), (_, t2) = project_on_line(db_lines[idx], trans_pt_1[:2], trans_pt_2[:2])
                if 0 <= t1 <= 1 or 0 <= t2 <= 1 or np.sign([t1, t2]).sum() == 0:
                    break
            else:
                continue

            n_inliers += 1
            if not npr.rand() <= 0.2:
                continue

            c = next(colors)
            plt.plot([x1, x2], [y1, y2], color=c)
            plt.plot([x1, qw + trans_pt_1[0]], [y1, trans_pt_1[1]], color=c, linestyle='dashed', linewidth=1)
            plt.plot([x2, qw + trans_pt_2[0]], [y2, trans_pt_2[1]], color=c, linestyle='dashed', linewidth=1)

    plt.title(f'Number of correspondences: {n_inliers}')
    plot_bbox(q_bbox, total_A, qw)
    plt.show()


PLOT_INDIVIDUAL_CORRESPONDENCES = True
if PLOT_INDIVIDUAL_CORRESPONDENCES:
    colors = cycle(['red', 'green', 'blue', 'yellow'])
    n_inliers = 0
    npr = np.random.RandomState(1234)
    for x1, y1, x2, y2 in q_lines:

        transformed_pts = transform_points(total_A, [x1, x2], [y1, y2])
        trans_pt_1, trans_pt_2 = np.vstack([*transformed_pts, [1, 1]]).T

        distances_1 = np.abs(np.sum(trans_pt_1 * db_line_eqs, axis=1))
        distances_2 = np.abs(np.sum(trans_pt_2 * db_line_eqs, axis=1))

        inlier = (distances_1 <= MAX_DIST) & (distances_2 <= MAX_DIST)
        inlier_idxs = np.where(inlier)[0]
        if inlier_idxs.size:
            plt.figure(figsize=(10, 7))
            plt.imshow(img)
            for idx in inlier_idxs:
                _x1, _y1, _x2, _y2 = db_lines[idx]

                (_, t1), (_, t2) = project_on_line(db_lines[idx], trans_pt_1[:2], trans_pt_2[:2])
                if 0 <= t1 <= 1 or 0 <= t2 <= 1 or np.sign([t1, t2]).sum() == 0:
                    plt.plot([qw + _x1, qw + _x2], [_y1, _y2], color='red')
                    n_inliers += 1
                else:
                    plt.plot([qw + _x1, qw + _x2], [_y1, _y2], color='yellow')
                

            plt.plot([x1, x2], [y1, y2], color='red')
            plt.plot([x1, qw + trans_pt_1[0]], [y1, trans_pt_1[1]], color='red', linestyle='dashed', linewidth=1)
            plt.plot([x2, qw + trans_pt_2[0]], [y2, trans_pt_2[1]], color='red', linestyle='dashed', linewidth=1)
            plot_bbox(q_bbox, total_A, qw)
            plt.show()

            cont = input('Continue? [Y/n]')
            assert cont.lower() != 'n'
