import os


def cid2filename(cid, prefix=''):
    if len(cid) < 6:
        filename = os.path.join(prefix, cid[-2:], cid[-4:-2], cid)
    else:
        filename = os.path.join(prefix, cid[-2:], cid[-4:-2], cid[-6:-4], cid)
    return filename
