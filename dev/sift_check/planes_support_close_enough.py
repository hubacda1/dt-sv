from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

"""
ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
print(ranks.shape)
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

io_helpers.save_pickle('_computed_ranks.pkl', ranks)
"""
ranks = io_helpers.load_pickle('_computed_ranks.pkl')

###############
###############
###############

class Database:

    def __init__(self, engine, gnd):
        self.engine = engine
        self.gnd = gnd

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None):
        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        return centroid_ids, coordx, coordy, scales

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname):
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'))



DATABASE = Database(engine, gnd)

t0 = perf_counter()
#  =========================
IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
MAX_SPATIAL = 100
PRINT = True
MAX_DIST = 10

ranks_copy = ranks.copy()

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = DATABASE.get_image_id(fname)

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_centroid_ids, q_coordx, q_coordy, q_scales = DATABASE.get_geometries(qid, bbox=q_bbox)
    shortlist, shortlist_scores = DATABASE.get_shortlist(q_centroid_ids)

    supports = []
    As = []
    dbids = []
    gndids = []
    for dbi, gnd_imid in enumerate(shortlist[:MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        db_centroid_ids, db_coordx, db_coordy, db_scales = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_centroid_ids, db_centroid_ids, lambda x, y: [1]
        )
        _similarities = similarities * (similarities > 0)
        
        if not corrs.size:
            supports.append(0)
            As.append(np.eye(3))
            dbids.append(dbid)
            gndids.append(gnd_imid)
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
        )
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=62)
        
        # Local optimization of the best model
        sorted_hypothesis_indexes = (-(verifications * _similarities[np.newaxis]).sum(axis=1)).argsort()
        keys = sorted_hypothesis_indexes[:10]

        total_A, total_mask, total_support = local_optimization_of_list_of_models(
            hypotheses[keys], verifications[keys], _similarities, corrs, coordx1, coordy1, coordx2, coordy2, 62
        )

        ################################################################################################################
        # Run the planes & lines verification
        colors = cycle(['red', 'green', 'blue', 'yellow'])
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

        qh, qw, _ = qimg.shape
        dbh, dbw, _ = dbimg.shape
            
        img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
        img[:qh, :qw] = qimg
        img[:dbh, qw:] = dbimg

        q_lines = DATABASE.get_lines(fname)
        db_lines = DATABASE.get_lines(db_fname)

        def plot_bbox(bbox, A=None, qw=None):
            x1, y1, x2, y2 = bbox.as_tuple
            plt.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

            if A is not None:
                assert qw is not None
                tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                plt.plot(tr_coords_A[0] + qw, tr_coords_A[1], color='red')

        db_line_eqs = get_line_equations(db_lines)


        if 1:
            trans_pts_1 = transform_points(total_A, q_lines[:, 0], q_lines[:, 1])
            trans_pts_1 = np.vstack([*trans_pts_1, np.ones_like(trans_pts_1[0])])
            trans_pts_2 = transform_points(total_A, q_lines[:, 2], q_lines[:, 3])
            trans_pts_2 = np.vstack([*trans_pts_2, np.ones_like(trans_pts_2[0])])

            distances_1 = np.abs(np.sum(trans_pts_1[np.newaxis, :, :] * db_line_eqs[:, :, np.newaxis], axis=1))
            distances_2 = np.abs(np.sum(trans_pts_2[np.newaxis, :, :] * db_line_eqs[:, :, np.newaxis], axis=1))

            close_enough = np.any((distances_1 <= MAX_DIST) & (distances_2 <= MAX_DIST), axis=0)
            idxs = np.where(close_enough)[0]

            plt.figure(figsize=(10, 7))
            plt.title(f'lines_support={idxs.size}')
            plt.imshow(img)
            npr = np.random.RandomState(1234)
            for i in idxs:
                #if not npr.rand() <= 0.2:
                #    continue

                x1, y1, x2, y2 = q_lines[i]

                trans_pt_1 = trans_pts_1[:, i]
                trans_pt_2 = trans_pts_2[:, i]

                c = next(colors)
                plt.plot([x1, x2], [y1, y2], color=c)
                plt.plot([x1, qw + trans_pt_1[0]], [y1, trans_pt_1[1]], color=c, linestyle='dashed', linewidth=1)
                plt.plot([x2, qw + trans_pt_2[0]], [y2, trans_pt_2[1]], color=c, linestyle='dashed', linewidth=1)

            plot_bbox(q_bbox, total_A, qw)
            plt.show()

        print()
        print()
        print()
        print()
        assert 0
        
        colors = cycle(['red', 'green', 'blue', 'yellow'])
        # plt.figure(figsize=(10, 7))
        # plt.imshow(img)
        n_inliers = 0
        npr = np.random.RandomState(1234)
        for x1, y1, x2, y2 in q_lines:

            transformed_pts = transform_points(total_A, [x1, x2], [y1, y2])
            trans_pt_1, trans_pt_2 = np.vstack([*transformed_pts, [1, 1]]).T

            distances_1 = np.abs(np.sum(trans_pt_1 * db_line_eqs, axis=1))
            distances_2 = np.abs(np.sum(trans_pt_2 * db_line_eqs, axis=1))

            inlier = (distances_1 <= MAX_DIST) & (distances_2 <= MAX_DIST)
            inlier_idxs = np.where(inlier)[0]
            if inlier_idxs.size:
                c = next(colors)
                n_inliers += 1

                plt.figure(figsize=(10, 7))
                plt.imshow(img)
                for idx in inlier_idxs:
                    _x1, _y1, _x2, _y2 = db_lines[idx]
                    plt.plot([qw + _x1, qw + _x2], [_y1, _y2], color=c)

                plt.plot([x1, x2], [y1, y2], color=c)
                plt.plot([x1, qw + trans_pt_1[0]], [y1, trans_pt_1[1]], color=c, linestyle='dashed', linewidth=1)
                plt.plot([x2, qw + trans_pt_2[0]], [y2, trans_pt_2[1]], color=c, linestyle='dashed', linewidth=1)
                plot_bbox(q_bbox, total_A, qw)
                plt.show()

        # plt.title(f'n_inliers={n_inliers}')
        # plot_bbox(q_bbox, total_A, qw)
        # plt.show()


        print(qi, '--', gnd_imid)

        assert 0

        # Save the support
        supports.append(total_support)
        As.append(total_A)
        dbids.append(dbid)
        gndids.append(gnd_imid)

    supports = np.array(supports)
    As = np.array(As)
    dbids = np.array(dbids)
    gndids = np.array(gndids)
    scores = shortlist_scores[:MAX_SPATIAL] + supports

    #
    # RE-RANKING
    #
    new_order = (-scores).argsort()
    ranks_copy[qi] = shortlist
    ranks_copy[qi, :MAX_SPATIAL] = gndids[new_order]

    if not PRINT:
        if qi % 10 == 0:
            print('Done #', qi)
