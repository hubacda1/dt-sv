from dataclasses import dataclass
from typing import Dict

import numpy as np

from .engine_config import EngineConfig


@dataclass
class Data:
    engine_config: EngineConfig
    invfile_bytes: bytes
    invfile_head: np.recarray
    idf: np.ndarray
    cid2id: Dict[int, int]
    id2cid: Dict[int, int]
    lengths: np.ndarray
    geometries_path: str
    image_sizes: np.ndarray
