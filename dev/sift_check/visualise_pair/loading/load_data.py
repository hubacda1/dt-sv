import pickle
from typing import Dict

import numpy as np

from utilities import BinaryFile, dotdict, sizeof_uint64_t, sizeof_unsigned_char, sizeof_unsigned_int
from .data_config import Data
from .engine_config import EngineConfig


def load_data(
        invfile_path: str,
        cached_lengths_path: str,
        geometries_path: str,
        cid2id_path: str,
        id2cid_path: str,
        image_sizes_path: str,
        options: Dict[str, str]
):
    with BinaryFile(invfile_path) as f:
        engine_version = f.read_string(8)
        n_vw = f.read_int()  # number of clusters
        n_documents = f.read_int()  # number of images

        # Reading actual inverted file matrix: visual words
        data_length_in_bytes = f.read_uint64()
        data = f.read_bytes(data_length_in_bytes * sizeof_uint64_t)

        sizeof_ifile_head = sizeof_uint64_t + sizeof_unsigned_int + sizeof_unsigned_char
        ifile_head_bytes = f.read_bytes(n_vw * sizeof_ifile_head)
        ifile_head = np.frombuffer(ifile_head_bytes, dtype=(np.dtype([("offset", "Q"), (("word_frequency", "wf"), "I"), ("max_bit", "B")])))
        ifile_head = ifile_head.view(np.recarray)

        code_bits = f.read_list(4, "int")  # stuff for compression of the inverted file
        # We only need the first three values though, the last one is specified by each VW header
        code_bits = tuple(code_bits[:3])

        # Using document frequency, in how many documents does the vw appear
        document_frequencies = f.read_list(n_vw, "unsigned")
        document_frequencies = np.array(document_frequencies)

        idf = np.log(n_documents / document_frequencies)
        idf[np.isinf(idf)] = 0

    with BinaryFile(cached_lengths_path) as f:
        cached_lengths = f.read_list(n_documents, "float")
        cached_lengths = np.array(cached_lengths, dtype=np.float32)

    with open(cid2id_path, "rb") as f:
        cid2id = pickle.load(f)

    with open(id2cid_path, "rb") as f:
        id2cid = pickle.load(f)

    image_sizes = np.load(image_sizes_path)

    engine_config = EngineConfig(
            version=engine_version,
            n_vw=n_vw,
            n_documents=n_documents,
            code_bits=code_bits,
            options=dotdict.from_dict(options),
    )

    return Data(
            engine_config=engine_config,
            invfile_bytes=data,
            invfile_head=ifile_head,
            idf=idf,
            cid2id=cid2id,
            id2cid=id2cid,
            lengths=cached_lengths,
            geometries_path=geometries_path,
            image_sizes=image_sizes
    )
