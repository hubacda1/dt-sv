from typing import Optional, BinaryIO
import struct


class BinaryFile:
    def __init__(self, filename: str):
        self.filename = filename
        self.file: Optional[BinaryIO] = None

    def __enter__(self):
        self.file = open(self.filename, "rb")
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.file.close()

    def read_bytes(self, n: int):
        return self.file.read(n)

    def read_int(self):
        return int.from_bytes(self.read_bytes(4), "little")

    def read_unsigned(self):
        return int.from_bytes(self.read_bytes(4), "little", signed=False)

    def read_uint64(self):
        return int.from_bytes(self.read_bytes(8), "little")

    def read_string(self, n: int):
        return self.read_bytes(n).decode("utf-8")

    def read_list(self, n: int, t: str):
        if t == "int":
            read_bytes = self.read_bytes(4 * n)
            return struct.unpack("<" + "i" * n, read_bytes)

        if t == "unsigned":
            read_bytes = self.read_bytes(4 * n)
            return struct.unpack("<" + "I" * n, read_bytes)

        if t == "float":
            read_bytes = self.read_bytes(4 * n)
            return struct.unpack("<" + "f" * n, read_bytes)

        raise NotImplementedError(f"read_list with the type '{t}' is not implemented")
