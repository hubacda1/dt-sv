# noinspection PyPep8Naming
class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    @staticmethod
    def from_dict(d: dict):
        """Create a dotdict from a dictionary"""

        result = dotdict()

        for k, v in d.items():
            if isinstance(v, dict):
                result[k] = dotdict.from_dict(v)
            else:
                result[k] = v
        
        return result
