from pathlib import Path
import sys
sys.path.insert(0, str(Path(__file__).parent.parent))
sys.path.insert(0, str(Path(__file__).parent.parent.parent.parent))
assert Path('.').resolve() != Path(__file__).parent.resolve(), 'Please, run the script from the first folder, not from the subfolder.'

from collections import defaultdict
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from sift_check.utilities import BBox
# from utilities import BBox
from how_devel.spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

from sift_check.loading import load_data
from sift_check.engine import Engine
# from loading import load_data
# from engine import Engine

################
## INPUT ARGS ##
################

# assert len(sys.argv) == 3, 'Incorrect args...'

# iSTART = int(sys.argv[1])
# iEND = int(sys.argv[2])

# OUTPUT_FOLDER = Path(__file__).parent / Path('compute_map_parallel/mid_results')
# OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

################
## SETUP COSS ##
################

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../../data-oxford/oxford_geometries.h5',
    cid2id_path='../../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle('../../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

"""
ranks = []
for i, fname in enumerate(gnd['qimlist']):
    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    
    # q_vw = engine.get_vw_in_image(qid)
    q_vw = engine.get_geometries(qid, bbox=BBox(*gnd['gnd'][i]['bbx'])).labels

    idxs, scores = engine.query(q_vw, top_k=10000)

    out = []
    for idx in idxs:
        key = engine.id2cid[idx].rsplit('/', 1)[1]
        if key in imlist_map:
            out.append(imlist_map[key])
    
    ranks.append(out)

ranks = np.array(ranks)
"""
ranks = io_helpers.load_pickle('../_computed_ranks.pkl')
lines_db = io_helpers.load_pickle('../_lines_database.pkl')
print('=== benchmark ===')
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])
print()

##############
## DATABASE ##
##############

class Database:

    def __init__(self, engine, gnd, lines_db):
        self.engine = engine
        self.gnd = gnd
        self.lines_db = lines_db

        self.imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

    def get_shortlist(self, q_vw, top_k=10000):
        idxs, scores = self.engine.query(q_vw, top_k=top_k)

        out = []
        out_scores = []
        for idx, score in zip(idxs, scores):
            key = self.engine.id2cid[idx].rsplit('/', 1)[1]
            if key in self.imlist_map:
                out.append(self.imlist_map[key])
                out_scores.append(score)
        
        return np.array(out), np.array(out_scores)
    
    def get_geometries(self, id, bbox=None, query=False):

        if query:
            fname = self.gnd['qimlist'][id]
            id = engine.cid2id[f'oxb-complete/0000/{fname}']

        geometry = engine.get_geometries(id, bbox=bbox)
        
        centroid_ids = geometry.labels
        coordx = geometry.positions[:, 0]
        coordy = geometry.positions[:, 1]
        scales = np.sqrt(geometry.positions[:, 2] * geometry.positions[:, 4])

        # Fake unused variables, just to satisfy the API
        vecs = None
        residuals = np.ones_like(scales)

        return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname):
        if f'oxb-complete/0000/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0000/{fname}']
        elif f'oxb-complete/0001/{fname}' in self.engine.cid2id:
            dbid = self.engine.cid2id[f'oxb-complete/0001/{fname}']
        else:
            raise Exception(f'Unknown image name: {fname}')

        return dbid
    
    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        raise KeyError(f'fname={fname} not in line database.')
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(arg1, arg2):
        return [1]



DATABASE = Database(engine, gnd, lines_db)

IMG_ROOT = Path('../../../oxbuild_images')

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

#  =========================
MAX_SPATIAL = 100
INLIER_THRESHOLD = 62

_QUERIES = [46]
_DBIMGS = [2066]

# _QUERIES = [32]
# _DBIMGS = [2895]

_QUERIES = [0]
_DBIMGS = [69]

if len(sys.argv) == 3:
    _QUERIES = [int(sys.argv[1])]
    _DBIMGS = [int(sys.argv[2])]

# for qi, fname in enumerate(gnd['qimlist']):  # query index
for qi in _QUERIES:
    fname = gnd['qimlist'][qi]

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
    # for gnd_imid in _DBIMGS:

        if dbi <= 16: continue
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)
        assert corrs.size
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        
        # Generate hypotheses
        hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        all_hypotheses = hypotheses
        num_original_hypotheses = hypotheses.shape[0]
        # hypotheses, new_hypotheses = generate_hypotheses_with_lines(
        #     coordx1,
        #     coordy1,
        #     scales1,
        #     coordx2,
        #     coordy2,
        #     scales2,
        #     q_lines,
        #     db_lines,
        #     qimg,
        #     dbimg,
        #     q_bbox,
        #     display=False,
        #     display_final=False,
        # )
        # any_line_hypotheses = False
        # num_original_hypotheses = hypotheses.shape[0]
        # all_hypotheses = hypotheses.copy()
        # if new_hypotheses.size:
        #     any_line_hypotheses = True
        #     all_hypotheses = np.vstack([all_hypotheses, new_hypotheses])

        #     # I am debugging the line hypothesis
        #     all_hypotheses = new_hypotheses
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        determinants = np.linalg.det(hypotheses)
        scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
        verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD)

        all_errors = compute_errors(all_hypotheses, coordx1, coordy1, coordx2, coordy2)
        all_determinants = np.linalg.det(all_hypotheses)
        all_scales_ratios = scales1[np.newaxis, :] * all_determinants[:, np.newaxis] / scales2[np.newaxis, :]
        all_verifications = verify_models(all_errors, all_scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD)
        
        #####
        #####
        #####

        def evaluate_(verifications_, similarities_, hypotheses_, num_original_hypotheses_):
            # Local optimization of the weighted best model
            weighted_scores_ = -(verifications_ * similarities_[np.newaxis]).sum(axis=1)
            sorted_hypothesis_indexes_ = weighted_scores_.argsort()
            keys_ = sorted_hypothesis_indexes_[:10]

            # I am debugging the line hypothesis
            return (
                -weighted_scores_[sorted_hypothesis_indexes_[0]],
                False,
                verifications_[sorted_hypothesis_indexes_[0]],
                hypotheses_[sorted_hypothesis_indexes_[0]]
            )

            total_A_, total_mask_, total_support_, optim_index_ = local_optimization_of_list_of_models(
                hypotheses_[keys_], verifications_[keys_], similarities_, corrs, coordx1, coordy1, scales1, coordx2, coordy2, scales2,
                INLIER_THRESHOLD, return_index=True
            )
            optimal_lo_comes_from_lines_ = bool(keys_[optim_index_] >= num_original_hypotheses_)
            optimal_comes_from_lines_ = bool(sorted_hypothesis_indexes_[0] >= num_original_hypotheses_)
            
            return (
                total_support_,
                optimal_lo_comes_from_lines_,
                # -weighted_scores_[sorted_hypothesis_indexes_[0]],
                # optimal_comes_from_lines_,
                total_mask_,
                total_A_
            )

        # all weighted support
        aws_lo, aws_lo_fl, total_mask, total_A = evaluate_(all_verifications, _similarities, all_hypotheses, num_original_hypotheses)

        DISPLAY = True
        if DISPLAY:


            def plot_bbox(bbox, ax, A=None, qw=None, Acol='red'):
                x1, y1, x2, y2 = bbox.as_tuple
                ax.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

                if A is not None and qw is not None:
                    tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                    ax.plot(tr_coords_A[0] + qw, tr_coords_A[1], color=Acol)


            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, score={aws_lo}')
            plot_bbox(q_bbox, plt, all_hypotheses[0], qw, Acol='red')
            plot_bbox(q_bbox, plt, all_hypotheses[1], qw, Acol='lime')
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

            # cont = input('Continue? [Y/n]')
            # assert cont.lower() != 'n'
            assert 0
