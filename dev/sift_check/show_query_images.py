from collections import defaultdict
from pathlib import Path
from time import perf_counter

import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

from engine import Engine
from loading import load_data
from utilities import dotdict, read_yaml, cid2filename, BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

engine_data = load_data(
    invfile_path='../data-oxford/oxford_invfile.dat',
    cached_lengths_path='../data-oxford/oxford_cached_lengths.dat',
    geometries_path='../data-oxford/oxford_geometries.h5',
    cid2id_path='../data-oxford/oxford_cid2id.pkl',
    id2cid_path='../data-oxford/oxford_id2cid.pkl',
    image_sizes_path='../data-oxford/oxford_image_sizes.npy',
    options={},
)
engine = Engine(engine_data)

gnd = io_helpers.load_pickle(
    '/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/asmk/data/test/roxford5k/gnd_roxford5k.pkl'
)
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

###############
###############
###############

COLS = 4
IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')

for qi, fname in enumerate(gnd['qimlist']):  # query index

    qid = engine.cid2id[f'oxb-complete/0000/{fname}']
    geometry = engine.get_geometries(qid)#, bbox=BBox(*gnd['gnd'][qi]['bbx']))
    q_centroid_ids = geometry.labels
    q_coordx = geometry.positions[:, 0]
    q_coordy = geometry.positions[:, 1]

    qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')

    fig, ax = plt.subplots(1, COLS, figsize=(15, 7))
    fig.suptitle(f'QUERY qi={qi}')

    ax[0].imshow(qimg)
    # ax[0].plot(q_coordx, q_coordy, 'rx')
    ax[0].set_title(f'#{qid} {fname}')

    for axi, gnd_imid in enumerate(gnd['gnd'][qi]['easy'][:COLS - 1], start=1):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

        ax[axi].imshow(dbimg)
        ax[axi].set_title(f'#{gnd_imid} {db_fname}')

    plt.show()

    inp = input('Continue? [Y/n]')
    assert inp != 'n'

    """
    Two planes:
    - qi=25, #1253 and db #2468
    - qi=27, #1241 and db #2468
    - qi=44, #1996 and db #214
    """
