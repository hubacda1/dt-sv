from pathlib import Path
from asmk import io_helpers
import matplotlib.pyplot as plt

IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')

QNAME = 'all_souls_000013'
DBNAME = 'all_souls_000126'
DBNAME = 'all_souls_000153'


lines1 = io_helpers.load_pickle('_lines_database.pkl')
lines2 = io_helpers.load_pickle('_lines_database_htlcnn.pkl')

fig, ax = plt.subplots(2, 2)
for i, imname in enumerate([QNAME, DBNAME]):
    img = plt.imread(IMG_ROOT / f'{imname}.jpg')

    ax[i, 0].set_title(f'IMG {i}, Lines 1')
    ax[i, 0].imshow(img)
    for x1, y1, x2, y2 in lines1[imname]:
        ax[i, 0].plot([x1, x2], [y1, y2])

    ax[i, 1].set_title(f'IMG {i}, Lines 2')
    ax[i, 1].imshow(img)
    for x1, y1, x2, y2 in lines2[imname]:
        ax[i, 1].plot([x1, x2], [y1, y2])

fig.tight_layout()
plt.show()
