from pathlib import Path
from asmk import io_helpers
import matplotlib.pyplot as plt

IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')

NAME = 'all_souls_000153'

lines = io_helpers.load_pickle('_lines_database.pkl')
# lines = io_helpers.load_pickle('_lines_database_htlcnn.pkl')

vps = io_helpers.load_pickle('_vp_database_ownsegments.pkl')

img = plt.imread(IMG_ROOT / f'{NAME}.jpg')

plt.title(f'{NAME}, Lines 1')
plt.imshow(img)
for x1, y1, x2, y2 in lines[NAME]:
    plt.plot([x1, x2], [y1, y2])

print([vp for vp in vps[NAME][:2]])

plt.plot(*vps[NAME][0]['coords'][:2], 'r.')
plt.plot(*vps[NAME][1]['coords'][:2], 'r.')

plt.show()
