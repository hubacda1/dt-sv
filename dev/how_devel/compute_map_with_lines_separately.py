from collections import defaultdict
from pathlib import Path
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
# dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = np.load('../desc_vecs-quantized-dbvecs_centroid_ids.npy')

q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    qvecs_centroid_ids,
    dbvecs_centroid_ids,
    q_residual_packs,
    db_residual_packs,
])
"""
qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs, db_residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../oxbuild_images')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }

        self.scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])
        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        q = ['', 'q'][query]

        ql = np.searchsorted(self.desc[f'{q}imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'{q}imids'], id, 'right')

        tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc[f'{q}vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[f'{q}vecs_centroid_ids'][ql:qr]
        scales = self.scales[self.desc[f'{q}scales'][ql:qr]]
        coordx = (self.desc[f'{q}coordx'][ql:qr] * 16 / scales) + tx
        coordy = (self.desc[f'{q}coordy'][ql:qr] * 16 / scales) + ty
        residuals = self.residual_packs[f'{q}residual_packs'][ql:qr]

        if bbox:
            mask = is_in_bbox(bbox, np.array([coordx, coordy]))
            return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        else:
            return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim



lines_db = io_helpers.load_pickle('../sift_check/_lines_database.pkl')
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                    db_residual_packs)

"""
qi = 0
q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)
fname = gnd['qimlist'][qi]

qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
plt.imshow(qimg, cmap='gray')
plt.plot(coordx, coordy, 'r.')
plt.show()
"""

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

print('Start spatial verification.')

t0 = perf_counter()
#  =========================
MAX_SPATIAL = 100
INLIER_THRESHOLD=62

ranks_all_weighted = ranks.copy()
ranks_all_weighted_lo = ranks.copy()
ranks_all_inliers = ranks.copy()
ranks_all_inliers_lo = ranks.copy()

ranks_basic_weighted = ranks.copy()
ranks_basic_weighted_lo = ranks.copy()
ranks_basic_inliers = ranks.copy()
ranks_basic_inliers_lo = ranks.copy()

for qi, fname in enumerate(gnd['qimlist']):  # query index

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)

    supports_all_weighted = np.zeros([MAX_SPATIAL])
    supports_all_weighted_lo = np.zeros([MAX_SPATIAL])
    supports_all_inliers = np.zeros([MAX_SPATIAL])
    supports_all_inliers_lo = np.zeros([MAX_SPATIAL])

    supports_basic_weighted = np.zeros([MAX_SPATIAL])
    supports_basic_weighted_lo = np.zeros([MAX_SPATIAL])
    supports_basic_inliers = np.zeros([MAX_SPATIAL])
    supports_basic_inliers_lo = np.zeros([MAX_SPATIAL])

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)

        if not corrs.size:
            supports_all_weighted[dbi] = 0
            supports_all_weighted_lo[dbi] = 0
            supports_all_inliers[dbi] = 0
            supports_all_inliers_lo[dbi] = 0

            supports_basic_weighted[dbi] = 0
            supports_basic_weighted_lo[dbi] = 0
            supports_basic_inliers[dbi] = 0
            supports_basic_inliers_lo[dbi] = 0
            continue
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        
        # Generate hypotheses
        # hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        any_line_hypotheses = False
        num_original_hypotheses = hypotheses.shape[0]
        all_hypotheses = hypotheses.copy()
        if new_hypotheses.size:
            any_line_hypotheses = True
            all_hypotheses = np.vstack([all_hypotheses, new_hypotheses])
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        verifications = verify_models(errors, corrs, inlier_threshold=INLIER_THRESHOLD)

        all_errors = compute_errors(all_hypotheses, coordx1, coordy1, coordx2, coordy2)
        all_verifications = verify_models(all_errors, corrs, inlier_threshold=INLIER_THRESHOLD)
        
        #####
        #####
        #####

        def evaluate_(verifications_, similarities_, hypotheses_, num_original_hypotheses_):
            # Local optimization of the weighted best model
            weighted_scores_ = -(verifications_ * similarities_[np.newaxis]).sum(axis=1)
            sorted_hypothesis_indexes_ = weighted_scores_.argsort()
            keys_ = sorted_hypothesis_indexes_[:10]

            total_A_, total_mask_, total_support_, optim_index_ = local_optimization_of_list_of_models(
                hypotheses_[keys_], verifications_[keys_], similarities_, corrs, coordx1, coordy1, coordx2, coordy2,
                INLIER_THRESHOLD, return_index=True
            )
            optimal_lo_comes_from_lines_ = bool(keys_[optim_index_] >= num_original_hypotheses_)
            optimal_comes_from_lines_ = bool(sorted_hypothesis_indexes_[0] >= num_original_hypotheses_)
            
            return (
                total_support_,
                optimal_lo_comes_from_lines_,
                -weighted_scores_[sorted_hypothesis_indexes_[0]],
                optimal_comes_from_lines_,
            )
        

        # all weighted support
        aws_lo, aws_lo_fl, aws, aws_fl = evaluate_(all_verifications, _similarities, all_hypotheses, num_original_hypotheses)

        # only basic weighted support
        ws_lo, ws_lo_fl, ws, ws_fl = evaluate_(verifications, _similarities, hypotheses, num_original_hypotheses)
        assert ws_lo_fl is False and ws_fl is False, f'No lines | weighted | but optim from lines: {ws_lo_fl}, {ws_fl}'

        # all inl support
        ainlsup_lo, ainlsup_lo_fl, ainlsup, ainlsup_fl = evaluate_(all_verifications, np.ones_like(_similarities), all_hypotheses, num_original_hypotheses)

        # only basic inl support
        inlsup_lo, inlsup_lo_fl, inlsup, inlsup_fl = evaluate_(verifications, np.ones_like(_similarities), hypotheses, num_original_hypotheses)
        assert inlsup_lo_fl is False and inlsup_fl is False, f'No lines | inlsup | but optim from lines: {ws_lo_fl}, {ws_fl}'


        supports_all_weighted[dbi] = aws
        supports_all_weighted_lo[dbi] = aws_lo
        supports_all_inliers[dbi] = ainlsup
        supports_all_inliers_lo[dbi] = ainlsup_lo

        supports_basic_weighted[dbi] = ws
        supports_basic_weighted_lo[dbi] = ws_lo
        supports_basic_inliers[dbi] = inlsup
        supports_basic_inliers_lo[dbi] = inlsup_lo

        
        # Log
        correct_pair = (gnd_imid in gnd['gnd'][qi]['easy']) or (gnd_imid in gnd['gnd'][qi]['hard'])
        print('JSON STATS:', json.dumps({
            'qi': qi,
            'dbi': dbi,
            'gnd_imid': int(gnd_imid),
            'correct_pair': correct_pair,
            'corrs': corrs.shape[0],
            'num_of_line_hypothesis': 0 if any_line_hypotheses is False else new_hypotheses.shape[0],

            'supports_all_weighted': supports_all_weighted[dbi],
            'supports_all_weighted_optim_from_lines': aws_fl,
            'supports_all_weighted_lo': supports_all_weighted_lo[dbi],
            'supports_all_weighted_lo_optim_from_lines': aws_lo_fl,
            'supports_all_inliers': supports_all_inliers[dbi],
            'supports_all_inliers_optim_from_lines': ainlsup_fl,
            'supports_all_inliers_lo': supports_all_inliers_lo[dbi],
            'supports_all_inliers_lo_optim_from_lines': ainlsup_lo_fl,

            'supports_basic_weighted': supports_basic_weighted[dbi],
            'supports_basic_weighted_lo': supports_basic_weighted_lo[dbi],
            'supports_basic_inliers': supports_basic_inliers[dbi],
            'supports_basic_inliers_lo': supports_basic_inliers_lo[dbi],
        }))

    new_order = (-supports_all_weighted).argsort()
    ranks_all_weighted[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_all_weighted_lo).argsort()
    ranks_all_weighted_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_all_inliers).argsort()
    ranks_all_inliers[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_all_inliers_lo).argsort()
    ranks_all_inliers_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_basic_weighted).argsort()
    ranks_basic_weighted[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_basic_weighted_lo).argsort()
    ranks_basic_weighted_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_basic_inliers).argsort()
    ranks_basic_inliers[qi, :MAX_SPATIAL] = ranks[qi, new_order]

    new_order = (-supports_basic_inliers_lo).argsort()
    ranks_basic_inliers_lo[qi, :MAX_SPATIAL] = ranks[qi, new_order]


#  =========================
t1 = perf_counter()
print('SV finished', t1 - t0)

print()
print('=== ranks ASMK')
compute_map_and_print('roxford5k', ranks.T, gnd['gnd'])

print()
print('=== all, ranks weighted')
compute_map_and_print('roxford5k', ranks_all_weighted.T, gnd['gnd'])

print()
print('=== all, ranks weighted lo')
compute_map_and_print('roxford5k', ranks_all_weighted_lo.T, gnd['gnd'])

print()
print('=== all, ranks inliers')
compute_map_and_print('roxford5k', ranks_all_inliers.T, gnd['gnd'])

print()
print('=== all, ranks inliers lo')
compute_map_and_print('roxford5k', ranks_all_inliers_lo.T, gnd['gnd'])

print()
print('=== basic only, ranks weighted')
compute_map_and_print('roxford5k', ranks_basic_weighted.T, gnd['gnd'])

print()
print('=== basic only, ranks weighted lo')
compute_map_and_print('roxford5k', ranks_basic_weighted_lo.T, gnd['gnd'])

print()
print('=== basic only, ranks inliers')
compute_map_and_print('roxford5k', ranks_basic_inliers.T, gnd['gnd'])

print()
print('=== basic only, ranks inliers lo')
compute_map_and_print('roxford5k', ranks_basic_inliers_lo.T, gnd['gnd'])
