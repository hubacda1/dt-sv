from collections import defaultdict
from pathlib import Path
from time import perf_counter
from itertools import cycle

import cv2
import numpy as np
import matplotlib.pyplot as plt

from utilities import BBox
from spatial_verification import *

from asmk import io_helpers
from cirtorch.utils.evaluate import compute_map_and_print

# Copy this weird configuration from the API

IMG_ROOT = Path('../../oxbuild_images')

metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')
cache = io_helpers.load_pickle('_lines_database.pkl')
gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')

###############
###############
###############

class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }

        self.scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])
        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        # print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    

DATABASE = Database(None, None, gnd, cache, None, None, None, None)

MIN_LINE_LENGTHS = 40
MAX_LINE_LENGTH = 110
MAX_SPATIAL = 100

for qi, fname in enumerate(gnd['qimlist']):  # query index
    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    qid = DATABASE.get_image_id(fname, query=True)

    if fname not in cache:
        lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[fname] = lines
        print('Query', qi, 'not in cache.')

    for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
        db_fname = gnd['imlist'][gnd_imid]
        if db_fname in cache:
            continue
        lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[db_fname] = lines
        print('Shortlist', db_fname, 'not in cache.')

    for gnd_imid in (gnd['gnd'][qi]['easy'] + gnd['gnd'][qi]['hard']):
        db_fname = gnd['imlist'][gnd_imid]
        if db_fname in cache:
            continue
        lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        cache[db_fname] = lines
        print('GND', db_fname, 'not in cache.')
    
    print(qi, 'completely done.')

io_helpers.save_pickle('_lines_database_updated.pkl', cache)
