import numpy as np
import matplotlib.pyplot as plt
from asmk import io_helpers
import sys
from pathlib import Path
from cirtorch.utils.evaluate import compute_map_and_print
from cirtorch.datasets.testdataset import configdataset

################
## INPUT ARGS ##
################

assert len(sys.argv) == 2, 'Incorrect args...'

FILE = sys.argv[1]
THRESHOLDS = np.arange(5, 70, 2)

metadata, images, ranks, scores = io_helpers.load_pickle(Path(__file__).parent.parent / '_asmk_ranks.pkl')

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])

gnd = configdataset(DATASET, f"{globals['root_path']}/test/")

###################
## COMPUTE STUFF ##
###################

data = io_helpers.load_pickle(FILE)
MAX_SPATIAL = 100

print(data.shape)

assert data.shape[1] == MAX_SPATIAL

mEs, mMs, mHs = [], [], []

for thr in THRESHOLDS:

    ranks_copy = ranks.copy()

    for qi in range(70):
        scores = data[qi]

        new_order = (-scores * (scores >= thr)).argsort(kind='stable')
        ranks_copy[qi, :MAX_SPATIAL] = ranks[qi, new_order]
    
    mapE, mapM, mapH = compute_map_and_print('roxford5k', ranks_copy.T, gnd['gnd'], ret=True)

    mEs.append(mapE)
    mMs.append(mapM)
    mHs.append(mapH)

fig, ax = plt.subplots(1, 3)

ax[0].plot(THRESHOLDS, mEs, color='green', label='Easy')
ax[1].plot(THRESHOLDS, mMs, color='orange', label='Medium')
ax[2].plot(THRESHOLDS, mHs, color='red', label='Hard')
# plt.legend()
plt.show()
