from pathlib import Path
from asmk import io_helpers
import numpy as np

print('Starting integration')

INPUT_FOLDER = Path(__file__).parent / 'compute_map_parallel'

first_done = False

ranks_weighted = None
ranks_weighted_lo = None
ranks_inliers = None
ranks_inliers_lo = None

scores_weighted = np.zeros([70, 100], dtype=float)
scores_weighted_lo = np.zeros([70, 100], dtype=float)
scores_inliers = np.zeros([70, 100], dtype=float)
scores_inliers_lo = np.zeros([70, 100], dtype=float)

for file in (INPUT_FOLDER / 'mid_results').glob('*.pkl'):
    ifrom, ito = [int(item) for item in file.stem.split('-')]
    data = io_helpers.load_pickle(file)

    if first_done is False:
        ranks_weighted = data['ranks_weighted']
        ranks_weighted_lo = data['ranks_weighted_lo']
        ranks_inliers = data['ranks_inliers']
        ranks_inliers_lo = data['ranks_inliers_lo']
        first_done = True
    else:
        ranks_weighted[ifrom:ito] = data['ranks_weighted'][ifrom:ito]
        ranks_weighted_lo[ifrom:ito] = data['ranks_weighted_lo'][ifrom:ito]
        ranks_inliers[ifrom:ito] = data['ranks_inliers'][ifrom:ito]
        ranks_inliers_lo[ifrom:ito] = data['ranks_inliers_lo'][ifrom:ito]

    scores_weighted_lo[ifrom:ito, :] = data['scores_weighted_lo']
    scores_weighted[ifrom:ito, :] = data['scores_weighted']
    scores_inliers_lo[ifrom:ito, :] = data['scores_inliers_lo']
    scores_inliers[ifrom:ito, :] = data['scores_inliers']

io_helpers.save_pickle(INPUT_FOLDER / 'ranks_weighted.pkl', ranks_weighted)
io_helpers.save_pickle(INPUT_FOLDER / 'ranks_weighted_lo.pkl', ranks_weighted_lo)
io_helpers.save_pickle(INPUT_FOLDER / 'ranks_inliers.pkl', ranks_inliers)
io_helpers.save_pickle(INPUT_FOLDER / 'ranks_inliers_lo.pkl', ranks_inliers_lo)

io_helpers.save_pickle(INPUT_FOLDER / 'scores_weighted.pkl', scores_weighted)
io_helpers.save_pickle(INPUT_FOLDER / 'scores_weighted_lo.pkl', scores_weighted_lo)
io_helpers.save_pickle(INPUT_FOLDER / 'scores_inliers.pkl', scores_inliers)
io_helpers.save_pickle(INPUT_FOLDER / 'scores_inliers_lo.pkl', scores_inliers_lo)

print('Integration done')
