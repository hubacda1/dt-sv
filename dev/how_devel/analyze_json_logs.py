import json
import matplotlib.pyplot as plt
import sys

assert len(sys.argv) == 2
FILE = sys.argv[1]

with open(FILE, 'r') as f:
    data = [json.loads(d[12:]) for d in f.readlines() if d.startswith('JSON STATS:')]

for d in data:
    if d['vps1'] is not None and d['ninl_before_vps'] < 5:
        print(d)
