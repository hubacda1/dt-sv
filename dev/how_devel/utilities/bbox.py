from __future__ import annotations

from dataclasses import dataclass

import numpy as np


@dataclass
class BBox:
    x1: float = 0.
    y1: float = 0.
    x2: float = 1.
    y2: float = 1.

    def __init__(self, x1=0., y1=0., x2=1., y2=1.):
        self.x1, self.x2 = sorted([x1, x2])
        self.y1, self.y2 = sorted([y1, y2])

    @property
    def as_tuple(self):
        return self.x1, self.y1, self.x2, self.y2

    @property
    def width(self):
        return self.x2 - self.x1

    @property
    def height(self):
        return self.y2 - self.y1

    @property
    def visible_width(self):
        return min(self.x2, 1) - max(self.x1, 0)

    @property
    def visible_height(self):
        return min(self.y2, 1) - max(self.y1, 0)

    @property
    def area(self):
        return self.width * self.height

    @property
    def visible_area(self):
        return self.visible_width * self.visible_height

    @property
    def whole_visible(self):
        return self.visible_width == self.width and self.visible_height == self.height

    @property
    def corner_points(self):
        return np.array([
                [self.x1, self.y1],
                [self.x2, self.y1],
                [self.x2, self.y2],
                [self.x1, self.y2],
        ])

    def __mul__(self, other: float | int | BBox | np.ndarray | list | tuple):
        if isinstance(other, (int, float)):
            return BBox(
                    x1=self.x1 * other,
                    y1=self.y1 * other,
                    x2=self.x2 * other,
                    y2=self.y2 * other,
            )

        if isinstance(other, (np.ndarray, list, tuple)):
            if len(other) == 4:
                return BBox(
                        x1=self.x1 * other[0],
                        y1=self.y1 * other[1],
                        x2=self.x2 * other[2],
                        y2=self.y2 * other[3],
                )
            elif len(other) == 2:
                return BBox(
                        x1=self.x1 * other[0],
                        y1=self.y1 * other[1],
                        x2=self.x2 * other[0],
                        y2=self.y2 * other[1],
                )
            else:
                raise ValueError(f"Expected 2 or 4 values, got {len(other)}: {other}")

        if isinstance(other, BBox):
            return BBox(
                    x1=self.x1 * other.x1,
                    y1=self.y1 * other.y1,
                    x2=self.x2 * other.x2,
                    y2=self.y2 * other.y2,
            )

        raise TypeError(f"Unsupported operand type(s) for *: '{type(self)}' and '{type(other)}'")


if __name__ == '__main__':
    bbox = BBox(1, 2, 3, 4)
    print(bbox)
    print(bbox * 2)
    print(bbox * (2, 3))
    print(bbox * BBox(x1=1, y1=2, x2=3, y2=4))
    print(bbox * np.array([2, 3]))
    print(bbox * np.array([1, 2, 3, 4]))
