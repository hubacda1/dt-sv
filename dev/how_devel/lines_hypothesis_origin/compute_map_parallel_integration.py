from pathlib import Path
from asmk import io_helpers

print('Starting integration')

INPUT_FOLDER = Path(__file__).parent / Path('compute_parallel')

correct_pairs_diffs_inliers = []
correct_pairs_diffs_ranks = []
incorrect_pairs_diffs_inliers = []
incorrect_pairs_diffs_ranks = []

for file in (INPUT_FOLDER / 'mid_results').glob('*.pkl'):
    ifrom, ito = [int(item) for item in file.stem.split('-')]
    data = io_helpers.load_pickle(file)

    correct_pairs_diffs_inliers.extend(data['correct_pairs_diffs_inliers'])
    correct_pairs_diffs_ranks.extend(data['correct_pairs_diffs_ranks'])
    incorrect_pairs_diffs_inliers.extend(data['incorrect_pairs_diffs_inliers'])
    incorrect_pairs_diffs_ranks.extend(data['incorrect_pairs_diffs_ranks'])


io_helpers.save_pickle(INPUT_FOLDER / 'correct_pairs_diffs_inliers.pkl', correct_pairs_diffs_inliers)
io_helpers.save_pickle(INPUT_FOLDER / 'correct_pairs_diffs_ranks.pkl', correct_pairs_diffs_ranks)
io_helpers.save_pickle(INPUT_FOLDER / 'incorrect_pairs_diffs_inliers.pkl', incorrect_pairs_diffs_inliers)
io_helpers.save_pickle(INPUT_FOLDER / 'incorrect_pairs_diffs_ranks.pkl', incorrect_pairs_diffs_ranks)

print('Integration done')
