import json
import matplotlib.pyplot as plt
import numpy as np

with open('outputs/_compute_map_with_lines_separately-thr62minll40maxll110.out.txt', 'r') as f:
    data = [json.loads(d[12:]) for d in f.readlines() if d.startswith('JSON STATS:')]


DISPLAY_POINTS = False
if DISPLAY_POINTS:
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title('Weighted score, no LO')
    ax[0, 1].set_title('Weighted score, LO')
    ax[1, 0].set_title('Inliers count, no LO')
    ax[1, 1].set_title('Inliers count, LO')

    colors = ['red', 'green']
    markersize = 2
    for d in data:
        ax[0, 0].plot(d['supports_basic_weighted'], d['supports_all_weighted'], color=colors[d['correct_pair']], 
                      marker='.', markersize=markersize)
        ax[0, 1].plot(d['supports_basic_weighted_lo'], d['supports_all_weighted_lo'], color=colors[d['correct_pair']], 
                      marker='.', markersize=markersize)
        ax[1, 0].plot(d['supports_basic_inliers'], d['supports_all_inliers'], color=colors[d['correct_pair']], 
                      marker='.', markersize=markersize)
        ax[1, 1].plot(d['supports_basic_inliers_lo'], d['supports_all_inliers_lo'], color=colors[d['correct_pair']], 
                      marker='.', markersize=markersize)

    plt.show()


DISPLAY_HISTOGRAMS = True
if DISPLAY_HISTOGRAMS:
    """
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title('Weighted score, no LO')
    ax[0, 1].set_title('Weighted score, LO')
    ax[1, 0].set_title('Inliers count, no LO')
    ax[1, 1].set_title('Inliers count, LO')
    """

    stats = {
        'w': [[], []],
        'wlo': [[], []],
        'i': [[], []],
        'ilo': [[], []],
    }
    for d in data:
        stats['w'][d['correct_pair']].append(d['supports_all_weighted'] - d['supports_basic_weighted'])
        stats['wlo'][d['correct_pair']].append(d['supports_all_weighted_lo'] - d['supports_basic_weighted_lo'])
        stats['i'][d['correct_pair']].append(d['supports_all_inliers'] - d['supports_basic_inliers'])
        stats['ilo'][d['correct_pair']].append(d['supports_all_inliers_lo'] - d['supports_basic_inliers_lo'])

    bins = [-10.5, -5.5, -3.5, -2.5, -1.5, -0.5, 0.5, 1.5, 2.5, 3.5, 5.5, 10.5, 20.5]
    plt.title('Difference between "all" (lines included) hypotheses and "basic-only" hypotheses')
    plt.suptitle('Weighted results, **with LO**')
    plt.hist(np.array(stats['ilo'][0]).clip(min=-10, max=20), bins=bins, color='red', density=True, alpha=0.5, label='incorrect pairs')
    plt.hist(np.array(stats['ilo'][1]).clip(min=-10, max=20), bins=bins, color='green', density=True, alpha=0.5, label='correct pairs')
    plt.legend()
    plt.show()


BEST_IS_LINE_HYPO = False
if BEST_IS_LINE_HYPO:
    stats = {
        0: {0: 0, 1: 0},
        1: {0: 0, 1: 0},
    }
    for d in data:
        stats[d['correct_pair']][d['supports_all_weighted_lo_optim_from_lines']] += 1

    print(stats)
    norm = [sum(stats[0].values()), sum(stats[1].values())]
    print({
        0: {0: stats[0][0] / norm[0], 1: stats[0][1] / norm[0]},
        1: {0: stats[1][0] / norm[1], 1: stats[1][1] / norm[1]},
    })


CHECK_WEIRD_STATS = False
if CHECK_WEIRD_STATS:
    ##### supports_all_weighted_optim_from_lines
    hyp1 = [d['supports_all_weighted_optim_from_lines'] for d in data if not d['correct_pair']]

    ##### supports_all_weighted_lo_optim_from_lines
    hyp2 = [d['supports_all_weighted_lo_optim_from_lines'] for d in data if not d['correct_pair']]

    print(sum(hyp1), sum(hyp2))
    print(hyp1 == hyp2)
    print(hyp1[:10])
    print(hyp2[:10])


FIND_BAD_CORRECT_PAIRS = False
if FIND_BAD_CORRECT_PAIRS:
    for d in (el for el in data if el['correct_pair']):
        break
        if d['supports_all_weighted_lo'] <= 2:
            print(d)
            print('.', end='')
    print()

    for d in (el for el in data if not el['correct_pair']):
        if d['supports_all_weighted_lo'] <= 3.2 and d['supports_all_weighted_lo'] >= 3:
            print(d)
            print('.', end='')
    print()
