from collections import defaultdict
from pathlib import Path
from time import perf_counter
import json

import numpy as np
import matplotlib.pyplot as plt

# from engine import Engine
# from loading import load_data
# from utilities import dotdict, read_yaml, cid2filename, BBox
from utilities import BBox
from spatial_verification import *

from cirtorch.datasets.testdataset import configdataset
from asmk import io_helpers, ASMKMethod, functional, hamming
from examples.demo_how import build_ivf, query_ivf
from cirtorch.utils.evaluate import compute_map_and_print

################
## SETUP ASMK ##
################

PARAMETERS_PATH = 'eccv20_how_r50-_1000'
DATASET = 'roxford5k'
EVAL_FEATURES = 'how_r50-_1000'

# Global variables
package_root = Path('.').resolve().parent.parent / 'asmk'
parameters_path = package_root / "examples" / ("params/%s.yml" % PARAMETERS_PATH)
params = io_helpers.load_params(parameters_path)

globals = {}
globals["root_path"] = (package_root / params['demo_how']['data_folder'])
exp_name = Path(parameters_path).name[:-len(".yml")]
globals["exp_path"] = (package_root / params['demo_how']['exp_folder']) / exp_name

# Setup logging
logger = io_helpers.init_logger(None)
logger.info("All variables and logger set up")

# Run demo
asmk = ASMKMethod.initialize_untrained(params)
logger.info("Created uninitialized ASMK")
asmk = asmk.train_codebook(cache_path=f"{globals['exp_path']}/codebook.pkl")
logger.info("Trained ASMK")

desc = io_helpers.load_pickle(f"{globals['root_path']}/features/{DATASET}_{EVAL_FEATURES}.pkl")
gnd = configdataset(DATASET, f"{globals['root_path']}/test/")
imlist_map = {fname: i for i, fname in enumerate(gnd['imlist'])}

asmk_dataset = build_ivf(asmk, DATASET, desc, globals, logger)

"""
metadata, images, ranks, scores = asmk_dataset.query_ivf(desc['qvecs'], desc['qimids'])
io_helpers.save_pickle('_asmk_ranks.pkl', [metadata, images, ranks, scores])
compute_map_and_print(DATASET, ranks.T, gnd['gnd'])
"""
metadata, images, ranks, scores = io_helpers.load_pickle('_asmk_ranks.pkl')

#######################
## End of SETUP ASMK ##
#######################

##############
## DATABASE ##
##############

"""
qvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['qvecs'], multiple_assignment=1)[1].ravel()
# dbvecs_centroid_ids = asmk_dataset.codebook.quantize(desc['vecs'], multiple_assignment=1)[1].ravel()
dbvecs_centroid_ids = np.load('../desc_vecs-quantized-dbvecs_centroid_ids.npy')

q_centroids = asmk_dataset.codebook.centroids[qvecs_centroid_ids]
db_centroids = asmk_dataset.codebook.centroids[dbvecs_centroid_ids]

q_residual_packs = get_packed_residuals(desc['qvecs'], q_centroids, binary=asmk_dataset.kernel.binary)
db_residual_packs = get_packed_residuals(desc['vecs'], db_centroids, binary=asmk_dataset.kernel.binary)

io_helpers.save_pickle('_centroids_and_residual_packs.pkl', [
    qvecs_centroid_ids,
    dbvecs_centroid_ids,
    q_residual_packs,
    db_residual_packs,
])
"""
qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs, db_residual_packs = io_helpers.load_pickle('_centroids_and_residual_packs.pkl')
IMG_ROOT = Path('../../oxbuild_images')


class Database:

    def __init__(self, desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                 db_residual_packs):
        self.desc = desc
        self.asmk_dataset = asmk_dataset
        self.gnd = gnd
        self.lines_db = lines_db
        self.vecs_centroid_ids = {
            'vecs_centroid_ids': dbvecs_centroid_ids,
            'qvecs_centroid_ids': qvecs_centroid_ids,
        }
        self.residual_packs = {
            'residual_packs': db_residual_packs,
            'qresidual_packs': q_residual_packs,
        }

        self.scales = np.array([2.0, 1.414, 1.0, 0.707, 0.5, 0.353, 0.25])
        self.imlist_map = {
            'imlist_map': {fname: i for i, fname in enumerate(gnd['imlist'])},
            'qimlist_map': {fname: i for i, fname in enumerate(gnd['qimlist'])},
        }

    def get_shortlist(self, q_vw, top_k=10000):
        metadata, images, ranks, scores = asmk_dataset.query_ivf(q_vw, np.zeros_like(q_vw))
        ranks = ranks.squeeze()
        scores = scores.squeeze()

        return ranks, scores

    def get_geometries(self, id, bbox=None, query=False):
        q = ['', 'q'][query]

        ql = np.searchsorted(self.desc[f'{q}imids'], id, 'left')
        qr = np.searchsorted(self.desc[f'{q}imids'], id, 'right')

        tx, ty = gnd['gnd'][id]['bbx'][:2] if query else [0, 0]

        vecs = self.desc[f'{q}vecs'][ql:qr]
        centroid_ids = self.vecs_centroid_ids[f'{q}vecs_centroid_ids'][ql:qr]
        scales = self.scales[self.desc[f'{q}scales'][ql:qr]]
        coordx = (self.desc[f'{q}coordx'][ql:qr] * 16 / scales) + tx
        coordy = (self.desc[f'{q}coordy'][ql:qr] * 16 / scales) + ty
        residuals = self.residual_packs[f'{q}residual_packs'][ql:qr]

        if bbox:
            mask = is_in_bbox(bbox, np.array([coordx, coordy]))
            return vecs[mask], centroid_ids[mask], coordx[mask], coordy[mask], scales[mask], residuals[mask]
        else:
            return vecs, centroid_ids, coordx, coordy, scales, residuals

    def get_image_id(self, fname, query=False):
        q = ['', 'q'][query]
        return self.imlist_map[f'{q}imlist_map'][fname]

    def get_lines(self, fname, **kwargs):
        if fname in self.lines_db:
            return self.lines_db[fname]
        print('WARNING: Image with no pre-computed lines:', fname)
        return detect_lines(str(IMG_ROOT / f'{fname}.jpg'), **kwargs)
    
    @staticmethod
    def kernel_similarity(v1, v2):
        _, sim = asmk_dataset.kernel.similarity(v1, v2[np.newaxis], np.array([0]), alpha=1, similarity_threshold=-np.inf)
        return sim



# lines_db = io_helpers.load_pickle('../sift_check/_lines_database.pkl')
lines_db = io_helpers.load_pickle('_lines_database_htlcnn.pkl')
DATABASE = Database(desc, asmk_dataset, gnd, lines_db, qvecs_centroid_ids, dbvecs_centroid_ids, q_residual_packs,
                    db_residual_packs)

"""
qi = 0
q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
vecs, centroid_ids, coordx, coordy, scales, residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)
fname = gnd['qimlist'][qi]

qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
plt.imshow(qimg, cmap='gray')
plt.plot(coordx, coordy, 'r.')
plt.show()
"""

#####################
## End of DATABASE ##
#####################


##########################
## SPATIAL VERIFICATION ##
##########################

#  =========================
MAX_SPATIAL = 100
INLIER_THRESHOLD = 62

_QUERIES = [46]
_DBIMGS = [2066]

# _QUERIES = [32]
# _DBIMGS = [2895]

_QUERIES = [0]
_DBIMGS = [69]

_QUERIES = [51]
_DBIMGS = [2551]

# Incorrect pair, weird transformation
_QUERIES = [30]
_DBIMGS = [944]

_QUERIES = [51]
_DBIMGS = [2551]

_QUERIES = [0]
_DBIMGS = [69]

# for qi, fname in enumerate(gnd['qimlist']):  # query index
for qi in _QUERIES:
    fname = gnd['qimlist'][qi]

    q_bbox = BBox(*gnd['gnd'][qi]['bbx'])
    q_vecs, q_centroid_ids, q_coordx, q_coordy, q_scales, q_residuals = DATABASE.get_geometries(qi, bbox=q_bbox, query=True)

    # for dbi, gnd_imid in enumerate(ranks[qi, :MAX_SPATIAL]):
    for gnd_imid in _DBIMGS:
        
        db_fname = gnd['imlist'][gnd_imid]
        dbid = DATABASE.get_image_id(db_fname)
        
        db_vecs, db_centroid_ids, db_coordx, db_coordy, db_scales, db_residuals = DATABASE.get_geometries(dbid)

        # Generate correspondences
        corrs, similarities = get_tentative_correspondencies(
            q_centroid_ids, db_centroid_ids, q_residuals, db_residuals, DATABASE.kernel_similarity
        )
        _similarities = similarities * (similarities > 0)
        assert corrs.size
        
        # Pick corresponding data
        scales1 = q_scales[corrs[:, 0]]
        coordx1 = q_coordx[corrs[:, 0]]# * 16 / scales1
        coordy1 = q_coordy[corrs[:, 0]]# * 16 / scales1

        scales2 = db_scales[corrs[:, 1]]
        coordx2 = db_coordx[corrs[:, 1]]# * 16 / scales2
        coordy2 = db_coordy[corrs[:, 1]]# * 16 / scales2

        # Detect lines too
        MIN_LINE_LENGTHS = 40
        MAX_LINE_LENGTH = 110
        q_lines = DATABASE.get_lines(fname, bbox=q_bbox, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        db_lines = DATABASE.get_lines(db_fname, min_length=MIN_LINE_LENGTHS, max_length=MAX_LINE_LENGTH, display=False)
        qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
        dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')
        
        # Generate hypotheses
        # hypotheses = generate_hypotheses(coordx1, coordy1, scales1, coordx2, coordy2, scales2)
        hypotheses, new_hypotheses = generate_hypotheses_with_lines(
            coordx1,
            coordy1,
            scales1,
            coordx2,
            coordy2,
            scales2,
            q_lines,
            db_lines,
            qimg,
            dbimg,
            q_bbox,
            display=False,
            display_final=False,
        )
        any_line_hypotheses = False
        num_original_hypotheses = hypotheses.shape[0]
        all_hypotheses = hypotheses.copy()
        if new_hypotheses.size:
            any_line_hypotheses = True
            all_hypotheses = np.vstack([all_hypotheses, new_hypotheses])

            # I am debugging the line hypothesis
            all_hypotheses = new_hypotheses
        
        # Compute errors and verify models
        errors = compute_errors(hypotheses, coordx1, coordy1, coordx2, coordy2)
        determinants = np.linalg.det(hypotheses)
        scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
        verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD)

        all_errors = compute_errors(all_hypotheses, coordx1, coordy1, coordx2, coordy2)
        all_determinants = np.linalg.det(all_hypotheses)
        all_scales_ratios = scales1[np.newaxis, :] * all_determinants[:, np.newaxis] / scales2[np.newaxis, :]
        all_verifications = verify_models(all_errors, all_scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD)
        
        #####
        #####
        #####

        def evaluate_(verifications_, similarities_, hypotheses_, num_original_hypotheses_):
            # Local optimization of the weighted best model
            weighted_scores_ = -(verifications_ * similarities_[np.newaxis]).sum(axis=1)
            sorted_hypothesis_indexes_ = weighted_scores_.argsort()
            keys_ = sorted_hypothesis_indexes_[:10]

            # I am debugging the line hypothesis
            # return (
            #     -weighted_scores_[sorted_hypothesis_indexes_[0]],
            #     False,
            #     verifications_[sorted_hypothesis_indexes_[0]],
            #     hypotheses_[sorted_hypothesis_indexes_[0]]
            # )

            total_A_, total_mask_, total_support_, optim_index_ = local_optimization_of_list_of_models(
                hypotheses_[keys_], verifications_[keys_], similarities_, corrs, coordx1, coordy1, scales1, coordx2, coordy2, scales2,
                INLIER_THRESHOLD, return_index=True
            )
            optimal_lo_comes_from_lines_ = bool(keys_[optim_index_] >= num_original_hypotheses_)
            optimal_comes_from_lines_ = bool(sorted_hypothesis_indexes_[0] >= num_original_hypotheses_)
            
            return (
                total_support_,
                optimal_lo_comes_from_lines_,
                # -weighted_scores_[sorted_hypothesis_indexes_[0]],
                # optimal_comes_from_lines_,
                total_mask_,
                total_A_
            )
        

        # all weighted support
        aws_lo, aws_lo_fl, total_mask, total_A = evaluate_(all_verifications, _similarities, all_hypotheses, num_original_hypotheses)



        print('Original supp:', aws_lo)
        print()


        # vps = io_helpers.load_pickle('_vp_database_ownsegments.pkl')
        vps = io_helpers.load_pickle('_vp_database.pkl')
        vpq = vps[fname]
        vpdb = vps[db_fname]

        def generate_vps_combinations(vps1, vps2, min_support=7):
            vps1 = [vp for vp in vps1 if vp['coords'][-1] == 1 and vp['support'] >= min_support]
            vps2 = [vp for vp in vps2 if vp['coords'][-1] == 1 and vp['support'] >= min_support]

            if vps1[0]['direction'] != 'VERTICAL' or vps2[0]['direction'] != 'VERTICAL':
                return
            
            v1 = vps1[0]
            v2 = vps2[0]

            for h1 in vps1[1:]:
                for h2 in vps2[1:]:
                    assert h1['direction'] == 'HORIZONTAL' and h2['direction'] == 'HORIZONTAL'
                    yield (
                        np.array(v1['coords'][:2]), 
                        np.array(h1['coords'][:2]), 
                        np.array(v2['coords'][:2]), 
                        np.array(h2['coords'][:2]),
                    )


        def get_kernel_from_vps(qhvp, qvvp, dbhvp, dbvvp):
            A_vp = np.array([
                [-qhvp[0], -qhvp[1], -1, 0, 0, 0, qhvp[0] * dbhvp[0], qhvp[1] * dbhvp[0], dbhvp[0]],
                [0, 0, 0, -qhvp[0], -qhvp[1], -1, qhvp[0] * dbhvp[1], qhvp[1] * dbhvp[1], dbhvp[1]],
                
                [-qvvp[0], -qvvp[1], -1, 0, 0, 0, qvvp[0] * dbvvp[0], qvvp[1] * dbvvp[0], dbvvp[0]],
                [0, 0, 0, -qvvp[0], -qvvp[1], -1, qvvp[0] * dbvvp[1], qvvp[1] * dbvvp[1], dbvvp[1]],
            ], dtype=float)
            U, S, Vh = np.linalg.svd(A_vp)

            return Vh.T[:, -5:]
        

        H_base = get_kernel_from_vps(vpq[1]['coords'], vpq[0]['coords'], vpdb[1]['coords'], vpdb[0]['coords'])


        def estimate_homography_with_hard_constraint_unstable(constraint_matrix, cx1, cy1, cx2, cy2):
            # "coord x 1", etc, because x1 is used in the loop
            A_pts = []
            for x1, y1, x2, y2 in zip(cx1, cy1, cx2, cy2):
                A_pts.append([-x1, -y1, -1, 0, 0, 0, x1 * x2, y1 * x2, x2])
                A_pts.append([0, 0, 0, -x1, -y1, -1, x1 * y2, y1 * y2, y2])
            A_pts = np.array(A_pts)

            A = A_pts @ constraint_matrix
            U, S, Vh = np.linalg.svd(A)
            alpha = Vh.T[:, -1]
            H = constraint_matrix @ alpha

            return H.reshape(3, 3)


        def center_and_normalize_image_points(pts):
            centroid = pts.mean(axis=1)
            # rms_mean_dist = np.sqrt(2 * np.mean(np.square(pts - centroid[:, np.newaxis])))
            rms_mean_dist = np.linalg.norm(pts - centroid[:, np.newaxis], axis=0).mean()

            norm_factor = np.sqrt(2) / rms_mean_dist

            transformation = np.array([
                [norm_factor, 0, -norm_factor * centroid[0]],
                [0, norm_factor, -norm_factor * centroid[1]],
                [0, 0, 1],
            ])
            transformed_pts = transformation[:2] @ homogeneous(pts)

            return transformation, transformed_pts


        def estimate_homography_with_hard_constraint(constraint_matrix, cx1, cy1, cx2, cy2):
            pts1 = np.array([cx1, cy1])
            pts2 = np.array([cx2, cy2])

            norm_trans1, norm_pts1 = center_and_normalize_image_points(pts1)
            norm_trans2, norm_pts2 = center_and_normalize_image_points(pts2)


            vpq1 = norm_trans1 @ vpq[1]['coords']
            vpq0 = norm_trans1 @ vpq[0]['coords']
            vpdb1 = norm_trans2 @ vpdb[1]['coords']
            vpdb0 = norm_trans2 @ vpdb[0]['coords']
            # H_base = get_kernel_from_vps(vpq[1]['coords'], vpq[0]['coords'], vpdb[1]['coords'], vpdb[0]['coords'])
            constraint_matrix = get_kernel_from_vps(vpq1, vpq0, vpdb1, vpdb0)


            A_pts = []
            for x1, y1, x2, y2 in zip(*norm_pts1, *norm_pts2):
                A_pts.append([-x1, -y1, -1, 0, 0, 0, x1 * x2, y1 * x2, x2])
                A_pts.append([0, 0, 0, -x1, -y1, -1, x1 * y2, y1 * y2, y2])
            A_pts = np.array(A_pts)

            A = A_pts @ constraint_matrix
            U, S, Vh = np.linalg.svd(A)
            alpha = Vh.T[:, -1]
            H = constraint_matrix @ alpha
            H = H.reshape(3, 3)

            model = np.linalg.inv(norm_trans2) @ H @ norm_trans1

            return model
        

        def estimate_homography_with_hard_constraint_supernorm(constraint_matrix, cx1, cy1, cx2, cy2):

            pts_to_norm1 = np.hstack([
                np.array([cx1, cy1]),
                np.array(vpq[1]['coords'])[:2, np.newaxis],
                np.array(vpq[0]['coords'])[:2, np.newaxis],
            ])
            pts_to_norm2 = np.hstack([
                np.array([cx2, cy2]),
                np.array(vpdb[1]['coords'])[:2, np.newaxis],
                np.array(vpdb[0]['coords'])[:2, np.newaxis],
            ])

            norm_trans1, norm_pts1 = center_and_normalize_image_points(pts_to_norm1)
            norm_trans2, norm_pts2 = center_and_normalize_image_points(pts_to_norm2)

            constraint_matrix = get_kernel_from_vps(norm_pts1[:, -2], norm_pts1[:, -1], norm_pts2[:, -2], norm_pts2[:, -1])

            A_pts = []
            for x1, y1, x2, y2 in zip(*norm_pts1[:, :-2], *norm_pts2[:, :-2]):
                A_pts.append([-x1, -y1, -1, 0, 0, 0, x1 * x2, y1 * x2, x2])
                A_pts.append([0, 0, 0, -x1, -y1, -1, x1 * y2, y1 * y2, y2])
            A_pts = np.array(A_pts)

            A = A_pts @ constraint_matrix
            U, S, Vh = np.linalg.svd(A)
            alpha = Vh.T[:, -1]
            H = constraint_matrix @ alpha
            H = H.reshape(3, 3)

            model = np.linalg.inv(norm_trans2) @ H @ norm_trans1

            model /= model[-1, -1]

            return model
        

        def estimate_homography_with_hard_constraint_supernorm_new(cx1, cy1, cx2, cy2, v1, h1, v2, h2):

            pts_to_norm1 = np.hstack([np.array([cx1, cy1]), h1[:, np.newaxis], v1[:, np.newaxis]])
            pts_to_norm2 = np.hstack([np.array([cx2, cy2]), h2[:, np.newaxis], v2[:, np.newaxis]])

            norm_trans1, norm_pts1 = center_and_normalize_image_points(pts_to_norm1)
            norm_trans2, norm_pts2 = center_and_normalize_image_points(pts_to_norm2)

            constraint_matrix = get_kernel_from_vps(norm_pts1[:, -2], norm_pts1[:, -1], norm_pts2[:, -2], norm_pts2[:, -1])

            A_pts = []
            for x1, y1, x2, y2 in zip(*norm_pts1[:, :-2], *norm_pts2[:, :-2]):
                A_pts.append([-x1, -y1, -1, 0, 0, 0, x1 * x2, y1 * x2, x2])
                A_pts.append([0, 0, 0, -x1, -y1, -1, x1 * y2, y1 * y2, y2])
            A_pts = np.array(A_pts)

            A = A_pts @ constraint_matrix
            U, S, Vh = np.linalg.svd(A)
            alpha = Vh.T[:, -1]
            H = constraint_matrix @ alpha
            H = H.reshape(3, 3)

            model = np.linalg.inv(norm_trans2) @ H @ norm_trans1

            model /= model[-1, -1]

            return model
        
        

        for v1, h1, v2, h2 in generate_vps_combinations(vpq, vpdb):
            H_estimated_supernorm = estimate_homography_with_hard_constraint_supernorm_new(
                coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask], v1, h1, v2, h2
            )


        H_estimated = estimate_homography_with_hard_constraint(
            H_base, coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]
        )
        H_estimated_us = estimate_homography_with_hard_constraint_unstable(
            H_base, coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]
        )
        # H_estimated_supernorm = estimate_homography_with_hard_constraint_supernorm(
        #     H_base, coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]
        # )
        # H_aff = H_estimated_supernorm.copy()
        # H_aff[-1, :-1] = 0

        errors = compute_errors(H_estimated[np.newaxis], coordx1, coordy1, coordx2, coordy2)
        # determinants = np.linalg.det(hypotheses)
        # scales_ratios = scales1[np.newaxis, :] * determinants[:, np.newaxis] / scales2[np.newaxis, :]
        scales_ratios = np.ones_like(coordx1)[np.newaxis]
        verifications = verify_models(errors, scales_ratios, corrs, inlier_threshold=INLIER_THRESHOLD)

        weighted_scores = (verifications * similarities[np.newaxis]).sum(axis=1)

        print('H score', weighted_scores)
        print('H deter', np.linalg.det(H_estimated))
        print('== H (cyan) ==')
        print(H_estimated_supernorm)



        DISPLAY = 1
        if DISPLAY:


            def plot_bbox(bbox, ax, A=None, qw=None, Acol='red'):
                x1, y1, x2, y2 = bbox.as_tuple
                ax.plot([x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1], 'r-')

                if A is not None and qw is not None:
                    tr_coords_A = transform_points(A, [x1, x2, x2, x1, x1], [y1, y1, y2, y2, y1])
                    ax.plot(tr_coords_A[0] + qw, tr_coords_A[1], color=Acol)


            IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')
            qimg = plt.imread(IMG_ROOT / f'{fname}.jpg')
            dbimg = plt.imread(IMG_ROOT / f'{db_fname}.jpg')

            qh, qw, _ = qimg.shape
            dbh, dbw, _ = dbimg.shape
            
            img = np.zeros([max(qh, dbh), qw + dbw, 3], dtype=int)
            img[:qh, :qw] = qimg
            img[:dbh, qw:] = dbimg

            plt.imshow(img)
            plt.title(f'q={fname}, db={db_fname}, score={aws_lo}')
            plot_bbox(q_bbox, plt, total_A, qw)
            plot_bbox(q_bbox, plt, H_estimated, qw, Acol='lime')
            plot_bbox(q_bbox, plt, H_estimated_us, qw, Acol='green')
            plot_bbox(q_bbox, plt, H_estimated_supernorm, qw, Acol='cyan')
            for qx, qy, dbx, dby in zip(coordx1[total_mask], coordy1[total_mask], coordx2[total_mask], coordy2[total_mask]):
                plt.plot(
                    [qx, dbx + qw],
                    [qy, dby],
                    color='green',
                    marker='o',
                    linestyle='dashed',
                    linewidth=1,
                    markersize=3,
                )
            plt.show()

            print(fname, '//', db_fname)
            print(total_A)
            print()

            # cont = input('Continue? [Y/n]')
            # assert cont.lower() != 'n'
