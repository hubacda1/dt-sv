from pathlib import Path
from asmk import io_helpers
import matplotlib.pyplot as plt

IMG_ROOT = Path('/Users/danielhubacek/Documents/school/ing/asmk-image-retrieval/oxbuild_images')

lines = io_helpers.load_pickle('_lines_database.pkl')
# lines = io_helpers.load_pickle('_lines_database_htlcnn.pkl')

vps = io_helpers.load_pickle('_vp_database.pkl')

gnd = io_helpers.load_pickle('../../asmk/data/test/roxford5k/gnd_roxford5k.pkl')

for qi, NAME in enumerate(gnd['qimlist']):

    img = plt.imread(IMG_ROOT / f'{NAME}.jpg')

    plt.title(f'qi={qi}, {NAME}, Lines 1')
    plt.imshow(img)
    for x1, y1, x2, y2 in lines[NAME]:
        plt.plot([x1, x2], [y1, y2])

    for vp in vps[NAME][:2]:
        plt.plot(*vp['coords'][:2], 'r.')

    for vp in vps[NAME][2:]:
        plt.plot(*vp['coords'][:2], 'b.')

    plt.show()

    cont = input('Continue? [Y/n] ')
    assert cont == '' or cont == 'y'
