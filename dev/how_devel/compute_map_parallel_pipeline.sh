MAX=70
CORES=48

STEP=$(($MAX/$CORES))

for i in $(seq 0 $STEP $(($MAX-1))); do
    max=$(( ($i+$STEP) > $MAX ? $MAX : ($i+$STEP) ))

    from=$i
    to=$max

    echo $from $to
    python compute_map_parallel.py $from $to &
done

echo "Tasks assigned, waiting"

wait

echo "All done, run integration"
python compute_map_parallel_integration.py
echo "Integration done"
